const admin = require("firebase-admin");


var serviceAccount = require("../wayt-78486-firebase-adminsdk-dolqe-a0a646196f.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://wayt-78486-default-rtdb.firebaseio.com"
})


module.exports.admin = admin