'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Settings',
    [
      {
        name:"limit",
        key:"limit",
        value:"10"
      },
      {
        name:"page",
        key:"page",
        value:'1'
      },
      {
        name:"order",
        key:"order",
        value:"ASC"
      },
      {
        name:"orderBy",
        key:"orderBy",
        value:"createdAt"
      },
      {
        name:"distance",
        key:"distance",
        value:"20"
      }
    ],
    {},
  );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Settings', null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
