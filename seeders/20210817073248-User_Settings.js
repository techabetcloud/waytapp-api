'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('User_Settings',
    [
      {
        userId:1,
        key:"distanceForClub",
        value:"20"
      },
      {
        userId:1,
        key:"distanceForNearbyUser",
        value:'20'
      },
    ],
    {},
  );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('User_Settings', null, {});
  
  }
};
