'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('WeekDays',
    [
      {
        day: "MONDAY",
      },
      {
        day: "TUESDAY",
      },
      {
        day: "WEDNESDAY",
      },
      {
        day: "THURSDAY",
      },
      {
        day: "FRIDAY",
      },
      {
        day: "SATURDAY",
      },
      {
        day: "SUNDAY",
      },
    ],
    {},
  );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('WeekDays', null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
