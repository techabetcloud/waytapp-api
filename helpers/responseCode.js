import { responseCode } from './StatusCode'


export const responseObject = (
  req,
  res,
  data,
  code = responseCode.OK,
  success = true,
  message = ""
) => 
  res.status(code).send({
  code,
  message:message,
  success:success,
  data,
 
});

export const responseMethod = (
  req,
  res,
  data,
  code = responseCode.OK,
  success = true,
  message = ""
) =>
 res.status(code).send({
  code,
  errors: [{
    message: [message]
  }],
  success: success,
  data,
});

export const responseObj = (data, code = responseCode.OK, success = false) => {
return {
  data,
  success: success,
  code: code,
};
};

export const errorResponse = (
  req,
  res,
  errorMessage = "Something went wrong",
  code = responseCode.INTERNAL_SERVER_ERROR,
  error = {}
) =>
 res.status(code).json({
  code,
  errorMessage,
  error,
  data: null,
  success: false,
});

export const joiErrorFormatConvert = (
  statusCodes,
  message,
  context,
  status,
  data,
) =>{
  const object = {
  code:statusCodes,
  message:message,
  status:status,
  data:data,
  context: {
    key: context.key,
    value: context.value
  },
  }
  return object
}