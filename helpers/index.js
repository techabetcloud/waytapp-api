import jwt from "jsonwebtoken";
import crypto from "crypto";


var response = {
  success: false,
  message: "",
  data: "",
};

export const generateToken = async (data, type) => {
  let expireTime = ''
  if (type == 'forget') {
    expireTime = process.env.VERIFYTOKEN
  } else {
    expireTime =  process.env.TOKEN_EXPIRES_IN
  }
  const token = await jwt.sign(data, process.env.SECRET, {
    expiresIn: expireTime,
  });
  return token;
};

export const extractToken = async (token) => {

  return jwt.verify(token, process.env.SECRET, (err, user) => {
    if (err) {
      response.message =
        "Your verification link may have expired. Please click on resend for verify your Email.";
      return response;
    }
    response.message = "Valid token";
    response.success = true;
    response.data = user;
    return response;
  });
};
