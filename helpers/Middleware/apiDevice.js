import { User, User_Login, } from '../../models';

const jwt = require('jsonwebtoken');

const deviceApiAuth = async (req, res, next) => {

  if (!(req.headers && req.headers['x-token'])) {
    return res.status(401).send({ message: 'Token is not provided' });
  }
  const token = req.headers['x-token'];
  try {
    const decoded = jwt.verify(token, process.env.SECRET);
    req.user = decoded;
    
    const user = await User.scope('withSecretColumns').findOne({
      include: [
        {
          model: User_Login,
          as: 'Userlogin',
          where: { loginToken: token }
        },

      ],
      where: { email: req.user.email }
    });
    if (user) {
      if (user.Userlogin.loginToken != token) {
        return res.status(401).send({ message: "Your token is expired or invalid." })
      }
      const reqUser = { ...user.get() };
      reqUser.userId = user.id;
      req.user = reqUser;
      req.user.deviceToken = user.Userlogin.deviceToken
      return next();
    } else {
      return res.status(401).send({ message: "You are not authorize to access this resource." })
    }

  } catch (error) {
    return res.status(401).send({ message: "Incorrect token is provided, try re-login" })


  }
};

module.exports = deviceApiAuth;
