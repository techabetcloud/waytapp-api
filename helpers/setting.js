import { Setting,User_Setting } from '../models'


export const FILTER = async function () {
  const data = await Setting.findAll({ raw: true })
  const resp = {}
  data.map((el) => {
    const key = el.key
    resp[key] = el.value
  })
  return resp
}

export const NearByUser = async function (req,res) {
  const data = await User_Setting.findAll({
    where:{
        userId:req.user.userId
    }
  }) 
  const resp = {}
  data.map((el) => {
    const key = el.key
    resp[key] = el.value
  })
  return resp
}