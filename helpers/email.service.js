import { User_Login } from '../models'
const nodemailer = require("nodemailer");
const  {admin}    = require("../config/firebaseConfig");



export default {
    async sendHTmlemail(name, data, email) {
        const transporter = nodemailer.createTransport({
            service: 'gmail',
            // host: 'smtp.gmail.com',
            // port: 465,
            // secure: true, // use SSL
            auth: {
                user: `${process.env.EMAILUSER}`,
                pass: `${process.env.EMAILPASS}`,
            }
        });
        const option = {
            from: 'bhandaripankaj532@gmail.com',
            to: email, // list of receivers
            subject: "Verification OTP", // Subject line
            html: `<b>Dear ${name}</b> <br>
            Your verification OTP is <h3>${data}</h3>
            Regrads<br>
            Waytapp
            `
        };

        transporter.sendMail(option, function (err, info) {
            if (err) {
                console.log('dfdgfdfgg.....', err);
                return "Email failed."
            } else {
                console.log('Message sent: ' + info.response);
                return "Email sent";
            }
        })
    },
    async firebaseService(body)  {
        const payload = {
          notification: {
            title:body.title,
            body: body.data
          }
        }
        const options = {
          priority: "high",
          timeToLive: 60 * 60 * 24
        }
        admin.messaging().sendToDevice(body.token, payload, options).then(function (pushNotification) {
          console.log("Successfully sent message.", pushNotification);
          return  true
        }).catch(function (error) {
          console.log("Something went wrong.", error)
        })
    },
}