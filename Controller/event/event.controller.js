const Sequelize = require('sequelize');
//  import { Event } from '../../models'
const { Event,Visit,User,Restaurant } = require('../../models')
import { Console } from 'console';
import { responseObject, errorResponse } from '../../helpers/responseCode';
import { responseCode } from '../../helpers/StatusCode'
import models from "../../models";
import { FILTER , NearByUser }  from '../../helpers/setting'
var sequelize = models.sequelize;
var moment = require("moment");
const {
    QueryTypes,
    Op
} = require("sequelize");
const log = require('log-to-file');


export default {
    async getEventByDate(req,res){
      log(`userName:${req.user.name} , lat:${req.query.lat} , long:${req.query.long}`, 'error-logs.log');
      if (req.query.lat === "null" || req.query.long === "null") {
        return responseObject(req, res, {}, responseCode.BAD_REQUEST, false, res.__("Lat and Long are required."))
      }
      var resp = await FILTER()
      var near = await NearByUser(req,res)

      const limit = parseInt(resp.limit)
      const offset = ((resp.page - 1) * parseInt(resp.limit))
      const orderby = resp.orderBy
      const order = resp.order
      const distance =  (near.distanceForClub && parseInt(near.distanceForClub)) || resp.distance
        try {
          const lat = parseFloat(req.query.lat).toFixed(6);
          const long = parseFloat(req.query.long).toFixed(6);
          
         
              // const date = moment(req.query.date).format("YYYY-MM-DD HH:mm:ss")
              const date =    moment(new Date(req.query.date)).format("YYYY-MM-DD HH:mm:ss")
            var filter ;
             if(req.query.date){
                filter = `Events.from_date <= '${date}' AND Events.to_date >= '${date}' AND Restaurants.status = 1`
             }
             
          const Event = await sequelize.query(`SELECT Events.id,Events.restaurantId,Events.name,Restaurants.categoryId,Events.description, Events.address, Events.city, Events.state, Events.country,
          CONCAT('${process.env.BASE_URL}images/event/',picture) AS eventPhoto, 
          Events.lat, Events.long, Events.from_date, Events.to_date,Restaurants.status,
          IF(Restaurants.status = 1,"Active","Inactive") AS status,Events.createdAt,
                   (3959 * acos(
                cos(radians(${lat}) ) * cos(radians( Events.lat) ) * cos (radians(Events.long) - radians(${long}
                )) + sin(radians(${lat})) * sin(radians( Events.lat )))) AS distance
             FROM Events
      LEFT JOIN Restaurants ON Restaurants.id = Events.restaurantId

             HAVING ${filter} AND  distance < ${distance} ORDER BY ${orderby} ${order}  LIMIT ${limit} OFFSET ${offset}`,
              {
             replacements:[req.query.date],
             nest: true,
             type: QueryTypes.SELECT,
           })
           if(Event.length){
            return responseObject(
                req,
                res,
                Event,
                responseCode.OK,
                true,
                res.__("GET_EVENT_SUCCESSFULLY")
                );
                }else{
                    return responseObject(
                        req,
                        res,
                        "",
                        responseCode.NOT_FOUND,
                        false,
                        res.__("DATA_NOT_FOUND")

                      )
                }
        } catch (err) {
           return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")

              );
        }
    },
    async getEventClubBar(req,res){
        try {
          log(`userName:${req.user.name} , lat:${req.query.lat} , long:${req.query.long}`, 'error-logs.log');
          if (req.query.lat === "null" || req.query.long === "null") {
            return responseObject(req, res, {}, responseCode.BAD_REQUEST, false, res.__("Lat and Long are required."))
          }
          var resp = await FILTER()
          var near = await NearByUser(req,res)

          const limit = parseInt(resp.limit)
          const offset = ((resp.page - 1) * parseInt(resp.limit))
          const orderby = resp.orderBy
          const order = resp.order
          const distance =  (near.distanceForClub && parseInt(near.distanceForClub)) || resp.distance
          const date = moment(req.query.date).utc().format("YYYY-MM-DD HH:mm:ss")

          const lat = parseFloat(req.query.lat).toFixed(6);
          const long = parseFloat(req.query.long).toFixed(6);
         
              var filter ;
              var category ;
              if(req.query.date){
                 filter = `Events.from_date <= '${date}' AND Events.to_date >= '${date}'`
              }
              if(req.query.type === "club"){
                category = `  AND Restaurants.categoryId = 1 AND Restaurants.status = 1`
              }
               if(req.query.type === "bar"){
                category = `  AND Restaurants.categoryId = 2 AND Restaurants.status = 1`
                  
             }
            const Event = await sequelize.query(`SELECT Events.id,Events.restaurantId,Events.name, Events.description, Events.address, Events.city, Events.state, Events.country, CONCAT('${process.env.BASE_URL}images/event/',picture) AS eventPhoto, Events.lat, Events.long, Events.from_date, Events.to_date, Restaurants.adminId,Restaurants.categoryId,Restaurants.status,Restaurants.link,Restaurants.logo,Events.createdAt,
            IF(Restaurants.status = 1,"Active","Inactive") AS status,
            IF(Restaurants.categoryId = 1,"Club","Bar") AS category,
            (3959 * acos(
         cos(radians(${lat}) ) * cos(radians(Events.lat) ) * cos (radians(Events.long) - radians(${long}
         )) + sin(radians(${lat})) * sin(radians(Events.lat ))) )AS distance
      FROM Events
      LEFT JOIN Restaurants ON Restaurants.id = Events.restaurantId
      HAVING ${filter} ${category} AND  distance < ${distance}  ORDER BY ${orderby} ${order}  LIMIT ${limit} OFFSET ${offset}`,
       {
      replacements:[req.query.date,lat,long],
      nest: true,
      type: QueryTypes.SELECT,
    })
    if(Event.length){
     return responseObject(
         req,
         res,
         Event,
         responseCode.OK,
         true,
         res.__("GET_EVENT_SUCCESSFULLY")
         );
         }else{
             return responseObject(
                 req,
                 res,
                 "",
                 responseCode.NOT_FOUND,
                 false,
                 res.__("DATA_NOT_FOUND")

               )

         }
        } catch (err) {
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")

              );
        }
    },
    async joinVisits(req,res){
        try {
              const restaurant =  await Restaurant.findOne({
                where:{
                  id:req.body.restaurantId,
                categoryId:req.body.category
                }
              })
              if(!restaurant){
                return responseObject(
                  req,
                  res,
                  "",
                  responseCode.NOT_FOUND,
                  false,
                  res.__("RESTAURANT_AND_CATEGORY_NOT_FOUND")
                )
              }
          var date = moment(req.body.date).format('YYYY-MM-DD')
          const todayVisit = await Visit.findOne({
            where:{
              userId:req.user.userId,
                categoryId:req.body.category,
                restaurantId:req.body.restaurantId,
                visit_date:date
            }
          })
             if(todayVisit){
              return responseObject(
                req,
                res,
                "",
                responseCode.BAD_REQUEST,
                false,
                res.__("USER_ALREADY_JOIN")
              );
             }
         await Visit.create({
                userId:req.user.userId,
                categoryId:req.body.category,
                restaurantId:req.body.restaurantId,
                visit_date:date
              }).then(Visit=>{
                  if(Visit){
                    return responseObject(
                        req,
                        res,
                        Visit,
                        responseCode.OK,
                        true,
                        res.__("JOIN_SUCCESSFULLY")
                        ); 
                  }else{
                    return responseObject(
                        req,
                        res,
                        "",
                        responseCode.BAD_REQUEST,
                        false,
                        res.__("SOMETHING_WENT_WRONG")
                      );
                     }
              }).catch(err=>{
        
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.INTERNAL_SERVER_ERROR,
                    false,
                    res.__("SOMETHING_WENT_WRONG")

                  );
              })

        } catch (err) {
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")

              );
        }
    },
    async getvisitsUser(req,res){
        try {
          var resp = await FILTER()
          const limit = parseInt(resp.limit)
          const offset = ((resp.page - 1) * parseInt(resp.limit))
          const orderby = resp.orderBy
          const order = resp.order
            var filter;
            var date = moment(req.query.date).utc().format("YYYY-MM-DD")
           
            if(req.query.date){
                filter = ` Visits.visit_date  = '${date}'` 
            }
                  const Visit = await sequelize.query(
                                  `SELECT Visits.id,Visits.userId,Users.name,CONCAT('${process.env.BASE_URL}images/userProfile/',profile_img) AS profile_img,Visits.restaurantId,Visits.categoryId,Visits.createdAt,
                                   DATE(Visits.visit_date) AS VisitDate
                                   FROM Visits
                                      LEFT JOIN Users ON Users.id = Visits.userId
                                   WHERE ${filter} and Visits.categoryId = ${req.query.category} AND Visits.restaurantId = ${req.query.restaurantId} ORDER BY ${orderby} ${order}  LIMIT ${limit} OFFSET ${offset}
                                   `,
                                {
                                  replacements:[req.query.category,req.query.date,req.query.restaurantId],
                                  nest: true,
                                  type: QueryTypes.SELECT,
                                })
                       if(Visit.length){
                       return responseObject(
                              req,
                              res,
                              Visit,
                              responseCode.OK,
                              true,
                              res.__("GET_VISIT_SUCCESSFULLY")
                            );
                      }else{
                      return responseObject(
                             req,
                             res,
                             "",
                             responseCode.NOT_FOUND,
                             false,
                             res.__("DATA_NOT_FOUND")

                           )
                        }
        } catch (err) {
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")

              );
        }
    },
    async getFavouritePlaces(req,res){
      try {
           
           await User.findAll({
             attributes:[('id'),[sequelize.col('profile_img'), 'profile'] ],
             include:[
           {
           model:Visit,
           as: "visit",
          attributes: ['restaurantId',[sequelize.fn('COUNT', sequelize.col('restaurantId')), 'total']],
          distinct: 'userId',
          include:[{
            model:Restaurant,
            as: "Restaurant",
            attributes: ['name'],

          }]
           }
          ],
      group : ['restaurantId'],
          where: {
            id:req.query.userId
           },
        }).then(function(favouritePlace) {
          for (let i = 0; i < favouritePlace.length; i++) {
            favouritePlace[i].dataValues.profile = process.env.BASE_URL + 'images/userProfile/' + favouritePlace[i].dataValues.profile;
          }
          if(favouritePlace.length){
            return responseObject(
              req,
              res,
              favouritePlace,
              responseCode.OK,
              true,
              res.__("GET_FAVOURITE_PLACES_SUCCESSFULLY")
              );
          }else{
            return responseObject(
              req,
              res,
              "",
              responseCode.NOT_FOUND,
              false,
              res.__("DATA_NOT_FOUND")

            )
          }
      });
      } catch (err) {
    
        return responseObject(
          req,
          res,
          "",
          responseCode.INTERNAL_SERVER_ERROR,
          false,
          res.__("SOMETHING_WENT_WRONG")

        );
      }
    },
}