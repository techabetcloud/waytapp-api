var express = require('express');
const  { validate } = require ('express-validation')
import * as eventValidator from './event.validation'
import eventController from './event.controller'

var eventRouter = express.Router();

eventRouter.get('/get-event',validate(eventValidator.Event),eventController.getEventByDate)
eventRouter.get('/get-ClubBar',validate(eventValidator.EventClubBar),eventController.getEventClubBar)
eventRouter.post('/join-clubBar',validate(eventValidator.joinVisits),eventController.joinVisits)
eventRouter.get('/get-visitsUser',validate(eventValidator.visitsUser),eventController.getvisitsUser)
eventRouter.get('/get-favouritePlace',validate(eventValidator.FavouritePlaces),eventController.getFavouritePlaces)






module.exports = eventRouter;