const Joi = require('joi');


export const Event = {
    query: Joi.object().keys({
    lat:Joi.string().required()
    .messages({"any.required": `lat is a required field`,
    "string.empty": `lat is not allowed to be empty`}),

    long:Joi.string().required()
    .messages({"any.required": `long is a required field`,
    "string.empty": `long is not allowed to be empty`}),

    date:Joi.date().required()
    .messages({"any.required": `date is a required field`,
    "string.empty": `date is not allowed to be empty`}),

  }).unknown(false).error(errors => {
    errors.forEach(err => {0
      switch (err.type) {
        case "object.allowUnknown":
          err.message = err.path[0] + ` parameter  is not allowed!`;
          break;
        default:
          break
      }
    });
    return errors;
  })
  };
  export const EventClubBar = {
    query: Joi.object().keys({
    lat:Joi.string().required()
    .messages({"any.required": `lat is a required field`,
    "string.empty": `lat is not allowed to be empty`}),

    long:Joi.string().required()
    .messages({"any.required": `long is a required field`,
    "string.empty": `long is not allowed to be empty`}),

    type:Joi.string().valid('bar','club').required()
    .messages({"any.required": `type is a required field`,
    "any.only": `type must be one of (bar, club)`}),

    date:Joi.date().required()
    .messages({"any.required": `date is a required field`,
    "string.empty": `date is not allowed to be empty`}),
  }).unknown(false).error(errors => {
    errors.forEach(err => {0
      switch (err.code) {
        case "object.allowUnknown":
          err.message = err.path[0] + ` parameter  is not allowed!`;
          break;
        default:
          break
      }
    });
    return errors;
  })
  };
  export const joinVisits = {
    body: Joi.object().keys({
    category:Joi.string().required().messages({"any.required": `categorie is a required field`}),
    restaurantId:Joi.string().required().messages({"any.required": `restaurantId is a required field`}),
    date:Joi.date().required().messages({"any.required": `date is a required field`}),
  }).unknown(false).error(errors => {
    errors.forEach(err => {0
      switch (err.code) {
        case "object.allowUnknown":
          err.message = err.path[0] + ` parameter  is not allowed!`;
          break;
        default:
          break
      }
    });
    return errors;
  })
  };
  export const visitsUser = {
    query: Joi.object().keys({
    category:Joi.string().required()
    .messages({"any.required": `category is a required field`,
    "string.empty": `category is not allowed to be empty`}),

    restaurantId:Joi.string().required()
    .messages({"any.required": `restaurantId is a required field`,
    "string.empty": `restaurantId is not allowed to be empty`}),

    date:Joi.date().required()
    .messages({"any.required": `date is a required field`,
    "string.empty": `date is not allowed to be empty`}),

  }).unknown(false).error(errors => {
    errors.forEach(err => {0
      switch (err.code) {
        case "object.allowUnknown":
          err.message = err.path[0] + ` parameter  is not allowed!`;
          break;
        default:
          break
      }
    });
    return errors;
  })
  };
  export const FavouritePlaces = {
    query: Joi.object().keys({
    userId:Joi.string().required()
    .messages({"any.required": `userId is a required field`,
    "string.empty": `date is not allowed to be empty`}),
  }).unknown(false).error(errors => {
    errors.forEach(err => {0
      switch (err.code) {
        case "object.allowUnknown":
          err.message = err.path[0] + ` parameter  is not allowed!`;
          break;
          default:
          break
      }
    });
    return errors;
  })
  };