const Joi = require('joi');


export const register = {
    body: Joi.object().keys({
      name:Joi.string().required()
      .messages({"any.required": `name is a required field`,
       "string.empty": `name is not allowed to be empty`}),

      gender:Joi.string().required()
      .messages({"any.required": `gender is a required field`,
    "string.empty": `gender is not allowed to be empty`}),

      username:Joi.string(),
      profile_img:Joi.string().optional(),
      long:Joi.string().required()
      .messages({"any.required": `long is a required field`,
    "string.empty": `long is not allowed to be empty`}),

      lat:Joi.string().required()
      .messages({"any.required": `lat is a required field`,
    "string.empty": `lat is not allowed to be empty`}),

      date_of_birth:Joi.string().required()
      .messages({"any.required": `dob is a required field`,
    "string.empty": `dob is not allowed to be empty`}),

      email: Joi.string()
      .required().regex(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z ]{2,}))$/)
      .max(50)
      .error(errors => {
        errors.forEach(err => {
          switch (err.code) {
            case "string.empty":
              err.message = "Email should not be empty!";
              break;
              case "any.required":
                err.message = "Email is a required field";
                break;
            case "string.pattern.base":
              err.message = "Invalid email id";
              break;
            default:
              break;
          }
        });
        return errors;
        }),
      password: Joi.string().required().regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?.&-])[A-Za-z\d@$.!%*?&-]{8,16}$/).error(errors => {
        errors.forEach(err => {
          switch (err.code) {
            case "string.empty":
              err.message = "password should not be empty!";
              break;
              case "any.required":
                err.message = "Password is a required field";
                break;
            case "string.pattern.base":
            err.message = "Password should have min 8 and max 16 characters, at least 1 uppercase letter, 1 lowercase letter, 1 number and 1 special character"
            default:
              break;
          }
        });
        return errors;
      }),
    }).unknown(false).error(errors => {
      errors.forEach(err => {
        switch (err.code) {
          case "object.unknown":
            err.message = ` parameter  is not allowed!`;
            break;
          default:
            break
        }
      });
      return errors;
    }).options({ abortEarly: false })
  };
  export const login = {
    body: Joi.object().keys({
      deviceType:Joi.string(),
      gender:Joi.string(),
      profile_img:Joi.string(),
      name:Joi.string(),
      deviceToken:Joi.string(),
      type:Joi.string().required()
      .messages({"any.required": `type is a required field`,
      "string.empty": `type is not allowed to be empty`}),
      facebookId:Joi.number(),
      email: Joi.string()
      .required().regex(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z ]{2,}))$/)
      .max(50).error(errors => {
        errors.forEach(err => {
          switch (err.code) {
            case "string.empty":
              err.message = "Email should not be empty!";
              break;
              case "any.required":
                err.message = "Email is a required field";
                break;
            case "string.pattern.base":
              err.message = "Invalid email id";
              break;
            default:
              break;
          }
        });
        return errors;
      }),
      password: Joi.string().required().regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?.&-])[A-Za-z\d@$.!%*?&-]{8,16}$/).error(errors => {
        errors.forEach(err => {
          switch (err.code) {
            case "string.empty":
              err.message = "Password should not be empty!";
              break;
              case "any.required":
                err.message = "Password is a required field";
                break;
            case "string.pattern.base":
              err.message = "Incorrect password"
            default:
              break;
          }
        });
        return errors;
      }),
    }).unknown(false).error(errors => {
      errors.forEach(err => {
        switch (err.code) {
          case "object.allowUnknown":
            err.message = err.path[0] + ` parameter  is not allowed!`;
            break;
          default:
            break
        }
      });
      return errors;
    })

  };
  export const verify = {
    body: Joi.object().keys({
      otp:Joi.number().min(6).required(),
      email: Joi.string()
      .required().regex(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z ]{2,}))$/)
      .max(50).error(errors => {
        errors.forEach(err => {
          switch (err.type) {
            case "any.empty":
              err.message = "Email should not be empty!";
              break;
            case "string.pattern.base":
              err.message = "Invalid email id";
              break;
            default:
              break;
          }
        });
        return errors;
      })
  }).unknown(false).error(errors => {
    errors.forEach(err => {0
      switch (err.type) {
        case "object.allowUnknown":
          err.message = err.path[0] + ` parameter  is not allowed!`;
          break;
        default:
          break
      }
    });
    return errors;
  })
};
  export const forgotPassword = {
    body: Joi.object().keys({
    email: Joi.string()
        .required().regex(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z ]{2,}))$/)
        .max(50).error(errors => {
          errors.forEach(err => {
            switch (err.code) {
              case "string.empty":
                err.message = "Email should not be empty!";
                break;
                case "any.required":
                  err.message = "Email is a required field";
                  break;
              case "string.pattern.base":
                err.message = "Invalid email id";
                break;
              default:
                break;
            }
          });
          return errors;
        }),
      
    }).unknown(false).error(errors => {
      errors.forEach(err => {
        switch (err.code) {
          case "object.allowUnknown":
            err.message = err.path[0] + ` parameter  is not allowed!`;
            break;
          default:
            break
        }
      });
      return errors;
    })
  };
  export const verifyOTP = {
    body: Joi.object().keys({
    otp:Joi.number().min(6).required()
    .messages({"any.required": `OTP is a required field`,
    "number.base": `OTP must be a number`}),

    token:Joi.string().required()
    .messages({"any.required": `token is a required field`,
    "string.empty": `token is not allowed to be empty`}),

  }).unknown(false).error(errors => {
    errors.forEach(err => {0
      switch (err.code) {
        case "object.allowUnknown":
          err.message = err.path[0] + ` parameter  is not allowed!`;
          break;
        default:
          break
      }
    });
    return errors;
  })
};
  export const resetPassword = {
    body: Joi.object().keys({
      email:Joi.string().required()
      .messages({"any.required": `email is a required field`,
       "string.empty": `email is not allowed to be empty`}),      
      token:Joi.string().required()
      .messages({"any.required": `token is a required field`,
       "string.empty": `token is not allowed to be empty`}),   
      password: Joi.string().required().regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?.&-])[A-Za-z\d@$.!%*?&-]{8,16}$/).error(errors => {
        errors.forEach(err => {
          switch (err.code) {
            case "string.empty":
              err.message = "password should not be empty!";
              break;
              case "any.required":
                err.message = "Password is a required field";
                break;
            case "string.pattern.base":
            err.message = "Password should have min 8 and max 16 characters, at least 1 uppercase letter, 1 lowercase letter, 1 number and 1 special character"
            default:
              break;
          }
        });
        return errors;
      }),
      
    }).unknown(false).error(errors => {
      errors.forEach(err => {
        switch (err.code) {
          case "object.allowUnknown":
            err.message = err.path[0] + ` parameter  is not allowed!`;
            break;
          default:
            break
        }
      });
      return errors;
    })
  };
  export const updateProfile = {
    body: Joi.object().keys({
      profile_img:Joi.string().allow(''),
      description:Joi.string().allow(''),
      name:Joi.string().allow(''),

  }).unknown(false).error(errors => {
    errors.forEach(err => {0
      switch (err.code) {
        case "object.allowUnknown":
          err.message = err.path[0] + ` parameter  is not allowed!`;
          break;
        default:
          break
      }
    });
    return errors;
  })
};
export const nearbyUser = {
  query: Joi.object().keys({
  lat:Joi.string().required()
  .messages({"any.required": `lat is a required field`,
  "string.empty": `lat is not allowed to be empty`}),
  long:Joi.string().required()
  .messages({"any.required": `long is a required field`,
  "string.empty": `long is not allowed to be empty`}),
  gender:Joi.string().valid('male','female').required()
  .messages({"any.required": `gender is a required field`,
  "any.only": `gender must be one of (male, female)`}),
}).unknown(false).error(errors => {
  errors.forEach(err => {
    switch (err.code) {

      case "object.allowUnknown":
        err.message = err.path[0] + ` parameter  is not allowed!`;
        break;
      default:
        break
    }
  });
  return errors;
})
};
export const postpicture = {
  body: Joi.object().keys({
    image:Joi.string(),

}).unknown(false).error(errors => {
  errors.forEach(err => {0
    switch (err.code) {
      case "object.allowUnknown":
        err.message = err.path[0] + ` parameter  is not allowed!`;
        break;
      default:
        break
    }
  });
  return errors;
})
};
