import jwt from "jsonwebtoken";
const Sequelize = require('sequelize');
import crypto from "crypto";
import axios from 'axios';

// import userlogin from "../../../models/userlogin";
const {
    generateToken,
    extractToken
} = require('../../helpers/index')
// const sequelize = require('sequelize')
const User = require('../../models').User
import {
    Postphoto,
    Friends
} from '../../models'
// const Postphoto =require('../../models').Postphoto
const Userlogin = require('../../models').User_Login
import {
    responseObject
} from '../../helpers/responseCode';
import {
    responseCode
} from '../../helpers/StatusCode'
import models from "../../models";
import emailservice from '../../helpers/email.service'
import {
    FILTER,
    NearByUser
} from '../../helpers/setting'
import {
    DATEONLY
} from "sequelize";
import {
    Console
} from "console";
var sequelize = models.sequelize;
const {
    QueryTypes,
    Op
} = require("sequelize");
var moment = require('moment')
import sendNotification from '../../helpers/email.service'
const log = require('log-to-file');
export default {
    async register(req, res) {
        try {
            log(`userName:${req.body.name} , lat:${req.body.lat} , long:${req.body.long}`, 'error-logs.log');
            let {
                name,
                email,
                gender,
                username,
                password,
                date_of_birth,

            } = req.body;

            const date = date_of_birth
            const currentdate = new Date()
            const birthDate = new Date(date);
            const calculatedAge = currentdate.getFullYear() - birthDate.getFullYear();

            email = req.body.email.toLowerCase().trim()
            gender = req.body.gender.toLowerCase()
            const user = await User.findOne({
                where: {
                    email: email
                }
            })
            if (user) {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.CONFLICT,
                    false,
                    res.__("USER_ALREDY_EXITS")
                );
            }
            if (birthDate > currentdate) {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.UNAUTHORIZED,
                    false,
                    res.__("YOU_ARE_NOT_SELECT_FUTURE_DATE")
                );
            }
            if (calculatedAge < 16) {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.UNAUTHORIZED,
                    false,
                    res.__("YOU_ARE_NOT_ELIGIBLE")
                );
            }


            const encPass = crypto.createHash("md5").update(password).digest("hex");
            let token_create = {
                email: email,

            };
            const emailVerificationToken = await generateToken(token_create);

            const payload = {
                email,
                name,
                gender,
                username,
                profile_img: req.body.profile_img ? req.body.profile_img : process.env.BASE_URL + '/profile.jpg',
                password: encPass,
                isVerified: true,
                verifyToken: emailVerificationToken,
                lat: parseFloat(req.body.lat).toFixed(6),
                long: parseFloat(req.body.long).toFixed(6),
                date_of_birth
            };
            const register = await User.create(payload)
            if (register) {
                // await emailservice.sendHTmlemail(otp)
                return responseObject(
                    req,
                    res,
                    register,
                    responseCode.OK,
                    true,
                    res.__("CREATE_USER_SUCCESSFULLY")
                );
            }
        } catch (err) {
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")
            );
        }
    },
    async login(req, res) {
        try {
            if (req.body.type == "simple") {
                const user = await User.findOne({
                    where: {
                        email: req.body.email.trim()
                    },
                });
                if (!user) {
                    return responseObject(
                        req,
                        res,
                        "",
                        responseCode.UNAUTHORIZED,
                        false,
                        res.__("INCORRECT_EMAIL")
                    );

                }
                if (user.isDeleted == 1) {
                    return responseObject(
                        req,
                        res,
                        "",
                        responseCode.UNAUTHORIZED,
                        false,
                        res.__("CONTACT_TO_ADMIN")

                    );

                }
                if (user) {
                    const reqPass = crypto.createHash("md5").update(req.body.password || "").digest("hex");
                    if (reqPass !== user.password) {
                        return responseObject(
                            req,
                            res,
                            "",
                            responseCode.UNAUTHORIZED,
                            false,
                            res.__("INCORRECT_PASSWORD")
                        );
                    }
                    let loginToken = {
                        userId: user.id,
                        email: user.email,
                        createdAt: new Date(),
                    }
                    const token = await generateToken(loginToken);

                    const body = {
                        userId: user.id,
                        loginToken: token,
                        deviceType: req.body.deviceType,
                        deviceToken: req.body.deviceToken,
                    };
                    // delete user.password;
                    delete user.verifyToken;
                    await Userlogin.create(body).then(async (userResult) => {
                        if (userResult) {

                            return responseObject(
                                req,
                                res,
                                userResult,
                                responseCode.OK,
                                true,
                                res.__("USER_LOGIN_SUCCESSFULLY")
                            );


                        } else {
                            return responseObject(
                                req,
                                res, {},
                                responseCode.NOT_FOUND,
                                false,
                                res.__("DATA_NOT_FOUND")
                            );
                        }

                    });
                }

            } else{
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.INTERNAL_SERVER_ERROR,
                    false,
                    res.__("SOMETHING_WENT_WRONG")
                );
            }
        } catch (error) {
            console.log(error)
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")
            );

        }
    },
    async verify(req, res) {

        try {
            const extractedToken = await extractToken(req.query.token.trim());


            if (extractedToken.success) {
                User.findOne({
                        where: {
                            email: extractedToken.data.email,
                            verifyToken: req.query.token,
                        },
                    }).then((user) => {


                        if (user.email !== req.body.email) {
                            return res.send({
                                message: "We were unable to find a user for this verification. Please click on SignUp button!"
                            })
                        }
                        if (user.isDeleted) {
                            return res.send({
                                message: "ACCOUNT_SUSPENDED"
                            })
                        }
                        if (user.isVerified) {
                            return res.send({
                                message: "ALREADY_VERIFYED"
                            })
                        }


                        if (extractedToken.data.otp == req.body.otp) {
                            user.isVerified = true;
                            user
                                .save()
                                .then((user) => {
                                    return res.send({
                                        message: "VERIFYED_SUCCESSFULLY"
                                    })

                                })
                                .catch((err) => {
                                    if (err) {

                                        return res.send("SOMETHING_WENT_WRONG")

                                    }
                                });
                        } else {
                            return res.send({
                                message: "WRONG_OTP"
                            })

                        }
                    })
                    .catch((err) => {

                        return res.send("SOMETHING_WENT_WRONG")

                    });
            } else {
                return res.send("VERIFICATION_LINK_EXPIRED")

            }
        } catch (err) {
            return res.send("SOMETHING_WENT_WRONG")

        }
    },
    async forgotPassword(req, res) {
        try {
            const user = await User.findOne({
                where: {
                    email: req.body.email.trim()
                }
            })
            if (!user) {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.UNAUTHORIZED,
                    false,
                    res.__("INCORRECT_EMAIL")
                );
            }
            if (user.isDeleted) {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.UNAUTHORIZED,
                    false,
                    res.__("ACCOUNT_SUSPENDED")
                );

            }
            if (user) {

                const otp = Math.floor(100000 + Math.random() * 9000);
                let token_payload = {
                    email: user.email,
                    otp: otp,
                };
                const passwordForgotToken = await generateToken(token_payload, "forget");
                if (passwordForgotToken) {
                    await emailservice.sendHTmlemail(user.name, otp, user.email)
                    return responseObject(
                        req,
                        res,
                        passwordForgotToken,
                        responseCode.OK,
                        true,
                        res.__("OTP_SEND_YOUR_EMAIL")
                    );
                }
                // const emailotp = await emailservice.sendHTmlemail().then(data=>{
                //         // return res.json({message:"OTP SENT YOUR EMAIL.",data:ema})

                //     }).catch
            }
        } catch (error) {
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")
            );
        }
    },
    async verifyOTP(req, res) {
        try {
            const {
                otp,
                token
            } = req.body
            const extractedToken = await extractToken(token.trim()).then(user => {


                if (user.data.exp == undefined) {
                    return responseObject(
                        req,
                        res,
                        "",
                        responseCode.REQUEST_TIMEOUT,
                        true,
                        res.__("TOKEN_EXPIRED")
                    )
                }
                if (user.data.otp == otp) {
                    return responseObject(
                        req,
                        res,
                        "",
                        responseCode.OK,
                        true,
                        res.__("OTP_CONFIRM_SUCCESSFULLY")
                    )

                } else {
                    return responseObject(
                        req,
                        res,
                        "",
                        responseCode.BAD_REQUEST,
                        true,
                        res.__("INVALID_OTP_PLEASE_RESEND_OTP")
                    )
                }
            }).catch(err => {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.INTERNAL_SERVER_ERROR,
                    false,
                    res.__("SOMETHING_WENT_WRONG")
                );
            })

        } catch (err) {
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")
            );
        }
    },
    async resetPassword(req, res) {
        try {
            const tokenData = await extractToken(req.body.token.trim());

            if (tokenData.data.exp == undefined) {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.REQUEST_TIMEOUT,
                    true,
                    res.__("TOKEN_EXPIRED")
                )
            }
            if (tokenData) {
                const email = tokenData.data.email

                const user = await User.findOne({
                    where: {
                        email: email
                    }
                }).then(async user => {
                    if (!user) {
                        return responseObject(
                            req,
                            res,
                            "",
                            responseCode.UNAUTHORIZED,
                            false,
                            res.__("INCORRECT_EMAIL")
                        );
                    }
                    if (user.email !== req.body.email.trim()) {
                        return responseObject(
                            req,
                            res,
                            "",
                            responseCode.UNAUTHORIZED,
                            false,
                            res.__("INCORRECT_EMAIL")
                        );
                    }

                    if (user.isDeleted) {
                        return responseObject(
                            req,
                            res,
                            "",
                            responseCode.UNAUTHORIZED,
                            false,
                            res.__("ACCOUNT_SUSPENDED")
                        );
                    }
                    const reqPass = await crypto.createHash("md5").update(req.body.password).digest("hex");
                    const passwordreset = {
                        password: reqPass,
                    }
                    await user.update(passwordreset).then(data => {
                        return responseObject(
                            req,
                            res,
                            "",
                            responseCode.OK,
                            true,
                            res.__("PASSWORD_UPDATE_SUCCESSFULLY")
                        );
                    })
                }).catch(err => {
                    return responseObject(
                        req,
                        res,
                        "",
                        responseCode.INTERNAL_SERVER_ERROR,
                        false,
                        res.__("SERVER_ERROR")
                    );
                })

            }
        } catch (err) {

            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")
            );

        }
    },
    async resendOTP(req, res) {
        try {

        } catch (err) {

            return res.send({
                message: "Something Went Wrong"
            })
        }
    },
    async profileUpdate(req, res) {
        try {
            const userCollection = await User.findOne({
                where: {
                    id: req.user.userId
                }
            });

            if (userCollection) {
                const user = {
                    profile_img: req.body.profile_img ? req.body.profile_img : "",
                    description: req.body.description,
                    name: req.body.name
                }
                const updatedUser = await userCollection.update(user, {
                    returning: true,
                    plain: true
                });
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.OK,
                    true,
                    res.__("USER_PROFILE_UPDATE_SUCCESSFULLY")
                );
            } else {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.NOT_FOUND,
                    false,
                    res.__("DATA_NOT_FOUND")
                )
            }
        } catch (err) {
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")
            );

        }
    },
    async logout(req, res) {
        try {
            if (!req.headers["x-token"]) {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.UNAUTHORIZED,
                    false,
                    res.__("TOKEN_IS_NOT_PROVIDED")
                );
            }
            const logout = await Userlogin.destroy({
                where: {
                    loginToken: req.headers["x-token"]
                }
            })
            if (logout) {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.OK,
                    true,
                    res.__("USER_LOGOUT_SUCCESSFULLY")
                );

            } else {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.CONFLICT,
                    false,
                    res.__("ALREADY_LOGOUT")
                );
            }
        } catch (err) {

            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")
            );
        }
    },
    // ====================== user next module
    async postpicture(req, res) {
        try {
            if (req.body.image.length <= 0) {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.BAD_REQUEST,
                    false,
                    res.__("You must select at least 1 image")
                );
            }
            if (req.body.image.length > 6) {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.BAD_REQUEST,
                    false,
                    res.__("You have selected only 6 image")
                );
            }
            const countPhoto = await Postphoto.findAll({
                attributes: ['id'],
                where: {
                    userId: req.user.userId
                }
            })
            const image = [];
            for (var i = 0; i < req.body.image.length; i++) {
                const user = await Postphoto.create({
                    createdBy: req.user.userId,
                    userId: req.user.userId,
                    photoLink: req.body.image[i]
                });
                image.push(user);
            }

            const len = countPhoto.length + image.length - 6

            if (len > 0) {
                await sequelize.query(`delete FROM Postphotos  WHERE userId = ${req.user.userId} ORDER BY id ASC LIMIT ${len} `)
                return responseObject(req, res, "", responseCode.OK, true, "User post successfully");
            }
            return responseObject(req, res, "", responseCode.OK, true, "User post successfully");

        } catch (err) {
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")
            );

        }
    },
    async getNearbyUser(req, res) {
        try {
            log(`userName:${req.user.name} , lat:${req.query.lat} , long:${req.query.long}`, 'error-logs.log');
            if (req.query.lat === "null" || req.query.long === "null") {
                return responseObject(req, res, {}, responseCode.BAD_REQUEST, false, res.__("Lat and Long are required."))
            }
            if (!req.query.gender) {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.BAD_REQUEST,
                    "GENDER_IS_REQUIRED"
                )
            }
            var resp = await FILTER()
            var near = await NearByUser(req, res)

            const limit = parseInt(resp.limit)
            const offset = ((resp.page - 1) * parseInt(resp.limit))
            const orderby = resp.orderBy
            const order = resp.order
            const distance = (near.distanceForNearbyUser && parseInt(near.distanceForNearbyUser)) || resp.distance
            const lat = parseFloat(req.query.lat).toFixed(6);
            const long = parseFloat(req.query.long).toFixed(6);

            var filter
            if (req.query.gender === "male") {
                filter = '  Users.gender="Male"'
            }
            if (req.query.gender === "female") {
                filter = '  Users.gender="Female"'
            }


            const user = await sequelize.query(
                `SELECT Users.id,Users.name,Users.email,Users.gender,Users.date_of_birth,Users.username,Users.password,Users.description,CONCAT('${process.env.BASE_URL}images/userProfile/',profile_img) AS profile_img,Users.city,Users.verifyToken,Users.facebookId,Users.isVerified,Users.isDeleted,Users.lat,Users.long,Users.role,Users.createdAt,Users.updatedAt,
           (3959 * acos(
                 cos(radians(${lat}) ) * cos(radians( lat) ) * cos (radians(Users.long) - radians(${long}
         )) + sin(radians(${lat})) * sin(radians( lat )))) AS distance
         FROM Users
        HAVING  ${filter}  AND  distance < ${distance}  ORDER BY ${orderby} ${order}  LIMIT ${limit} OFFSET ${offset}`, {
                    replacements: [req.query.lat, req.query.long],
                    nest: true,
                    type: QueryTypes.SELECT
                })
            if (user.length) {
                return responseObject(
                    req,
                    res,
                    user,
                    responseCode.OK,
                    true,
                    res.__("GET_USER_SUCCESSFULLY")
                );
            } else {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.NOT_FOUND,
                    false,
                    res.__("DATA_NOT_FOUND")
                )
            }
        } catch (err) {
            console.log("======erre", err)
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")

            );
        }
    },
    async userDetails(req, res) {
        try {
            var resp = await FILTER()
            const limit = parseInt(resp.limit)
            const offset = ((resp.page - 1) * parseInt(resp.limit))
            const orderby = resp.orderBy
            const order = resp.order

            const user = await User.findOne({
                where: {
                    id: req.user.userId,
                },
                include: [{
                    model: Friends,
                    required: false,
                    where: {
                        friendId: req.user.userId,
                        status: "follow"

                    },
                    as: "friend",
                    attributes: [
                        [sequelize.fn('COUNT', sequelize.col('friend.status')), 'Followers']
                    ],

                }, ],
                attributes: ['id', 'email', 'name', 'description', 'username', 'lat', 'long', [sequelize.col('profile_img'), 'profile']],

            })
            const photo = await Postphoto.findAll({
                attributes: ['id', [sequelize.col('photoLink'), 'photo'], 'createdAt'],
                where: {
                    userId: req.user.userId
                }
            })
            user.dataValues.profile = process.env.BASE_URL + 'images/userProfile/' + user.dataValues.profile;
            for (let i = 0; i < photo.length; i++) {
                photo[i].dataValues.photo = process.env.BASE_URL + 'images/userProfile/' + photo[i].dataValues.photo;
            }
            if (user) {
                return responseObject(
                    req,
                    res, {
                        user,
                        photo
                    },
                    responseCode.OK,
                    true,
                    res.__("GET_USER_SUCCESSFULLY")
                );
            } else {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.NOT_FOUND,
                    false,
                    res.__("DATA_NOT_FOUND")
                )
            }
        } catch (err) {
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")

            );

        }
    },
    async getDetails(req, res) {
        try {
            var resp = await FILTER()
            const limit = parseInt(resp.limit)
            const offset = ((resp.page - 1) * parseInt(resp.limit))
            const orderby = resp.orderBy
            const order = resp.order
            const user = await User.findAll({
                include: [{
                    model: Friends,
                    as: "friend",
                    required: false,
                    attributes: [
                        [sequelize.fn('COUNT', sequelize.col('status')), 'Followers']
                    ],
                    where: {
                        friendId: req.user.userId,
                        status: "follow"
                    },
                }, ],
                group: ['friendId'],
                where: {
                    id: req.user.userId,
                },
                attributes: ['id', 'email', 'name', 'description', 'username', 'lat', 'long', [sequelize.col('profile_img'), 'profile']],

                raw: true,
                nest: true,

            })
            const Following = await sequelize.query(
                `SELECT 
                  COUNT(Friends.status) As Following
                FROM Friends
                 WHERE ${req.user.userId} = Friends.userId AND Friends.status = "follow"`, {
                    replacements: [],
                    nest: true,
                    type: QueryTypes.SELECT
                })
            const favourite = await sequelize.query(
                `SELECT DISTINCT  Restaurants.id,Restaurants.name,Restaurants.createdAt,
                  COUNT(Visits.restaurantId) As Favourite
                FROM Visits
                LEFT JOIN Restaurants ON Restaurants.id = Visits.restaurantId
                 WHERE ${req.user.userId} = Visits.userId  GROUP BY restaurantId ORDER BY ${orderby} ${order} LIMIT ${limit} OFFSET ${offset}`,

                {
                    replacements: [],
                    nest: true,
                    type: QueryTypes.SELECT
                })
            user.Following = Object.values(Following, favourite);
            user[0].profile = process.env.BASE_URL + 'images/userProfile/' + user[0].profile;
            if (user) {
                return responseObject(
                    req,
                    res, {
                        user: user[0],
                        Following: Following[0],
                        favourite
                    },
                    responseCode.OK,
                    true,
                    res.__("GET_USER_SUCCESSFULLY")
                );

            } else {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.NOT_FOUND,
                    false,
                    res.__("DATA_NOT_FOUND")
                )
            }
        } catch (err) {
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")
            );
        }
    },
    async loginWithFacebook(req, res) {
        try {
            const accessToken = req.body.accessToken ||"EAAEQ3TtZCXkQBAFxcHkQ5A724k43ozGtUzcQmm0ZATp1ZCQpWBDnVIZCArY816wFb6OI1VVxAfJGwBLJiHZCnQGRQ7fDOyLcxo2vZB4i2tCiCmEVI04vYTPhZCXU7ZBOLfuTwtcwb2a8e3yf6UrTA9PjBtikkIFfM6D4MlkP03qGcQROUMU4jnZCmT2vwcLiYAn2AKLZBKWyxdZAurJgsbEPnjjSNyUqPZBss8cONFLcRdDSiSqflSQ4iXGn";
            const userFromFacebook = await axios.get(
                `https://graph.facebook.com/me?access_token=${accessToken}`
            );
            const userDetails = await axios.get(
                `https://graph.facebook.com/${userFromFacebook.data?.id}?fields=id,name,email,picture&access_token=${accessToken}`
            );
            console.log(userDetails.data)
            if (!userDetails.data ?.email) {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.UNAUTHORIZED,
                    false,
                    res.__("INCORRECT_EMAIL")
                );

            }
              const user= await User.findOne({
                    where: {
                        email:userDetails.data?.email.trim(),
                        facebookId: userDetails.data?.id,
                        loginType: 1
                    }
                })
            if(!user){
                let token_create = {
                    email: userDetails.data ?.email,
                };
                const emailVerificationToken = await generateToken(token_create);
                const userInfo = {
                    name: userDetails.data.name || "",
                    isVerified: true,
                    loginType: 1,
                    facebookId: userDetails.data ?.id,
                    email: userDetails.data ?.email,
                    profile_img: userDetails ?.data ?.picture ?.data ?.url || "",
                    verifyToken: emailVerificationToken,
                };
                const fbUserCreate = await User.create(userInfo)
                return responseObject(
                    req,
                    res,
                    register,
                    responseCode.OK,
                    true,
                    res.__("CREATE_USER_SUCCESSFULLY")
                );
            }else{
                let loginToken = {
                    userId: user.id,
                    email: user.email,
                    createdAt: new Date(),
                }
                const token = await generateToken(loginToken);
                const body = {
                    userId: user.id,
                    loginToken: token,
                    deviceType: req.body.deviceType,
                    deviceToken: req.body.deviceToken,
                };
                // delete user.verifyToken;
               const userLoginFb =  await Userlogin.create(body)
                if(userLoginFb){
                    return responseObject(
                        req,
                        res,
                        userResult,
                        responseCode.OK,
                        true,
                        res.__("USER_LOGIN_SUCCESSFULLY")
                    );
                }
            }

        } catch (err) {
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")
            );
        } 
    },
}