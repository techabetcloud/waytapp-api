var express = require('express');

const  { validate } = require ('express-validation')
import * as userValidator from '../user/user.validation'
import userController from './user.controller'

var userRouter = express.Router();


userRouter.post('/upload-post',userController.postpicture)
userRouter.put('/update-profile',validate(userValidator.updateProfile),userController.profileUpdate)
userRouter.get('/get-allUser',validate(userValidator.nearbyUser),userController.getNearbyUser)
userRouter.get('/user-Details',userController.userDetails)
userRouter.get('/getDetails',userController.getDetails)









module.exports = userRouter;
