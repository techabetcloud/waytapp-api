const Sequelize = require('sequelize');

 import { User, Post_Like ,Albumb_Like,Restaurant,RestaurantPhoto,Postphoto } from '../../models'
 import { responseObject, errorResponse } from '../../helpers/responseCode';
 import { responseCode } from '../../helpers/StatusCode'
import models from "../../models";
import { FILTER , NearByUser }  from '../../helpers/setting'

var sequelize = models.sequelize;
const {
    QueryTypes,
    Op
  } = require("sequelize");
 const log = require('log-to-file');



export default {
    async postLike(req,res){
        try {
           const picture = await Postphoto.findOne({
            where:{
              id:req.body.postId,
            }})   
        if(!picture){
         return responseObject(
           req,
           res,
           "",
           responseCode.NOT_FOUND,
           false,
           res.__("USER_IMAGE_NOT_AVAILABLE")
       )}
       const likes = await Post_Like.findOne({
        where:{
         likedBY:req.user.userId,
         postId:req.body.postId
        }
      })
      if (!likes){
        await Post_Like.create({
            likedBY:req.user.userId,
            postId:req.body.postId
        })
        return responseObject(
          req,
          res,
          "",
          responseCode.OK,
          true,
          res.__("LIKE_IMAGE_SUCCESS")
          );
      }else{
          await likes.destroy({
            where:{
              likedBY:req.user.userId,
              postId:req.body.postId
             }
           })
           return responseObject(
            req,
            res,
            "",
            responseCode.OK,
            true,
            res.__("UNLIKE_IMAGE_SUCCESS")
            );
      }
        } catch (err) {
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")

              );
        }
    },
    async albumbLike(req,res){
      try {
        const Restaurants = await Restaurant.findOne({
          where:{
            id:req.body.restaurantId}
        })
         if(!Restaurants){
          return responseObject(
            req,
            res,
            "",
            responseCode.NOT_FOUND,
            true,
            res.__("RESTAURANT_NOT_FOUND")
        )
         }
        const picture = await RestaurantPhoto.findOne({
             where:{restaurantId:req.body.restaurantId,
              picture:req.body.image
            }})
         if(!picture){
          return responseObject(
            req,
            res,
            "",
            responseCode.OK,
            true,
            res.__("RESTAURANT_IMAGE_NOT_AVAIABLE")
        )}
        await Albumb_Like.create({
          userId:req.user.userId,
          restaurantId:req.body.restaurantId,
          image:req.body.image,
          likes:true
        }).then(Like =>{
          return responseObject(
              req,
              res,
              Like,
              responseCode.OK,
              true,
              res.__("LIKE_IMAGE_SUCCESS")
              );
        }).catch(err=>{
          return responseObject(
              req,
              res,
              "",
              responseCode.INTERNAL_SERVER_ERROR,
              false,
              res.__("SERVER_ERROR")
            );
        })
      } catch (err) {
          return responseObject(
              req,
              res,
              "",
              responseCode.INTERNAL_SERVER_ERROR,
              false,
              res.__("SOMETHING_WENT_WRONG")

            );
      }
    },
    async getPostLikes(req,res){
      try {
        var resp = await FILTER()
        const limit = parseInt(resp.limit)
        const offset = ((resp.page - 1) * parseInt(resp.limit))
        const orderby = resp.orderBy
        const order = resp.order
        const postdetail = await sequelize.query(
         ` SELECT  Post_Likes.postId,Post_Likes.likedBY,Postphotos.photoLink,Users.id AS userId,Users.name,CONCAT('${process.env.BASE_URL}images/userProfile/',profile_img) AS profile_img,
         COUNT(Post_Likes.postId) AS likes
         FROM Postphotos
         LEFT JOIN Post_Likes ON Post_Likes.postId = Postphotos.id
         LEFT JOIN Users ON Users.id = Postphotos.userId
         WHERE Post_Likes.postId = ${req.query.postId} GROUP BY Post_Likes.postId `,
                {
               replacements:[],
               nest: true,
               plain:true,
               type: QueryTypes.SELECT
             })
             const userdetail = await sequelize.query(
              `SELECT DISTINCT Users.id,Users.name,CONCAT('${process.env.BASE_URL}images/userProfile/',profile_img) AS profile_img,Users.createdAt,
              IF(Friends.userId IN (SELECT friendId FROM Friends where ${req.user.userId} = Friends.userId OR Friends.friendId  = ${req.user.userId} AND Friends.status = "Follow"), "Following" , "Follow") AS status

              FROM Users
              LEFT JOIN Postphotos ON  Postphotos.id = Users.id
              LEFT JOIN Post_Likes ON Post_Likes.likedBY= Users.id
              LEFT JOIN Friends ON Friends.userId = Users.id
              WHERE  Post_Likes.postId = ${req.query.postId} AND  Post_Likes.likedBY  = Users.id
                ORDER BY ${orderby} ${order}  LIMIT ${limit} OFFSET ${offset} `,
                    {
                   replacements:[req.query.postId],
                   nest: true,
                   type: QueryTypes.SELECT
                 })
       

             if(postdetail){
              return responseObject(
                  req,
                  res,
                  {postdetail,userdetail},
                  responseCode.OK,
                  true,
                  res.__("GET_USER_SUCCESSFULLY")
                  );
      }else{
          return responseObject(
              req,
              res,
              "",
              responseCode.NOT_FOUND,
              false,
              res.__("DATA_NOT_FOUND")

          )
          }
      } catch (err) {
        return responseObject(
          req,
          res,
          "",
          responseCode.INTERNAL_SERVER_ERROR,
          false,
          res.__("SOMETHING_WENT_WRONG")

        );
      }
    },
    async getNearByPost(req,res){
      try {
        log(`userName:${req.user.name} , lat:${req.body.lat} , long:${req.body.long}`, 'error-logs.log');
        if (req.body.lat === "null" || req.body.long === "null") {
          return responseObject(req, res, {}, responseCode.BAD_REQUEST, false, res.__("Lat and Long are required."))
        }
        const lat = parseFloat(req.body.lat).toFixed(6);
      const long = parseFloat(req.body.long).toFixed(6);
    
        var resp = await FILTER()
        var near = await NearByUser(req,res)
        
        const limit = parseInt(resp.limit)
        const offset = ((resp.page - 1) * parseInt(resp.limit))
        const orderby = resp.orderBy
        const order = resp.order
        const distance =  (near.distanceForNearbyUser && parseInt(near.distanceForNearbyUser)) || resp.distance
      

        const nearyby = await sequelize.query(`
              SELECT Users.id,Users.name,CONCAT('${process.env.BASE_URL}images/userProfile/',profile_img) AS profile_img,  CONCAT('${process.env.BASE_URL}images/userProfile/',photoLink)  AS photoLink,Postphotos.createdAt,
            ( SELECT COUNT(post.postId) 
            FROM Post_Likes as post
            WHERE Postphotos.id = post.postId GROUP BY post.postId) AS TotalLikes,
              (3959 * acos(
                cos(radians(${lat}) ) * cos(radians( lat) ) * cos (radians(Users.long) - radians(${long}
                )) + sin(radians(${lat})) * sin(radians( lat )))) AS distance
              FROM Users
              LEFT JOIN Postphotos ON  Postphotos.userId = Users.id
              LEFT JOIN Post_Likes ON  Post_Likes.postId = Postphotos.id
              

              WHERE  Postphotos.isDeleted = 0
              HAVING distance < ${distance}  ORDER BY ${orderby} ${order}  LIMIT ${limit} OFFSET ${offset} `,
                  {
                 replacements:[lat,long],
                 nest: true,
                 type: QueryTypes.SELECT,
               })
               if(nearyby.length){
                return responseObject(
                    req,
                    res,
                    nearyby,
                    responseCode.OK,
                    true,
                    res.__("GET_USER_POST_SUCCESSFULLY")
                    );
               }else{
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.NOT_FOUND,
                    false,
                    res.__("DATA_NOT_FOUND")
                )
                }
      } catch (err) {
        return responseObject(
          req,
          res,
          "",
          responseCode.INTERNAL_SERVER_ERROR,
          false,
          res.__("SOMETHING_WENT_WRONG")

        );
      }
    },
    async getFollowingByPost(req,res){
      try {

        var resp = await FILTER()
        const limit = parseInt(resp.limit)
        const offset = ((resp.page - 1) * parseInt(resp.limit))
        const orderby = resp.orderBy
        const order = resp.order
        const following = await sequelize.query(`
        SELECT Users.id,Users.name,CONCAT('${process.env.BASE_URL}images/userProfile/',profile_img) AS profile_img,
        CONCAT('${process.env.BASE_URL}images/userProfile/',photoLink)  AS  photoLink,Postphotos.createdAt,
        ( SELECT COUNT(post.postId) 
        FROM Post_Likes as post
        WHERE Postphotos.id = post.postId GROUP BY post.postId) AS TotalLikes

         FROM Users
        LEFT JOIN Friends ON Friends.friendId = Users.id
        LEFT JOIN Postphotos ON  Postphotos.userId = Users.id
        LEFT JOIN Post_Likes ON  Post_Likes.postId = Postphotos.id

        WHERE  Friends.userId = ${req.user.userId} AND Postphotos.isDeleted = 0 
        ORDER BY ${orderby} ${order}  LIMIT ${limit} OFFSET ${offset} `,
            {
           replacements:[],
           nest: true,
           type: QueryTypes.SELECT,
         })
        if(following.length){
          return responseObject(
              req,
              res,
              following,
              responseCode.OK,
              true,
              "GET_USER_FOLLOWING_POST_SUCCESSFULLY"
              );
              }else{
                  return responseObject(
                      req,
                      res,
                      "",
                      responseCode.NOT_FOUND,
                      false,
                      res.__("DATA_NOT_FOUND")
                    )
              }
             } catch (err) {
                 return responseObject(
                     req,
                     res,
                     "",
                     responseCode.INTERNAL_SERVER_ERROR,
                     false,
                     res.__("SOMETHING_WENT_WRONG")

                   );
      }
    },
}