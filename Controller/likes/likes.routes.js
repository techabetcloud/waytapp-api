const express = require('express');
const  { validate } = require ('express-validation')
import * as likesValidator from './likes.validation'
import likesController from './likes.controller'

const likesRouter = express.Router();

likesRouter.post('/post-like',validate(likesValidator.postLike),likesController.postLike)
likesRouter.post('/albumb-like',validate(likesValidator.albumbLike),likesController.albumbLike)
likesRouter.get('/get-postlike',validate(likesValidator.getPostLikes),likesController.getPostLikes)
likesRouter.post('/get-near-post',validate(likesValidator.nearByPost),likesController.getNearByPost)
likesRouter.get('/get-following-post',likesController.getFollowingByPost)




module.exports = likesRouter;