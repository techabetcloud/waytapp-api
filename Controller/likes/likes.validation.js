const Joi = require('joi');



export const postLike = {
    body: Joi.object().keys({
    postId:Joi.number().required().messages({"any.required": `postId is a required field`}),
  }).unknown(false).error(errors => {
    errors.forEach(err => {0
      switch (err.code) {
        case "object.allowUnknown":
          err.message = err.path[0] + ` parameter  is not allowed!`;
          break;
        default:
          break
      }
    });
    return errors;
  })
  };
  export const getPostLikes = {
    query: Joi.object().keys({
      postId:Joi.number().required()
      .messages({"any.required": `postId is a required field`,
      "string.empty": `postId is not allowed to be empty`})

  }).unknown(false).error(errors => {
    errors.forEach(err => {0
      switch (err.code) {
        case "object.allowUnknown":
          err.message = err.path[0] + ` parameter  is not allowed!`;
          break;
        default:
          break
      }
    });
    return errors;
  })
  };
  export const albumbLike = {
    body: Joi.object().keys({
    restaurantId:Joi.number().required().messages({"any.required": `restaurantId is a required field`}),
    image:Joi.string().required().messages({"any.required": `image is a required field`}),
  }).unknown(false).error(errors => {
    errors.forEach(err => {0
      switch (err.code) {
        case "object.allowUnknown":
          err.message = err.path[0] + ` parameter  is not allowed!`;
          break;
        default:
          break
      }
    });
    return errors;
  })
  };
  export const nearByPost = {
    body: Joi.object().keys({
    lat:Joi.number().required().messages({"any.required": `lat is a required field`}),
    long:Joi.string().required().messages({"any.required": `long is a required field`}),
  }).unknown(false).error(errors => {
    errors.forEach(err => {0
      switch (err.code) {
        case "object.allowUnknown":
          err.message = err.path[0] + ` parameter  is not allowed!`;
          break;
        default:
          break
      }
    });
    return errors;
  })
  };