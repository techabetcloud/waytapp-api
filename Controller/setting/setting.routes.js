const express = require('express');
const  { validate } = require ('express-validation')
import * as settingValidator from './setting.validation'
import settingController from './setting.controller'

const settingRouter = express.Router();

settingRouter.get('/get-distance',settingController.getDistanceUser)
settingRouter.post('/get-distance',validate(settingValidator.updateDistance),settingController.updateDistance)




module.exports = settingRouter;