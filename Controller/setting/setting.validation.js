const Joi = require('joi');



export const updateDistance = {
    body: Joi.object().keys({
    type:Joi.string().required().messages({"any.required": `type is a required field`}),
    value:Joi.string(),
  }).unknown(false).error(errors => {
    errors.forEach(err => {0
      switch (err.code) {
        case "object.allowUnknown":
          err.message = err.path[0] + ` parameter  is not allowed!`;
          break;
        default:
          break
      }
    });
    return errors;
  })
  };