const Sequelize = require('sequelize');

 import { User_Setting } from '../../models'
 import { responseObject, errorResponse } from '../../helpers/responseCode';
 import { responseCode } from '../../helpers/StatusCode'
import models from "../../models";

var sequelize = models.sequelize;
const {
    QueryTypes,
    Op
  } = require("sequelize");

export default {
  async getDistanceUser(req,res){
      try {
          const distance = await User_Setting.findAll({
              where:{
                  userId:req.user.userId
              }
          })
          if(distance.length){
            return responseObject(
                req,
                res,
                distance,
                responseCode.OK,
                true,
               res.__("GET_DISTANCE_SUCCESSFULLY")
                );
           }else{
            return responseObject(
                req,
                res,
                "",
                responseCode.NOT_FOUND,
                false,
                res.__("DATA_NOT_FOUND")
            )
            }
      } catch (err) {
        return responseObject(
            req,
            res,
            "",
            responseCode.INTERNAL_SERVER_ERROR,
            false,
            res.__("SOMETHING_WENT_WRONG")

          ); 
      }
  },
  async updateDistance(req,res){
      try {
          if(req.body.type == "club"){
           const distance = await User_Setting.findOne({
                where:{
                    userId:req.user.userId,
                    key:"distanceForClub"
                }
            })
            if(distance){
                const club = await distance.update({value:req.body.value})
                return responseObject(
                    req,
                    res,
                    club,
                    responseCode.OK,
                    true,
                    "SUCCESSFULLY"
                    );
            }else{
                const data = await User_Setting.create({
                    userId:req.user.userId,
                    key:"distanceForClub",
                    value:req.body.value
                })
                return responseObject(
                    req,
                    res,
                    data,
                    responseCode.OK,
                    true,
                    "SUCCESSFULLY"
                    );
            }
            
        }else if(req.body.type == "nearby"){
            const user = await User_Setting.findOne({
                where:{
                    userId:req.user.userId,
                    key:"distanceForNearbyUser"
                }
            })
            if(user){
                const Nearby = await user.update({value:req.body.value})
                return responseObject(
                    req,
                    res,
                    Nearby,
                    responseCode.OK,
                    true,
                    "SUCCESSFULLY"
                    );
                
            }else{
                const data = await User_Setting.create({
                    userId:req.user.userId,
                    key:"distanceForNearbyUser",
                    value:req.body.value
                })
                return responseObject(
                    req,
                    res,
                    data,
                    responseCode.OK,
                    true,
                    "SUCCESSFULLY"
                    );
            }
        }else{
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
               res.__("SERVER_ERROR")
              ); 
        }
      } catch (err) {
        return responseObject(
            req,
            res,
            "",
            responseCode.INTERNAL_SERVER_ERROR,
            false,
            res.__("SOMETHING_WENT_WRONG")

          ); 
      }
  },
}