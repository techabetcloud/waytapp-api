const Sequelize = require('sequelize');
const User = require('../../../models').User
var nodemailer = require('nodemailer');
import {
    where
} from 'sequelize';
import {
    responseObject,
    errorResponse
} from '../../../helpers/responseCode';
import {
    User_Login
} from '../../../models';
import {
    responseCode
} from '../../../helpers/StatusCode'
import models from "../../../models";
var sequelize = models.sequelize;

// const open = require('amqplib').connect('amqp://localhost');
const q = 'tasks';

export default {
    async sendEmail(req, res, data) {
        try {
            const message = req.body.editor1
            const heading = req.body.heading
            if (req.body.email) {
                const useremail = await User.findAll({
                    where: {
                        role: "User",
                        id: req.body.email,
                    },
                });
                const To = []
                if (useremail.length) {
                    useremail.map((item) => {
                        To.push(item.email)
                    })
                }
                const transporter = nodemailer.createTransport({
                    service: 'gmail',
                    // host: 'smtp.gmail.com',
                    // port: 465,
                    // secure: true, // use SSL
                    auth: {
                        user: `${process.env.EMAILUSER}`,
                        pass: `${process.env.EMAILPASS}`,
                    }
                });
                const option = {
                    from: 'bhandaripankaj532@gmail.com',
                    to: To,
                    subject: req.body.subject,
                    html: `<h3><b> ${heading}</b></h3> <br></br>${message}`
                };


                transporter.sendMail(option, function (err, info) {
                    if (err) {
                        req.flash('failure', "Email Failed");
                        return res.redirect('/admin/sendemail/emailids')
                        // return "Email failed.";
                    } else {
                        console.log('Message sent: ' + info.response);
                        req.flash('success', "Email send sucessfully");
                        return res.redirect('/admin/sendemail/emailids')

                    }
                });

                // Publisher
                // open.then(function (conn) {
                //     return conn.createChannel();
                // }).then(async function (ch) {
                //     const ok = await ch.assertQueue(q);
                //     return ch.sendToQueue(q, Buffer.from(JSON.stringify(option)));
                // }).catch(console.warn);


                // // Consumer
                // open.then(function (conn) {
                //     return conn.createChannel();
                // }).then(async function (ch) {
                //     const ok = await ch.assertQueue(q);
                //     return await ch.consume(q, function (msg) {
                //         if (msg !== null) {
                //             const option = JSON.parse(msg.content)
                        
                //             transporter.sendMail(option, function (err, info) {
                //                 if (err) {
                //                     req.flash('failure', "Email Failed");
                //                     return res.redirect('/admin/sendemail/emailids')
                //                     // return "Email failed.";
                //                 } else {
                //                     console.log('Message sent: ' + info.response);
                //                     req.flash('success', "Email send sucessfully");
                //                     return res.redirect('/admin/sendemail/emailids')

                //                 }
                //             });
                //             ch.ack(msg);
                //         }
                //     });
                // }).catch(console.warn);

            } else if (req.body.city) {
                const city = await User.findAll({
                    where: {
                        role: "User",
                        city: req.body.city
                    },
                });
                const To = []
                if (city.length) {

                    city.map((item) => {
                        To.push(item.email)
                    })
                }
                const transporter = nodemailer.createTransport({
                    service: 'gmail',
                    // host: 'smtp.gmail.com',
                    // port: 465,
                    // secure: true, // use SSL
                    auth: {
                        user: `${process.env.EMAILUSER}`,
                        pass: `${process.env.EMAILPASS}`,
                    }
                });

                const option = {
                    from: 'bhandaripankaj532@gmail.com',
                    to: To,
                    subject: req.body.subject,
                    html: `<h3><b> ${heading}</b></h3> <br></br> ${message}`

                };

                // Publisher
                // open.then(function (conn) {
                //     return conn.createChannel();
                // }).then(async function (ch) {
                //     const ok = await ch.assertQueue(q);
                //     return ch.sendToQueue(q, Buffer.from(JSON.stringify(option)));
                // }).catch(console.warn);


                transporter.sendMail(option, function (err, info) {
                    if (err) {
                        req.flash('failure', "Email Failed");
                        return res.redirect('/admin/sendemail/emailids')
                        // return "Email failed.";
                    } else {
                        console.log('Message sent: ' + info.response);
                        req.flash('success', "Email send sucessfully");
                        return res.redirect('/admin/sendemail/emailids')

                    }
                });
                // Consumer
                // open.then(function (conn) {
                //     return conn.createChannel();
                // }).then(async function (ch) {
                //     const ok = await ch.assertQueue(q);
                //     return await ch.consume(q, function (msg) {
                //         if (msg !== null) {
                //             console.log(msg.content.toString());
                //             const option = JSON.parse(msg.content)
                //             transporter.sendMail(option, function (err, info) {
                //                 if (err) {
                //                     req.flash('failure', "Email Failed");
                //                     return res.redirect('/admin/sendemail/emailids')
                //                     // return "Email failed.";
                //                 } else {
                //                     console.log('Message sent: ' + info.response);
                //                     req.flash('success', "Email send sucessfully");
                //                     return res.redirect('/admin/sendemail/emailids')

                //                 }
                //             });
                //             ch.ack(msg);
                //         }
                //     });
                // }).catch(console.warn);
            }
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")
        }
    },
    async emailId(req, res) {
        try {
            const useremail = await User.findAll({
                raw: true,
            });
            const city = await User.findAll({
                attributes: [
                    [Sequelize.fn('DISTINCT', Sequelize.col('city')), 'city', ]
                ],
                where: {
                    role: "User"
                }
            });
            return res.render('admin/emails/create', {
                useremail: useremail,
                city: city,
                success: await req.consumeFlash('success'),
                failure: await req.consumeFlash('failure')
            });

        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
}