const express = require('express');
import sendEmailController from './sendEmailController';
import authController from '../auth/auth.controller';
export const emailRoutes = express.Router()


// emailRoutes.post('/send',authController.isLoggedIn,sendEmailController.sendEmail)
emailRoutes.get('/emailids',authController.isLoggedIn,sendEmailController.emailId)
emailRoutes.get('/email',authController.isLoggedIn,(req,res)=> { 
    res.render('admin/emails/create')});


