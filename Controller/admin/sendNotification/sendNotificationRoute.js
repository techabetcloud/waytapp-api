const express = require('express');
import sendNotificationController from './sendNotificationController';
import authController from '../auth/auth.controller';
export const sendNotificationRoutes = express.Router()



sendNotificationRoutes.get('/notification',authController.isLoggedIn,sendNotificationController.Notification)
sendNotificationRoutes.post('/senNotification',authController.isLoggedIn,sendNotificationController.senNotification)