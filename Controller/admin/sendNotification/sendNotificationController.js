const Sequelize = require('sequelize');
const User = require('../../../models').User
var nodemailer = require('nodemailer');
import { where } from 'sequelize';
import {
    responseObject,
    errorResponse
} from '../../../helpers/responseCode';
import {
    User_Login
} from '../../../models';
import {
    responseCode
} from '../../../helpers/StatusCode'
import models from "../../../models";
import  sendNotification   from '../../../helpers/email.service'

var sequelize = models.sequelize;


export default {
async Notification(req,res){
    try {
        const city = await User.findAll({
            attributes: [
                [Sequelize.fn('DISTINCT', Sequelize.col('city')) ,'city',]
            ],
            where:{
                role:"User"
            }
        });
       return res.render('admin/sendNotification/create',{
              city:city,
              success: await req.consumeFlash('success'),
              failure: await req.consumeFlash('failure')
       });
    } catch (err) {
        return responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")
    }
},
async senNotification(req,res){
    try {
        const user =  await User.findAll({
            where:{
                city:req.body.city
                  }
        })
        const rest = []
        if (user.length) {
            user.map((item) => {
                rest.push(item.id)
            })
        }
        const login =  await User_Login.findAll({
               where:{
                   userId:rest,
                   deviceType:req.body.deviceType,
               }
        })
        const resp = []
        if (login.length) {
            login.map((element) => {
                resp.push(element.deviceToken)
            })
        }
        const notification = {
            token:resp,
            title: req.body.Title,
            data:req.body.message
             }
        const sendPushNotificaton = await sendNotification.firebaseService(notification)
        if(sendPushNotificaton) {
            req.flash('success', "Notification send sucessfully");
        }else{
            req.flash('failure', "notification Failed");
        }
        return res.redirect('/admin/sendnotification/notification')
    } catch (err) {
       return  responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")
    }
},
}