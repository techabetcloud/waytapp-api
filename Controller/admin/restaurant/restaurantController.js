const Sequelize = require('sequelize');
const Restaurant = require('../../../models').Restaurant
import {
    RestaurantPhoto,
    WeekDay,
    Restaurant_Schedule,
    Restaurant_closing_days,
    Restaurant_Categorie
} from '../../../models';
import {
    responseObject,
    errorResponse
} from '../../../helpers/responseCode';
import {
    responseCode
} from '../../../helpers/StatusCode'
import models from "../../../models";
import { data } from 'jquery';
var sequelize = models.sequelize;
const {
    Op
} = require("sequelize");
var moment=require('moment')

// const Restaurant = require('../../../models').Restaurant

export default {
    async create(req, res, ) {
     
        try {

            const days = await WeekDay.findAll({
                raw: true
            });
            const categoryID = await Restaurant_Categorie.findAll({
                raw: true,
            });
     
            return res.render('admin/restaurant/create', {
                days: days,
                category: categoryID
            });
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },

    async save(req, res) {
        try {
            const restrauntInfo = {
                categoryId: req.body.category,
                name: req.body.name,
                address: req.body.address,
                city: req.body.city,
                lat: req.body.lat,
                long: req.body.long,
                description: req.body.description,
                link: req.body.link1,
                status: req.body.status,
                drinks_allowed: req.body.drinks,
            }
            let weekId =req.body.weekId
            let From =req.body.From
            let To = req.body.To
            var items = weekId.map(( id, index) => {
                let ids = parseInt(id) - 1
                 return {
                    weekDayId: id,
                    from_date:  From[ids],
                    to_date:To[ids]
                 }
               });
            const clubBar = await Restaurant.create(restrauntInfo)
            if (clubBar) {
                let response = []
                let days = []
                for (let i = 0; i < req.files.length; i++) {
                    let data = {}
                    data = {
                        restaurantId: clubBar.id,
                        picture: req.files[i].originalname
                    }
                    response.push(data)
                }
                if (items) {
                    items.map((el, index) => {
                     el.restaurantId = clubBar.id
                    })
                    Promise.allSettled([
                        await Restaurant_Schedule.bulkCreate(items)
                    ]).then(async data => {
                        await RestaurantPhoto.bulkCreate(response)
                    })
                } else {
                    Promise.allSettled([
                        await RestaurantPhoto.bulkCreate(response),
                    ]).then(data => {
                    })
                }
                req.flash('success', "Restaurant added sucessfully");
            } else {
                req.flash('failure', "Failed to add restaurant");

                responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")
            }

            return res.redirect("/admin/restaurants/list");
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }

    },
    async list(req, res, ) {
        try {
            return res.render('admin/restaurant/listing', {
                data: '',
                success: await req.consumeFlash('success'),
                failure: await req.consumeFlash('failure')
            });
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },

    async delete(req, res, ) {
        try {
            let data = await Restaurant.destroy({
                where: {
                    id: req.params.id
                }
            });
            if (data) {
                return res.json({
                    success: true
                })
            } else {
                return res.json({
                    success: false
                })
            }
            // return res.redirect('/admin/clubBar/list');
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },


    async ajaxList(req, res, ) {
        try {
            var pagno = req.query.start / req.query.length + 1
            var page = parseInt(req.query.draw) || 1;
            var limit = parseInt(req.query.length) || 2;
            let searchString = req.query.search.value || ''
            // var order = req.query.order
            let order = []
            if (req.query.order[0].column == '0') {
                order = [
                    ["name", req.query.order[0].dir]
                ]
            }
           else if (req.query.order[0].column == '1') {
                order = [
                    ["category", "name", req.query.order[0].dir]
                ]
            } else  if (req.query.order[0].column == '2') {
                order = [
                    ["address", req.query.order[0].dir]
                ]
            } else if (req.query.order[0].column == '3') {
                order = [
                    ["city", req.query.order[0].dir]
                ]
            } else {
                order = [
                    ["createdAt", req.query.order[0].dir]
                ]
            }
            var filter = {
                include: [{
                    model: Restaurant_Categorie,
                    as: "category"
                }],
                offset: ((pagno - 1) * limit),
                limit: limit,
                order: order,
            }

            var filter1 = {

                raw: true
            }
            if (req.query.search.value) {
                filter.where = {
                    [Op.or]: {
                        '$category.name$': {
                            [Op.like]: '%' + req.query.search.value + '%'
                        },
                        name: {
                            [Op.like]: '%' + req.query.search.value + '%'
                        },
                        address: {
                            [Op.like]: '%' + req.query.search.value + '%'
                        },
                        city: {
                            [Op.like]: '%' + req.query.search.value + '%'
                        },
                    }
                }
            }

            const restrauntInfo = await Restaurant.findAll(filter)
            const totaldata = await Restaurant.findAll(filter1)

            return res.json({
                draw: page,
                recordsTotal: totaldata.length,
                recordsFiltered: totaldata.length,
                data: restrauntInfo
            })


        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },

    async edit(req, res, ) {
        try {
            const editRestaurant = await Restaurant.findOne({
                where: {
                    id: req.params.id
                },
                include: [

                    {
                        model: Restaurant_Schedule,
                        as: "restaurantOpen",
                        attributes: ['id', 'restaurantId', 'weekDayId']

                    }, {
                        model: RestaurantPhoto,
                        as: "restaurantPhoto",
                        attributes: ['id', 'restaurantId', 'picture']
                    }
                ],
            });
            const restronCategory = await Restaurant_Categorie.findAll({
                raw: true,
            });
            const days = await Restaurant_Schedule.findAll({
                where: {
                    restaurantId: req.params.id
                },
                raw: true,
            });
            const weekday = await WeekDay.findAll({
                raw: true,
            });
            const rest = []
            if (days.length) {
                days.map((item) => {
                    rest.push(item.weekDayId)
                })
            }
            return res.render('admin/restaurant/edit', {
                editRestaurant: editRestaurant,
                restronCategory: restronCategory,
                selectdays: rest,
                weekday: weekday,
                days:days
            });
        } catch (err) {
            
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async update(req, res) {
        var id = req.body.test.split(",")
        try {
            const restron = await Restaurant.findOne({
                where: {
                    id: req.params.id
                }
            });
            const rmvClsDys = await Restaurant_Schedule.destroy({
                where: {
                    restaurantId: req.params.id

                }
            })
            const restroImg = await RestaurantPhoto.destroy({
                where: {
                    // restaurantId:req.params.id
                    // {
                    id: {
                        [Op.in]: id
                    }

                    //     // req.params.id
                    // }
                }
            })

            let weekId =req.body.weekId
            let From =req.body.From
            let To = req.body.To
            var items = weekId.map(( id, index) => {
                let ids = parseInt(id) - 1
                 return {
                    weekDayId: id,
                    from_date:  From[ids],
                    to_date:To[ids]
                 }
               });

            if (restron) {
                let response = []
                let days = []

                for (let i = 0; i < req.files.length; i++) {
                    let data = {}
                    data = {
                        restaurantId: restron.id,
                        picture: req.files[i].originalname
                    }
                    response.push(data)
                }

                if (items) {
                    items.map((el, index) => {
                        el.restaurantId = restron.id
                       })
                    Promise.allSettled([
                        await Restaurant_Schedule.bulkCreate(items)
                    ]).then(async data => {
                        await RestaurantPhoto.bulkCreate(response)
                    })
                } else {
                    Promise.allSettled([
                        await RestaurantPhoto.bulkCreate(response),
                    ]).then(data => {
                    })
                }
                const restronUpdate = await restron.update(req.body, {});
                req.flash('success', "User updated sucessfully");
            } else {
                req.flash('failure', "Updation Failed");
            }
            return res.redirect('/admin/restaurants/list')
        } catch (err) {
               console.log(err)
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async restaurantDetail(req, res, ) {
        try {
            const restronDtail = await Restaurant.findOne({
                where: {
                    id: req.params.id
                },
                include: [{
                        model: Restaurant_Categorie,
                        as: "category",
                        attributes: ['id', 'name', ]

                    },
                    {
                        model: Restaurant_Schedule,
                        as: "restaurantOpen",
                        attributes: ['id', 'restaurantId', 'weekDayId','from_date','to_date'],
                    }, {
                        model: RestaurantPhoto,
                        as: "restaurantPhoto",
                        attributes: ['id', 'restaurantId', 'picture']
                    }

                ],
            });
         

            const arr = [];
            restronDtail.restaurantOpen.map(el => {
                arr.push(el.weekDayId)
            })

            const cls = await WeekDay.findAll({
                where: {
                    id: {
                        [Op.in]: arr
                    }
                }
            })
            return res.render('admin/restaurant/restaurantDetail', {
                restronDtail: restronDtail,
                closeDay: cls

            });

        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
}