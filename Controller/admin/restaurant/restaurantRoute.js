const express = require('express');
import multer  from 'multer'
import restaurantController from './restaurantController';
import authController from '../auth/auth.controller';
import { restaurantStorage } from '../../../generic';
export const restaurantsRoutes  = express.Router()
var restaurantUpload=multer({storage:restaurantStorage })


restaurantsRoutes.get('/add',authController.isLoggedIn,restaurantController.create);
restaurantsRoutes.post('/save',authController.isLoggedIn,restaurantUpload.array('picture',4),restaurantController.save);
restaurantsRoutes.get('/list',authController.isLoggedIn,restaurantController.list);
restaurantsRoutes.get('/ajax-listing',authController.isLoggedIn,restaurantController.ajaxList);
restaurantsRoutes.get('/edit/:id',authController.isLoggedIn,restaurantController.edit);
restaurantsRoutes.post('/update/:id',authController.isLoggedIn,restaurantUpload.array('picture',4),restaurantController.update);
restaurantsRoutes.get('/delete/:id',authController.isLoggedIn,restaurantController.delete);
restaurantsRoutes.get('/view/:id',authController.isLoggedIn,restaurantController.restaurantDetail);

// restaurantsRoutes.get('/',authController.isLoggedIn,(req,res)=> { 
//     res.render('')});