const express = require('express');
import drinkRestaurantController from './drinkRestaurantController';
import authController from '../auth/auth.controller';
export const drinkRestaurantRoutes  = express.Router() 

drinkRestaurantRoutes.get('/add',authController.isLoggedIn,drinkRestaurantController.create);
drinkRestaurantRoutes.post('/save',authController.isLoggedIn,drinkRestaurantController.save);
drinkRestaurantRoutes.get('/list',authController.isLoggedIn,drinkRestaurantController.list);
drinkRestaurantRoutes.get('/listing',authController.isLoggedIn,drinkRestaurantController.listing);
drinkRestaurantRoutes.get('/delete/:id',authController.isLoggedIn,drinkRestaurantController.delete);
drinkRestaurantRoutes.get('/edit/:id',authController.isLoggedIn,drinkRestaurantController.edit);
drinkRestaurantRoutes.post('/update/:id',authController.isLoggedIn,drinkRestaurantController.update);

