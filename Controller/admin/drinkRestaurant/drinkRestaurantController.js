const Sequelize = require('sequelize');
import slugify from 'slugify';
import {
    Drink,Restaurant,Restaurant_Drink

} from '../../../models';
import {
    responseObject,
    errorResponse
} from '../../../helpers/responseCode';
import {
    responseCode
} from '../../../helpers/StatusCode'
import models from "../../../models";
import { readdirSync } from 'fs';
import restaurant from '../../../models/restaurant';
import { updateLocale } from 'moment-timezone';
var sequelize = models.sequelize;
const {
    Op
} = require("sequelize");


export default {
    async create(req, res, ) {
        try {
            const restaurantInfo= await Restaurant.findAll({});
            const drinkInfo= await Drink.findAll({});
         
             return res.render('admin/drinksRestaurant/create',{restaurantInfo:restaurantInfo,drinkInfo:drinkInfo});
        } catch (err) {
          
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async save(req, res, ) {
        try {
           
               let drink = req.body.drink
               let response = []
               for (let i = 0; i < drink.length; i++) {
                   const drinkForRestaurant = await Restaurant_Drink.create({
                    restaurantId: req.body.restaurant,
                       drinkId: drink[i]
                   });
                   response.push(drinkForRestaurant)
                }
                if(response){
                    req.flash('success', "Drink for Restaurant added sucessfully");     
    
                   }
                   else{
                    req.flash('failure', "Failed to add Drink for Restaurant ");
    
                   }
            

             return res.redirect('/admin/restaurantDrink/list')

        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async list(req, res, ) {
        try {
             return res.render('admin/drinksRestaurant/listing',{ 
                success: await req.consumeFlash('success'),
                failure: await req.consumeFlash('failure')})
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async listing(req, res, ) {
        try {
            
           
            var pagno = req.query.start / req.query.length + 1
            var page = parseInt(req.query.draw) || 1;
            var limit = parseInt(req.query.length) || 2;
            let searchString = req.query.search.value || ''
            let order  = []
            if(req.query.order[0].column == '0'){
                order = [["restaurantId", req.query.order[0].dir]]
            }else if(req.query.order[0].column == '1'){
                order = [["drinkId", req.query.order[0].dir]]
            }
            else{
                order = [["createdAt", req.query.order[0].dir]]
            }
            var filter = {
                offset: ((pagno - 1) * limit),
                limit: limit,
                order:order,
                include:[{
                model:Restaurant,
                as:"restaurant",
                
                },
                {
                    model:Drink,
                    as:"drink",
                }
            ],
              
            }

            var filter1 = {

                raw: true
            }
            
                if (req.query.search.value) {
                    filter.where =  {[Op.or]: {
                        restaurantId: {[Op.like]: '%'+req.query.search.value+'%'},
                        drinkId: {[Op.like]: '%'+req.query.search.value+'%'},
                        } 
                        }
                    }
            
            const restaurantDrinkInfo = await Restaurant_Drink.findAll(filter)
            const totaldata = await Restaurant_Drink.findAll(filter1)

            return res.json({
                draw: page,
                recordsTotal: totaldata.length,
                recordsFiltered: totaldata.length,
                data: restaurantDrinkInfo
            })

         
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async delete(req, res, ) {
        try {
            let data = await Restaurant_Drink.destroy({
                where: {
                    id: req.params.id
                }
            });
            if (data) {
                return res.json({
                    success: true
                })
            } else {
                return res.json({
                    success: false
                })
            }
           
         
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async edit(req,res){
        try {
            const editrestaurantdrink = await Restaurant_Drink.findOne({
                where:{
                    id:req.params.id
                },
                include: [
                    {
                    model:Restaurant,
                    as: "restaurant",
                   },
                   {
                    model:Drink,
                    as: "drink",
                   }
            ],
            })
            const drink = await Drink.findAll({
                raw: true,
            });
            const restaurant = await Restaurant.findAll({
                
            });
            return res.render('admin/drinksRestaurant/edit',{
                editrestaurantdrink:editrestaurantdrink,
                drink:drink,
                restaurant:restaurant
            });
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async update(req,res){
        try {
           
            const editrestaurantdrink = await Restaurant_Drink.findOne({
                where:{
                    id:req.params.id
                },
               
            })
            if(editrestaurantdrink){
                                 const drinkrestaurant={
                                restaurantId:req.body.restaurant,
                                 drinkId:req.body.drink,
                                 }
                const updatesubcategory = await editrestaurantdrink.update(drinkrestaurant,{});
               
                if(updatesubcategory) {
                    req.flash('success', "Event updated sucessfully");
                }else{
                    req.flash('failure', "Updation Failed");
                }
    
            }
            return res.redirect('/admin/restaurantDrink/list');
        } catch (err) {
            
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")

        }
    },
}