const express = require('express');
import multer from 'multer';
import drinkCategorieController from './drinkCategorieController';
import authController from '../auth/auth.controller';
import { drinksCategoryStorage } from '../../../generic';
export const drinkCategoreiRoutes  = express.Router()
var drinksCategoryUpload = multer({storage:drinksCategoryStorage })


drinkCategoreiRoutes.get('/add',authController.isLoggedIn,drinkCategorieController.create);
drinkCategoreiRoutes.post('/save',authController.isLoggedIn,drinksCategoryUpload.single('image'),drinkCategorieController.save);
drinkCategoreiRoutes.get('/list',authController.isLoggedIn,drinkCategorieController.list);
drinkCategoreiRoutes.get('/listing',authController.isLoggedIn,drinkCategorieController.listing);
drinkCategoreiRoutes.get('/delete/:id',authController.isLoggedIn,drinkCategorieController.delete);
drinkCategoreiRoutes.get('/edit/:id',authController.isLoggedIn,drinkCategorieController.edit);
drinkCategoreiRoutes.post('/update/:id',authController.isLoggedIn,drinksCategoryUpload.single('image'),drinkCategorieController.update);
