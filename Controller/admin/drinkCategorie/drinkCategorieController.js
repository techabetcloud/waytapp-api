const Sequelize = require('sequelize');
import slugify from 'slugify';
import {
    Drink_Categorie,
} from '../../../models';
import {
    responseObject,
    errorResponse
} from '../../../helpers/responseCode';
import {
    responseCode
} from '../../../helpers/StatusCode'
import models from "../../../models";
import { readdirSync } from 'fs';
var sequelize = models.sequelize;
const {
    Op
} = require("sequelize");




export default {
    async create(req, res, ) {
        try {
            
           
         
             return res.render('admin/drinkCategorie/create',{
             success: await req.consumeFlash('success'),
             failure: await req.consumeFlash('failure')})
        } catch (err) {
           
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async save(req, res, ) {
        try {

            
        const fndCtry = await Drink_Categorie.findOne({where:{
            name: req.body.type
        }})
        if(fndCtry) {
          req.flash('failure', "Drink Categorie already exist"); 
            return res.redirect("/admin/drinkCategorie/add")
        }
            const drinkType={
            name :req.body.type,
            slug : slugify(req.body.type, "-"),
            description:req.body.description,
            image: req.file.originalname,
           }
           const typeInfo = await Drink_Categorie.create(drinkType);
           if(!typeInfo){
            req.flash('failure', "Failed to add Drink Categorie"); 
           }
           else{
            req.flash('success', "Drink Categorie added sucessfully");     
           }
       return res.redirect("/admin/drinkCategorie/list");
        } catch (err) {
         
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async list(req, res, ) {
        try {
             return res.render('admin/drinkCategorie/listing',{
                success: await req.consumeFlash('success'),
                failure: await req.consumeFlash('failure')
            })
        } catch (err) {
      
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async listing(req, res, ) {
        try {
            var pagno = req.query.start / req.query.length + 1
            var page = parseInt(req.query.draw) || 1;
            var limit = parseInt(req.query.length) || 2;
            let searchString = req.query.search.value || ''
        
            let order  = []
            if(req.query.order[0].column == '0'){
                order = [["name", req.query.order[0].dir]]
            }else if(req.query.order[0].column == '2'){
                order = [["description", req.query.order[0].dir]]
            }
            else{
                order = [["createdAt", req.query.order[0].dir]]
            }
            var filter = {
                offset: ((pagno - 1) * limit),
                limit: limit,
                order:order,
                raw: true
            }

            var filter1 = {

                raw: true
            }
            
                if (req.query.search.value) {
                    filter.where =  {[Op.or]: {
                            name: {[Op.like]: '%'+req.query.search.value+'%'},
                            description: {[Op.like]: '%'+req.query.search.value+'%'},
                        } 
                        }
                    }
            
            const drinksCategoryInfo = await Drink_Categorie.findAll(filter)
            const totaldata = await Drink_Categorie.findAll(filter1)

            return res.json({
                draw: page,
                recordsTotal: totaldata.length,
                recordsFiltered: totaldata.length,
                data: drinksCategoryInfo
            })

         
        } catch (err) {
      
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async delete(req, res, ) {
        try {
            let data = await Drink_Categorie.destroy({
                where: {
                    id: req.params.id
                }
            });
            if (data) {
                return res.json({
                    success: true
                })
            } else {
                return res.json({
                    success: false
                })
            }
           
         
        } catch (err) {
        
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
  async edit(req,res){
      try {
        const editcategory = await Drink_Categorie.findOne({
            where:{
                id:req.params.id
            }
        })   
   
        return res.render('admin/drinkCategorie/edit',{
            editcategory:editcategory
        });
      } catch (err) {
        
        responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")

      }
  },
  async update(req,res){
      try {
       
          const category =  await Drink_Categorie.findOne({
            where:{
                id:req.params.id
            }
        })
        if(category){
            const drinkType={
                name :req.body.type,
                slug : slugify(req.body.type, "-"),
                description:req.body.description,
                image: req.file.originalname,
               }
            const updatecategory = await category.update(drinkType,{});
            
            if(updatecategory) {
                req.flash('success', "Event updated sucessfully");
            }else{
                req.flash('failure', "Updation Failed");
            }

        }
        return res.redirect('/admin/drinkCategorie/list');

      } catch (err) {
        responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")
      }
  }
}