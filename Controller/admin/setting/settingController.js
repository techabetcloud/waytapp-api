const Sequelize = require('sequelize');
import {
    Setting
} from '../../../models';
import {
    responseObject,
    errorResponse
} from '../../../helpers/responseCode';
import {
    responseCode
} from '../../../helpers/StatusCode'
import models from "../../../models";
var sequelize = models.sequelize;
const {
    Op
} = require("sequelize");

export default {
    async list(req, res, ) {
        try {
            return res.render('admin/setting/listing', {
                data: '',
                success: await req.consumeFlash('success'),
                failure: await req.consumeFlash('failure')
            });
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async ajaxList(req, res, ) {
        try {
          
    
            // var order = req.query.order
            let order = []
            if (req.query.order[0].column == '0') {
                order = [
                    ["name", req.query.order[0].dir]
                ]
            } else if (req.query.order[0].column == '1') {
                order = [
                    ["key", req.query.order[0].dir]
                ]
            } else if (req.query.order[0].column == '2') {
                order = [
                    ["value", req.query.order[0].dir]
                ]
            } else {
                order = [
                    ["createdAt", req.query.order[0].dir]
                ]
            }
            var filter = {
                order: order,
            }

            var filter1 = {

                raw: true
            }
            if (req.query.search.value) {
                filter.where = {
                    [Op.or]: {
                        name: {
                            [Op.like]: '%' + req.query.search.value + '%'
                        },
                        key: {
                            [Op.like]: '%' + req.query.search.value + '%'
                        },
                        value: {
                            [Op.like]: '%' + req.query.search.value + '%'
                        },
                    }
                }
            }

            const setting = await Setting.findAll(filter)
            const totaldata = await Setting.findAll(filter1)

            return res.json({
             
                recordsTotal: totaldata.length,
                recordsFiltered: totaldata.length,
                data: setting
            })


        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async create(req, res, ) {
        try {
            return res.render('admin/setting/edit', {
              
            });
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async edit(req, res, ) {
        try {
            
            const setting = await Setting.findOne({
                where:{
                    id:req.params.id
                },
                raw: true,
            });
            return res.render('admin/setting/edit', {
                setting:setting,
            });
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async update (req,res){
        try {
            
            const setting = await Setting.findOne({
                where:{
                    id:req.params.id
                }
              
            });
            
              if(setting){
                delete req.body.key
                const updateSetting = await setting.update(req.body,{
                });
                    if(updateSetting) {
                        req.flash('success', "Event updated sucessfully");
                    }else{
                        req.flash('failure', "Updation Failed");
                    }
            }
            return res.redirect('/admin/setting/list');
             
        } catch (err) {
          responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")
        }
    },
}