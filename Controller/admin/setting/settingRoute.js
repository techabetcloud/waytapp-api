const express = require('express');
import settingController from './settingController';
import authController from '../auth/auth.controller';
export const settingRoutes  = express.Router()




settingRoutes.get('/list',authController.isLoggedIn,settingController.list);
settingRoutes.get('/ajax-listing',authController.isLoggedIn,settingController.ajaxList);
settingRoutes.get('/add',authController.isLoggedIn,settingController.create);
settingRoutes.get('/edit/:id',authController.isLoggedIn,settingController.edit);
settingRoutes.post('/update/:id',authController.isLoggedIn,settingController.update);



