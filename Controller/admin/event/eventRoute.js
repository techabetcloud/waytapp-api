const express = require('express');
import multer  from 'multer'
import eventController from './eventController';
import authController from '../auth/auth.controller';
import { eventStorage } from '../../../generic';
export const eventRoutes  = express.Router()
var eventUpload = multer({storage:eventStorage})



eventRoutes.get('/add',authController.isLoggedIn,eventController.create);
eventRoutes.post('/save',authController.isLoggedIn,eventUpload.single('picture'),eventController.save);
eventRoutes.get('/list',authController.isLoggedIn,eventController.list);
eventRoutes.get('/listing',authController.isLoggedIn,eventController.listing);
eventRoutes.get('/delete/:id',authController.isLoggedIn,eventController.delete);
eventRoutes.get('/edit/:id',authController.isLoggedIn,eventController.edit);
eventRoutes.post('/update/:id',authController.isLoggedIn,eventUpload.single('picture'),eventController.update);
eventRoutes.get('/view/:id',authController.isLoggedIn,eventController.eventDetail);
