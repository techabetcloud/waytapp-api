const Sequelize = require('sequelize');
import {
    Event,Restaurant
} from '../../../models';
import {
    responseObject,
    errorResponse
} from '../../../helpers/responseCode';
import {
    responseCode
} from '../../../helpers/StatusCode'
import models from "../../../models";
var sequelize = models.sequelize;
var moment=require('moment')
const {
    Op
} = require("sequelize");

export default {
    async create(req, res, ) {
        try {
            const restaurantInfo= await Restaurant.findAll({});
            return res.render('admin/event/create',{restaurantInfo:restaurantInfo});
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },

    async list (req, res, ) {
        try {
                    
            return res.render('admin/event/listing',{
                success: await req.consumeFlash('success'),
                failure: await req.consumeFlash('failure')
            });
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },


    async save(req, res,) {
        try {
           
const datepicker= req.body.daterange.toString().split('-')
        const eventInfo={
            name: req.body.name,
            restaurantId:req.body.restaurant,
            address: req.body.address,
            country: req.body.country,
            city: req.body.city,
            lat: req.body.latitude,
            long: req.body.longitude,
            picture: req.file.originalname,
            description: req.body.description,
            from_date: moment(datepicker[0]).format('YYYY-MM-DDThh:mm:ss.ms'),
            to_date: moment(datepicker[1]).format('YYYY-MM-DDThh:mm:ss.ms'),
         
        }
          const event= await Event.create(eventInfo);
          if(event){
            req.flash('success', "Event added sucessfully");     
          }
          else{
            req.flash('failure', "Failed to add event");     
 
          }
            return res.redirect('/admin/event/list');
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },

    async delete(req, res) {
        try {
            let data = await Event.destroy({
                where: {
                    id: req.params.id
                }
            });
            if (data) {
                return res.json({
                    success: true
                })
            } else {
                return res.json({
                    success: false
                })
            }
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }

    },
    async listing(req, res, ) {
        try {
            var pagno = req.query.start / req.query.length + 1
            var page = parseInt(req.query.draw) || 1;
            var limit = parseInt(req.query.length) || 2;
            let searchString = req.query.search.value || ''
            let order  = []
            if(req.query.order[0].column == '1'){
                order = [["name", req.query.order[0].dir]]
            }else if (req.query.order[0].column == '2'){
                order = [
                    ["restaurant", "name", req.query.order[0].dir]
                ]
            }else if(req.query.order[0].column == '3'){
                order = [["country", req.query.order[0].dir]]
            }else if(req.query.order[0].column == '4'){
                order = [["city", req.query.order[0].dir]]
            }else if(req.query.order[0].column == '5'){
                order = [["from_date", req.query.order[0].dir]]
            }
            else{
                order = [["createdAt", req.query.order[0].dir]]
            }

            var filter = {
                offset: ((pagno - 1) * limit),
                limit: limit,
                order: order,
                include:[{
                    model:Restaurant,
                    as:"restaurant",
                    
                    }],
            }

            var filter1 = {

                raw: true
            }
            if (req.query.search.value) {
                filter.where =  {[Op.or]: {
                    '$restaurant.name$': {
                        [Op.like]: '%' + req.query.search.value + '%'
                    },
                    name: {[Op.like]: '%'+req.query.search.value+'%'},
                    country: {[Op.like]: '%'+req.query.search.value+'%'},
                    city: {
                        [Op.like]: '%' + req.query.search.value + '%'
                    },
                    } 
                    }
                }
            
            const eventInfo = await Event.findAll(filter)
         eventInfo.from_date= moment(eventInfo.from_date).format('MM/DD/YYYY h:mm A');
         
            const totaldata = await Event.findAll(filter1)
     
            return res.json({
                draw: page,
                recordsTotal: totaldata.length,
                recordsFiltered: totaldata.length,
                data: eventInfo,
            })


        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
  async edit(req,res){
      try {
          const editevent = await Event.findOne({
              where:{
                  id:req.params.id
              },
              include: [
                {
                model:Restaurant,
                as: "restaurant",
                attributes:["id","name"]
               },
              ]
          })
          const restaurantInfo = await Restaurant.findAll({
                
        });

        editevent.from = moment(editevent.from_date).format('MM/DD/YYYY h:mm A')
        editevent.to = moment(editevent.to_date).format('MM/DD/YYYY h:mm A')
          return res.render('admin/event/edit',{
            editevent:editevent,restaurantInfo:restaurantInfo
        });
      } catch (err) {
        responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")

      }
  },
  async update (req,res){
      try {
        const event = await Event.findOne({
            where:{
                id:req.params.id
            },
        })
        if(event){
            const datepicker= req.body.daterange.toString().split('-')
 
            const eventInfo={
                name: req.body.name,
                restaurantId:req.body.restaurant,
                address: req.body.address,
                country: req.body.country,
                city: req.body.city,
                lat: req.body.latitude,
                long: req.body.longitude,
                // picture: req.file.originalname,
                description: req.body.description,
                from_date: moment(datepicker[0]).format('YYYY-MM-DDThh:mm:ss.ms'),
                to_date: moment(datepicker[1]).format('YYYY-MM-DDThh:mm:ss.ms'),
             
            }
            if(req.file){
                eventInfo.picture = req.file.originalname
            }
            const updateEvent = await event.update(eventInfo,{});

            if(updateEvent) {
                req.flash('success', "Event updated sucessfully");
            }else{
                req.flash('failure', "Updation Failed");
            }
        }

        return res.redirect('/admin/event/list');

      } catch (err) {
        responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")
      }
  },
  async eventDetail (req, res, ) {
    try {
        const eventInfo = await Event.findOne({
            where:{
                id:req.params.id
            },include: [
                {
                model:Restaurant,
                as: "restaurant",
                attributes:["id","name"]
               },
              ]
        })
        eventInfo.from = moment(eventInfo.from_date).format('MM/DD/YYYY h:mm A')
        eventInfo.to = moment(eventInfo.to_date).format('MM/DD/YYYY h:mm A')
        return res.render('admin/event/eventDetail',{
            eventInfo:eventInfo
      });    
        
    } catch (err) {
        responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


    }
},
}