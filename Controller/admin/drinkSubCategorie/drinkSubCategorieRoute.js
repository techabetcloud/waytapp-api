const express = require('express');
import multer from 'multer';
import drinkSubCategorieController from './drinkSubCategorieController';
import authController from '../auth/auth.controller';
import { drinkSubCategoryStorage } from '../../../generic';
export const drinkSubCategoreiRoutes  = express.Router()
var drinkSubCategoryUpload = multer({storage:drinkSubCategoryStorage })

drinkSubCategoreiRoutes.get('/add',authController.isLoggedIn,drinkSubCategorieController.create);
drinkSubCategoreiRoutes.post('/save',authController.isLoggedIn,drinkSubCategoryUpload.single('image'),drinkSubCategorieController.save);
drinkSubCategoreiRoutes.get('/list',authController.isLoggedIn,drinkSubCategorieController.list);
drinkSubCategoreiRoutes.get('/listing',authController.isLoggedIn,drinkSubCategorieController.listing);
drinkSubCategoreiRoutes.get('/delete/:id',authController.isLoggedIn,drinkSubCategorieController.delete);
drinkSubCategoreiRoutes.get('/edit/:id',authController.isLoggedIn,drinkSubCategorieController.edit);
drinkSubCategoreiRoutes.post('/update/:id',authController.isLoggedIn,drinkSubCategoryUpload.single('image'),drinkSubCategorieController.update);
