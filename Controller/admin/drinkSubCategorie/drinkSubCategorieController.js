const Sequelize = require('sequelize');
import slugify from 'slugify';
import {
    Drink_Categorie,Drink_Sub_Categorie
} from '../../../models';
import {
    responseObject,
    errorResponse
} from '../../../helpers/responseCode';
import {
    responseCode
} from '../../../helpers/StatusCode'
import models from "../../../models";
import { readdirSync } from 'fs';
import { raw } from 'body-parser';
var sequelize = models.sequelize;
const {
    Op
} = require("sequelize");


export default {
    async create(req, res, ) {
        try {
            const drinkCategorie = await Drink_Categorie.findAll({
            
            });
           
         
             return res.render('admin/drinkSubCategorie/create',{drinkCategorie:drinkCategorie})
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async save(req, res, ) {
        try {
            const drinkSubType={
                typeId:req.body.types,
                name :req.body.name,
                slug : slugify(req.body.name, "-"),
                description:req.body.description,
                image: req.file.originalname,
               }
               const subTypeInfo = await Drink_Sub_Categorie.create(drinkSubType);
               if(subTypeInfo){
                req.flash('success', "Drink Sub-Categorie added sucessfully");     

               }
               else{
                req.flash('failure', "Failed to add Drink Sub-Categorie ");

               }
            
           return res.redirect("/admin/drinkSubCategorie/list")
         
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async list(req, res, ) {
        try {
            
           
         
             return res.render('admin/drinkSubCategorie/listing',{ 
                success: await req.consumeFlash('success'),
                failure: await req.consumeFlash('failure')})
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async listing(req, res, ) {
        try {
            
           
            var pagno = req.query.start / req.query.length + 1
            var page = parseInt(req.query.draw) || 1;
            var limit = parseInt(req.query.length) || 2;
            let searchString = req.query.search.value || ''
            let order  = []
            if(req.query.order[0].column == '0'){
                order = [["categories","name", req.query.order[0].dir]]
            }else if(req.query.order[0].column == '1'){
                order = [["name", req.query.order[0].dir]]
            }
            else if(req.query.order[0].column == '2'){
                order = [["description", req.query.order[0].dir]]
            }
            else{
                order = [["createdAt", req.query.order[0].dir]]
            }
            var filter = {
                include :[{
                    model: Drink_Categorie,
                    as: "categories"
                }],
                offset: ((pagno - 1) * limit),
                limit: limit,
                order:order,
                
            }

            var filter1 = {

                raw: true
            }
            
                if (req.query.search.value) {
                    filter.where =  {[Op.or]: {
                    typeId: {[Op.like]: '%'+req.query.search.value+'%'},
                            name: {[Op.like]: '%'+req.query.search.value+'%'},
                            description: {[Op.like]: '%'+req.query.search.value+'%'},
                            '$categories.name$': {[Op.like]: '%'+req.query.search.value+'%'},
                        }
                        
                        }
                    }
            
            const drinkSubCategoryInfo = await Drink_Sub_Categorie.findAll(filter)
            const totaldata = await Drink_Sub_Categorie.findAll(filter1)

            return res.json({
                draw: page,
                recordsTotal: totaldata.length,
                recordsFiltered: totaldata.length,
                data: drinkSubCategoryInfo
            })

         
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async delete(req, res, ) {
        try {
            let data = await Drink_Sub_Categorie.destroy({
                where: {
                    id: req.params.id
                }
            });
            if (data) {
                return res.json({
                    success: true
                })
            } else {
                return res.json({
                    success: false
                })
            }
           
         
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
  async edit (req,res){
      try {
        const editsubcategory = await Drink_Sub_Categorie.findOne({
            where:{
                id:req.params.id
            },
            include: [{
                model: Drink_Categorie,
                as: "categories",
                attributes: ['id','name']

            }],
           
        }) 

        const drinkCategorie = await Drink_Categorie.findAll({
           
        });

        return res.render('admin/drinkSubCategorie/edit',{
            editsubcategory:editsubcategory,
            drinkCategorie:drinkCategorie,
            // selectcategorie:
        });
      } catch (err) {
        responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")
      }
  },
  async update(req,res){
      try {
         
        const subcategory = await Drink_Sub_Categorie.findOne({
            where:{
                id:req.params.id
            }, 
        })
        if(subcategory){
            const drinkSubType={
                typeId:req.body.types,
                name :req.body.name,
                slug : slugify(req.body.name, "-"),
                description:req.body.description,
                image: req.file.originalname,
               }
            const updatesubcategory = await subcategory.update(drinkSubType,{});
            
            if(updatesubcategory) {
                req.flash('success', "Event updated sucessfully");
            }else{
                req.flash('failure', "Updation Failed");
            }

        }
        return res.redirect('/admin/drinkSubCategorie/list');
      } catch (err) {
        responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")
      }
  }
}