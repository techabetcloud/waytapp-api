const Sequelize = require('sequelize');
const User = require('../../../models').User
import {
    create
} from 'domain';
// import { userInfo } from 'os';
import {
    responseObject,
    errorResponse
} from '../../../helpers/responseCode';
import {
    responseCode
} from '../../../helpers/StatusCode'
import models from "../../../models";
var sequelize = models.sequelize;
const {
    Op
} = require("sequelize");
const { validationResult }=require('express-validation')
var moment=require('moment')


export default {
    async create(req, res) {
        try {

        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }

    },
    async list(req, res) {
        try {
            const userInfo = await User.findAll({
                where:{
                    role:'User'
                }
            }) 
        
            return res.render('admin/userslist/listing', {
                data: '',
                success: await req.consumeFlash('success'),
                failure: await req.consumeFlash('failure')
            })
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")
        }

    },
    async view(req, res) {
        try {
              const user= await User.findOne({
                  where:{
                    id:req.params.id
                  }
                })
                user.date = moment(user.createdAt).format('MMM-DD-YYYY h:mm A')
            
                    res.render('admin/userslist/userDetail',{user:user})
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")
        }

    },
    async ajaxlist(req, res) {
        try {

           
            var pagno = req.query.start / req.query.length + 1
            var page = parseInt(req.query.draw) || 1;
            var limit = parseInt(req.query.length) || 2;
            let searchString = req.query.search.value || '';
            let order  = []
            if(req.query.order[0].column == '0'){
                order = [["name", req.query.order[0].dir]]
            }else if(req.query.order[0].column == '1'){
                order = [["email", req.query.order[0].dir]]
            }else if(req.query.order[0].column == '2'){
                order = [["city", req.query.order[0].dir]]
            }
            else{
                order = [["createdAt", req.query.order[0].dir]]
            }
            var filter = {
                offset: ((pagno - 1) * limit),
                limit: limit,
                order:order,
                where:{ role:'User'},
                raw: true
            }

            var filter1 = {
                where:{ role:'User'},
                raw: true,
            }
            if (req.query.search.value) {
                filter.where = {[Op.or]: {
                    name: {[Op.like]: '%'+req.query.search.value+'%'},
                    email: {[Op.like]: '%'+req.query.search.value+'%'},
                    city: {[Op.like]: '%'+req.query.search.value+'%'},
                } 
                }
            }


          
            const userInfo = await User.findAll(filter)
            const totaldata = await User.findAll(filter1)

            return res.json({
                draw: page,
                recordsTotal: totaldata.length,
                recordsFiltered: totaldata.length,
                data: userInfo
            })
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")
        }

    },
    async delete(req, res) {
        try {
        
            let data = await User.destroy({
                where: {
                    id: req.params.id
                }
            });
            if (data) {
                return res.json({
                    success: true
                })
            } else {
                return res.json({
                    success: false
                })
            }
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }

    },
    async edit(req, res) {
        try {
           

            const user = await User.findOne({
                where: {
                    id: req.params.id
                }
            });
            
            return res.render('admin/userslist/edit', {
                user: user,
                    success: await req.consumeFlash('success'),
                    failure: await req.consumeFlash('failure')

            });

        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }

    },
    async update(req, res) {
        try {
        
            const user = await User.findOne({
                where: {
                    id:req.params.id
                }
            });
            var date = req.body.dob
            var birthDate = new Date(date);
            var difference=Date.now() - birthDate.getTime(); 
            var  ageDate = new Date(difference); 
            var calculatedAge=   Math.abs(ageDate.getUTCFullYear() - 1970);
              if(calculatedAge < 16){
                req.flash('failure', "Age should be atleast 16 years");
                return res.redirect(`/admin/userlist/edit/${req.params.id}`);
            }
            if (user) {
                delete req.body.email
                delete req.body.role
                delete req.body.isVerified
               
                var userUpdate = await user.update(req.body, {

                });
                if(userUpdate) {
                    req.flash('success', "User updated sucessfully");
                    
                    
                }else{
                    req.flash('failure', "Updation Failed");
                }
            }
        
            return res.redirect('/admin/userlist/listing');

            //   return res.send({message:"User Updated Successfully.",data:userUpdate});
           


        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }

    },
}