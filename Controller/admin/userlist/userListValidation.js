const Joi = require('joi');


export default {
    async createAccountSchema(req, res,next) {
        const schema = Joi.object({
            name: Joi.string().trim().required().min(3).max(20),
            // city: Joi.string().trim().min(3).max(20),
           
        })

        // schema options
        const options = {
            abortEarly: false, // include all errors
            allowUnknown: true, // ignore unknown props
            stripUnknown: true // remove unknown props
        };

        // validate request body against schema
        const {
            error,
            value
        } = schema.validate(req.body, options);

        if (error) {
            let data = {}
            error.details.map(x=>{
             data[x.path[0]] = x.message
            })
            return res.json({error:data})
        //   next();
            next(`Validation error: ${error.details.map(x => x.message).join(', ')}`);
            // return next(`Validation error: ${error.details.map(x => x.message).join(', ')}`);
        } else {
            // return true
            // on success replace req.body with validated value and trigger next middleware function
            //req.body = value;
            next();
        }  

    }
}