const express = require('express');
const  { validate } = require ('express-validation')
import userListController from './userListController';
import authController from '../auth/auth.controller';
import  test from './userListValidation';
export const userRouter = express.Router()




userRouter.get('/add',authController.isLoggedIn,userListController.create)
userRouter.get('/listing',authController.isLoggedIn,userListController.list)
userRouter.get('/ajax-listing',authController.isLoggedIn,userListController.ajaxlist)
userRouter.get('/delete/:id',authController.isLoggedIn,userListController.delete)
userRouter.get('/edit/:id',authController.isLoggedIn,userListController.edit)
userRouter.get('/view/:id',authController.isLoggedIn,userListController.view)
userRouter.post('/update/:id',authController.isLoggedIn,function (req, res,next) {
    
    test.createAccountSchema(req,res,next)
},userListController.update)

// test.createAccountSchema,



                                                                                                                                                             

// module.exports = userRouter;
