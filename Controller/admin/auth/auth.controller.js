const Sequelize = require('sequelize');
const User = require('../../../models').User
import crypto from "crypto";
import {
  responseObject,
  errorResponse
} from '../../../helpers/responseCode';
import {
  responseCode
} from '../../../helpers/StatusCode'
import models from "../../../models";
var sequelize = models.sequelize;


export default {

  async adminlogin(req, res) {
    try {
  

      const user = await User.findOne({
        where: {
          email: req.body.email
        }
      });
    

      const reqPass = crypto.createHash("md5").update(req.body.password || "").digest("hex");
      
      if (!user) {
        return res.json({status:0,message:"Please enter valid email "});
      
      }
      if (reqPass !== user.password) {
        
        return res.json({stauts:0, message:"Invalid Password "});
       
      }
      if (user.role !== "Admin") {
        return res.json({status:0,message:"You are not authorised to access this portal "});
     
      }
      req.session.email = user.email;
      req.session.userid = user.id;
      req.session.roleid = user.role;
      req.session.password = user.password;
     
      if (
        req.session.roleid ){
          return res.json({status:1,message:"User logged In"});
        }
       
    } catch (err) {
      responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")
      return res.redirect('/admin/login');
    }
  },
  async isLoggedIn(req, res, next) {
    
    if (req.session.userid) {
            next();
    } else {
          return res.redirect('/admin/login');
    }
  },

  async create(req, res) {
    try {


      if(req.session.userid){
        return res.redirect('/admin/dashbord')
      }

      return res.render('admin/auth/adminlogin')
    } catch (err) {
      return res.status(400).json({
        status: "false",
        data: err
      });
    }
  },


  async logout(req, res) {
    if (req.session) {
      req.session.destroy(function (err) {
        if (err) {
          return next(err);
        } else {
          req.session = null;
          return res.redirect('/admin/login');
        }
      });
    }
  },

  async emailTokens(req, res){

    const users = await User.findAll();
   // return res.json(user);
    res.render('admin/emailToken/listing',{users:users})
  }
}