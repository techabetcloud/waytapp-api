const express = require('express');
import multer  from 'multer'
import drinksController from './drinksController';
import authController from '../auth/auth.controller';
import { drinksStorage } from '../../../generic';
export const drinksRoutes  = express.Router()
var drinksUpload=multer({storage:drinksStorage })


drinksRoutes.get('/add',authController.isLoggedIn,drinksController.create);
drinksRoutes.post('/save',authController.isLoggedIn,drinksUpload.single('image'),drinksController.save);
drinksRoutes.get('/list',authController.isLoggedIn,drinksController.list);
drinksRoutes.get('/listing',authController.isLoggedIn,drinksController.listing);
drinksRoutes.get('/delete/:id',authController.isLoggedIn,drinksController.delete);
drinksRoutes.get('/edit/:id',authController.isLoggedIn,drinksController.edit);
drinksRoutes.post('/update/:id',authController.isLoggedIn,drinksUpload.single('image'),drinksController.update);

