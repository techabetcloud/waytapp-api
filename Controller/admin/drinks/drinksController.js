const Sequelize = require('sequelize');
// const Restaurant = require('../../../models').Restaurant

import {
    Drink,
    Drink_Categorie,
    Drink_Sub_Categorie,Restaurant,
} from '../../../models';
import {
    responseObject,
    errorResponse
} from '../../../helpers/responseCode';
import {
    responseCode
} from '../../../helpers/StatusCode'
import models from "../../../models";
import { type } from 'jquery';
import { updateLocale } from 'moment-timezone';
var sequelize = models.sequelize;
const {
    Op
} = require("sequelize");

export default {
    async create(req, res, ) {
        try {
            
            const types = await Drink_Categorie.findAll({
                include:[
                    {
                        model:Drink_Sub_Categorie,
                        as: "subCategory",
                       attributes: ['id','name', 'typeId']
                        
                    }
                ],
            });
           
                return res.render('admin/drinks/create',{types:types})
            
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },

    async save(req, res, ) {
        try {
          
              const drinksInfo = {

                sub_categorieId : req.body.typess,
                name: req.body.name,
                price: req.body.price,
                detail: req.body.details,
                image: req.file.originalname,
                status: req.body.status,
            }
            
            const drinks = await Drink.create(drinksInfo)
            if(!drinks){
                req.flash('failure', "Failed to add drink"); 
               }
               else{
                req.flash('success', "Drink  added sucessfully");     
               }
           return res.redirect('/admin/drinks/list')
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async list(req, res, ) {
        try {
            
         
             
                return res.render('admin/drinks/listing',{ 
                success: await req.consumeFlash('success'),
                failure: await req.consumeFlash('failure')})
           
             
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async delete(req, res, ) {
        try {
            let data = await Drink.destroy({
                where: {
                    id: req.params.id
                }
            });
            if (data) {
                return res.json({
                    success: true
                })
            } else {
                return res.json({
                    success: false
                })
            }
         
             
                // return res.render('admin/drinks/listing')
           
             
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async listing(req, res, ) {
        try {
            var pagno = req.query.start / req.query.length + 1
            var page = parseInt(req.query.draw) || 1;
            var limit = parseInt(req.query.length) || 2;
            let searchString = req.query.search.value || ''
            let order  = []
            if(req.query.order[0].column == '0'){
                order = [["name", req.query.order[0].dir]]
            }else if(req.query.order[0].column == '1'){
                order = [["subCategory","name", req.query.order[0].dir]]
            }else if(req.query.order[0].column == '2'){
                order = [["detail", req.query.order[0].dir]]
            }
            else{
                order = [["createdAt", req.query.order[0].dir]]
            }
            var filter = {
                offset: ((pagno - 1) * limit),
                limit: limit,
                order:order,
                include: [{
                    model: Drink_Sub_Categorie,
                    as: "subCategory",
                  }],
                
            }
                
            var filter1 = {
                raw:true
            
            }
            
                if (req.query.search.value) {
                    filter.where =  {
                        [Op.or]: {
                            name: {[Op.like]: '%'+req.query.search.value+'%'},
                            detail: {[Op.like]: '%'+req.query.search.value+'%'},
                            '$subCategory.name$':{[Op.like]: '%'+req.query.search.value+'%'}
                       
                        },
                     
                        }
                    }
            const drinksInfo = await Drink.findAll(filter)
            const totaldata = await Drink.findAll(filter1)
        

            return res.json({
                draw: page,
                recordsTotal: totaldata.length,
                recordsFiltered: totaldata.length,
                data: drinksInfo
            })


        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")


        }
    },
    async edit(req,res){
        try {
            const editdrink = await Drink.findOne({
                where:{
                    id:req.params.id
                },
                include: [{
                    model: Drink_Sub_Categorie,
                    as: "subCategory"
                }],
            })
       
            const drinkCategory = await Drink_Categorie.findAll({
                include:[
                    {
                        model:Drink_Sub_Categorie,
                        as: "subCategory",
                       attributes: ['id','name', 'typeId']
                        
                    }
                ],
            });
    
            return res.render('admin/drinks/edit',{
                editdrink:editdrink,
                drinkCategory:drinkCategory
            });
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")

        }
    },
    async update(req,res){
        try {
            const drink = await Drink.findOne({
                where:{
                    id:req.params.id
                },
            })
            if(drink){
                const drinksInfo = {

                    sub_categorieId : req.body.typess,
                    name: req.body.name,
                    price: req.body.price,
                    detail: req.body.details,
                    // image: req.file.originalname,
                    status: req.body.status,
                }

           const updatedrink = await drink.update(drinksInfo,{});
            
            if(updatedrink) {
                req.flash('success', "Event updated sucessfully");
            }else{
                req.flash('failure', "Updation Failed");
            }

        }
        return res.redirect('/admin/drinks/list');
            
        } catch (err) {
            responseObject(req, res, {}, responseCode.INTERNAL_SERVER_ERROR, false, "Something went wrong")
        }
    }
}