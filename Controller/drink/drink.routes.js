var express = require('express');
const  { validate } = require ('express-validation')
import * as drinkValidator from './drink.validation'
import drinkController from './drink.controller'

var drinkRouter = express.Router();


drinkRouter.get('/get-drink-categorie',drinkController.getDrinkCategorie)
drinkRouter.get('/get-drink-sub-categorie',validate(drinkValidator.SubCategorie),drinkController.getDrinkSubCategorie)

module.exports = drinkRouter;
