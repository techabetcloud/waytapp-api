const Sequelize = require('sequelize');
 import { Restaurant,Drink_Categorie ,Drink_Sub_Categorie,Drink,Restaurant_Drink } from '../../models'
import { responseObject, errorResponse } from '../../helpers/responseCode';
import { responseCode } from '../../helpers/StatusCode'
import models from "../../models";
import { FILTER }  from '../../helpers/setting'
import { name } from 'ejs';
import { raw } from 'body-parser';
var sequelize = models.sequelize;
var moment = require("moment");
const {
    QueryTypes,
    Op,
    where
} = require("sequelize");


export default {
    async getDrinkCategorie(req,res){
        try {
         const categorie =  await Drink_Categorie.findAll({
                            attributes:['id','name','order'],
                            })
                        if(categorie.length){
                            return responseObject(
                                req,
                                res,
                                categorie,
                                responseCode.OK,
                                true,
                                res.__("GET_CATEGORIES_SUCCESSFULLY")
                                );
                            }else{
                              return responseObject(
                                req,
                                res,
                                "",
                                responseCode.NOT_FOUND,
                                false,
                                res.__("DATA_NOT_FOUND")

                              )
                            } 

        } catch (err) {
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")

              );
        }
    },
    async getDrinkSubCategorie(req,res){
        try {
          var resp = await FILTER()

          const drinkCategory = await Drink_Sub_Categorie.findOne({
            where:{
              typeId:req.query.categorie
            }
          })
           if(!drinkCategory){
            return responseObject(
              req,
              res,
              "",
              responseCode.NOT_FOUND,
              true,
              res.__("DRINK_CATEGORY_NOT_FOUND")
          )
           }
           const Restaurants = await Restaurant.findOne({
            where:{
              id:req.query.restaurantId
            }
          })
           if(!Restaurants){
            return responseObject(
              req,
              res,
              "",
              responseCode.NOT_FOUND,
              true,
              res.__("RESTAURANT_NOT_FOUND")
          )
           }
          const SubCategorie = await Restaurant_Drink.findAll({
            attributes: ['restaurantId','drinkId'],
              include:[{
                model:Drink,
                as:"drink",
                required: true,
                attributes:['sub_categorieId','price'],
                include: [{
                  model: Drink_Sub_Categorie,
                  as: 'subCategory',
                  required: true,
                  attributes:['typeId','name','image'],
                         where:{
                          typeId:req.query.categorie
                         },
                }]
              }],
              where:{
                restaurantId:req.query.restaurantId
              },
            order:[
              [resp.orderBy,resp.order]
          ],
          limit:parseInt(resp.limit),
          offset:((resp.page - 1) * parseInt(resp.limit)), 
      
        // raw:true
          })
      if(SubCategorie.length){
        return responseObject(
            req,
            res,
            SubCategorie,
            responseCode.OK,
            true,
            res.__("GET_SUBCATEGORIES_SUCCESSFULLY")
            );
      }else{
        return responseObject(
            req,
            res,
            "",
            responseCode.NOT_FOUND,
            false,
            res.__("DATA_NOT_FOUND")
        )
      }
        } catch (err) {
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")
              );
        }
    },
}