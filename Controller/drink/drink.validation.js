const Joi = require('joi');


  export const SubCategorie = {
    query: Joi.object().keys({
    categorie:Joi.string().required()
    .messages({"any.required": `categories is a required field`,
    "string.empty": `categories is not allowed to be empty`}),
    restaurantId:Joi.string().required()
    .messages({"any.required": `restaurantId is a required field`,
    "string.empty": `restaurantId is not allowed to be empty`}),
  }).unknown(false).error(errors => {
    errors.forEach(err => {0
      switch (err.code) {
        case "object.allowUnknown":
          err.message = err.path[0] + ` parameter  is not allowed!`;
          break;
        default:
          break
      }
    });
    return errors;
  })
  };