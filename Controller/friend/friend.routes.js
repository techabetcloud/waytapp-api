const express = require('express');
const  { validate } = require ('express-validation')
import * as friendValidator from './friend.validation'
import friendController from './friend.controller'

const friendRouter = express.Router();


friendRouter.post('/follow/unfollow',validate(friendValidator.followFriend),friendController.followFriend)
friendRouter.get('/get-followers',validate(friendValidator.Followers),friendController.getFollowers)
friendRouter.get('/get-blockList',friendController.BlockList)
friendRouter.post('/unblock',friendController.unblockFriend)
friendRouter.post('/block',validate(friendValidator.block),friendController.blockUser)
friendRouter.get('/friend-details',validate(friendValidator.Details),friendController.getDetails)
friendRouter.get('/friend-favourite-details',validate(friendValidator.Details),friendController.getFriendFavouriteDetails)
friendRouter.get('/get-friend-followers',friendController.getFriendFollowers)
friendRouter.get('/get-friend-details',friendController.getFriendDetails)





module.exports = friendRouter;
