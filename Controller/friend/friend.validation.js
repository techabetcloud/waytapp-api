const Joi = require('joi');



export const followFriend = {
    body: Joi.object().keys({
    friendId:Joi.number().required().messages({"any.required": `friendId is a required field`}),
  }).unknown(false).error(errors => {
    errors.forEach(err => {0
      switch (err.code) {
        case "object.allowUnknown":
          err.message = err.path[0] + ` parameter  is not allowed!`;
          break;
        default:
          break
      }
    });
    return errors;
  })
  };
  export const Followers = {
    query: Joi.object().keys({
    type:Joi.string().required()
    .messages({"any.required": `type is a required field`,
    "string.empty": `type is not allowed to be empty`}),
  }).unknown(false).error(errors => {
    errors.forEach(err => {0
      switch (err.code) {
        case "object.allowUnknown":
          err.message = err.path[0] + ` parameter  is not allowed!`;
          break;
        default:
          break
      }
    });
    return errors;
  })
  };
  export const Details = {
    query: Joi.object().keys({
    id:Joi.number().required()
    .messages({"any.required": `id is a required field`,
    "number.base": `id must be a number and not allowed to be empty`}),
    // type:Joi.string().required()
    // .messages({"any.required": `type is a required field`,
    // "string.empty": `type is not allowed to be empty`}),
  }).unknown(false).error(errors => {
    errors.forEach(err => {0
      switch (err.code) {
        case "object.allowUnknown":
          err.message = err.path[0] + ` parameter  is not allowed!`;
          break;
        default:
          break
      }
    });
    return errors;
  })
  };
  export const block = {
    body: Joi.object().keys({
      friendId:Joi.string().required().messages({"any.required": `friendId is a required field`}),
  }).unknown(false).error(errors => {
    errors.forEach(err => {0
      switch (err.code) {
        case "object.allowUnknown":
          err.message = err.path[0] + ` parameter  is not allowed!`;
          break;
        default:
          break
      }
    });
    return errors;
  })
  };