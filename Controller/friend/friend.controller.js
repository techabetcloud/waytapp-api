const Sequelize = require('sequelize');

import {
    Friends,
    User,
    Setting,
    User_Login
} from '../../models'
import {
    responseObject,
    errorResponse
} from '../../helpers/responseCode';
import {
    responseCode
} from '../../helpers/StatusCode'
import models from "../../models";
import {
    Console
} from 'console';
import {
    FILTER
} from '../../helpers/setting'
import sendNotification from '../../helpers/email.service'
var sequelize = models.sequelize;
const {
    QueryTypes,
    Op
} = require("sequelize");

export default {
    async followFriend(req, res) {
        try {
            const user = await User.findOne({
                where: {
                    id: req.body.friendId
                }
            })
            if (!user) {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.NOT_FOUND,
                    false,
                    res.__("USER_NOT_FOUND")
                )
            }
            const followfriend = await Friends.findOne({
                where: {
                    userId: req.user.userId,
                    friendId: req.body.friendId,
                    status: "follow"
                }
            })
            const unfollow = await Friends.findOne({
                where: {
                    userId: req.user.userId,
                    friendId: req.body.friendId,
                    status: "unfollow"
                }
            })
            if (followfriend) {
                const unfollow = await followfriend.update({
                    status: "unfollow"
                })
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.OK,
                    true,
                    res.__("UNFOLLOW_SUCCESSFULLY")
                );
            } else if (unfollow) {
                const follow = await unfollow.update({
                    status: "follow"
                })
                const token = await User_Login.findOne({
                    where: {
                        userId: req.body.friendId
                    },
                    order: [ [ 'id', 'DESC' ]],
                })
            
                const notification = {
                    token: token.deviceToken,
                    title: "waytapp",
                    data: `${req.user.name} following you`
                  }
                await sendNotification.firebaseService(notification)
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.OK,
                    true,
                    res.__("FOLLOW_SUCCESS")
                );
            } else {
                const follow = await Friends.create({
                    userId: req.user.userId,
                    friendId: req.body.friendId,
                    status: "follow"
                })
                if (follow) {
                    const userToken = await User_Login.findOne({
                        where: {
                            userId: follow.friendId
                        }
                    })
                    const user = await User.findOne({
                        where: {
                            id: follow.friendId
                        }
                    })
                }
                const token = await User_Login.findOne({
                    where: {
                        userId: req.body.friendId
                    },
                    order: [ [ 'id', 'DESC' ]],
                })
                const notification = {
                    token: token.deviceToken,
                    title: "waytapp",
                    data: `${req.user.name} following you`
                }
                await sendNotification.firebaseService(notification)
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.OK,
                    true,
                    res.__("FOLLOW_SUCCESS")
                );
            }
        } catch (err) {
            console.log("=====err",err)
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")
            );
        }
    },
    async getFollowers(req, res) {
        try {
            var resp = await FILTER()
            const limit = parseInt(resp.limit)
            const offset = ((resp.page - 1) * parseInt(resp.limit))
            const orderby = resp.orderBy
            const order = resp.order
            const search = (resp.searchString)
            const field = (resp.searchColumn)
            var filter
            if (req.query.type === "followers") {
                const follower = await sequelize.query(`SELECT DISTINCT  Friends.id , Friends.userId,Friends.friendId,Friends.createdAt,Users.name, CONCAT('${process.env.BASE_URL}images/userProfile/',profile_img) as image,
                IF(Friends.userId IN (SELECT friendId FROM Friends where Friends.userId = ${req.user.userId} AND Friends.status = "follow"), "Following" , "Follow") AS status
              
              FROM Friends
                LEFT JOIN Users ON Users.id = Friends.userId
              WHERE Friends.friendId  = ${req.user.userId} AND  ${req.user.userId} = Friends.userId OR Friends.friendId  = ${req.user.userId} AND Friends.status = "follow"  ORDER BY ${orderby} ${order}  LIMIT ${limit} OFFSET ${offset}`, {
                    replacements: [req.query.type],
                    nest: true,
                    type: QueryTypes.SELECT,
                })
                if (follower.length) {
                    return responseObject(
                        req,
                        res,
                        follower,
                        responseCode.OK,
                        true,
                        res.__("GET_USER_SUCCESSFULLY")
                    );
                } else {
                    return responseObject(
                        req,
                        res,
                        "",
                        responseCode.NOT_FOUND,
                        false,
                        res.__("DATA_NOT_FOUND")
                    )
                }
            } else if (req.query.type === "following") {
                const follower = await sequelize.query(`SELECT DISTINCT  Friends.id , Friends.userId,Friends.friendId,Friends.createdAt,user.name , CONCAT('${process.env.BASE_URL}images/userProfile/',profile_img)  as picture ,
            IF(Friends.status = "follow","following","follow") AS status 
          FROM Friends
            LEFT JOIN Users user ON user.id = Friends.friendId
          WHERE ${req.user.userId} = Friends.userId AND Friends.status = "follow"   ORDER BY ${orderby} ${order}  LIMIT ${limit} OFFSET ${offset}`, {
                    replacements: [req.query.type],
                    nest: true,
                    type: QueryTypes.SELECT,
                })

                if (follower.length) {
                    return responseObject(
                        req,
                        res,
                        follower,
                        responseCode.OK,
                        true,
                        res.__("GET_USER_SUCCESSFULLY")
                    );
                } else {
                    return responseObject(
                        req,
                        res,
                        "",
                        responseCode.NOT_FOUND,
                        false,
                        res.__("DATA_NOT_FOUND")
                    )
                }
            }
        } catch (err) {
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")

            );

        }
    },
    async blockUser(req, res) {
        try {
            const user = await User.findOne({
                where: {
                    id: req.body.friendId
                }
            })
            if (!user) {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.NOT_FOUND,
                    false,
                    res.__("USER_NOT_FOUND")
                )
            }
            const friend = await Friends.findOne({
                where: {
                    userId: req.user.userId,
                    friendId: req.body.friendId,
                    status: "block"
                }
            })
            if (friend) {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.BAD_REQUEST,
                    false,
                    res.__("USER_ALREADY_BLOCK")
                )
            }

            await Friends.findOne({
                where: {
                    userId: req.user.userId,
                    friendId: req.body.friendId,
                    status: "follow"
                }
            }).then(async block => {
                if (block) {
                    const blockUser = await block.update({
                        status: "block"
                    })
                    return responseObject(
                        req,
                        res,
                        "",
                        responseCode.OK,
                        true,
                        res.__("USER_BLOCK_SUCCESSFULLY")
                    )
                } else {
                    return responseObject(
                        req,
                        res,
                        "",
                        responseCode.NOT_FOUND,
                        false,
                        res.__("USER_NOT_FOUND")
                    )
                }
            }).catch(err => {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.NOT_FOUND,
                    res.__("SOMETHING_WENT_WRONG")
                )
            })
        } catch (err) {
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")
            );
        }
    },
    async BlockList(req, res) {
        try {
            var resp = await FILTER()
            const blockUser = await Friends.findAll({
                where: {
                    userId: req.user.userId,
                    status: "block",
                },
                include: [{
                    model: User,
                    as: "friend",
                    attributes: ['name', [sequelize.col('profile_img'), 'profile']],

                }],
                order: [
                    [resp.orderBy, resp.order]
                ],
                limit: parseInt(resp.limit),
                offset: ((resp.page - 1) * parseInt(resp.limit)),
            })
            if (blockUser.length) {
                return responseObject(
                    req,
                    res,
                    blockUser,
                    responseCode.OK,
                    true,
                    res.__("GET_USER_SUCCESSFULLY")
                );
            } else {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.NOT_FOUND,
                    false,
                    res.__("DATA_NOT_FOUND")

                )
            }
        } catch (err) {
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")

            );
        }
    },
    async getDetails(req, res) {
        try {
            var resp = await FILTER()
            const limit = parseInt(resp.limit)
            const offset = ((resp.page - 1) * parseInt(resp.limit))
            const orderby = resp.orderBy
            const order = resp.order
            const friend = await sequelize.query(
                `SELECT Users.id,Users.email,Users.name,Users.username,Users.description,Users.lat,Users.long,CONCAT('${process.env.BASE_URL}images/userProfile/',profile_img) AS profile ,  COUNT(Friends.status) AS Followers ,
            IF(Friends.friendId IN (SELECT friendId FROM Friends where Friends.userId = ${req.user.userId} AND Friends.status = "follow"), "Following" , "Follow") AS status
              FROM Users
              LEFT JOIN Friends ON Friends.friendId = Users.id
            
              WHERE Users.id =${req.query.id}`, {
                    replacements: [req.query.id],
                    nest: true,
                    type: QueryTypes.SELECT
                })
            const photo = await sequelize.query(
                `SELECT Postphotos.id, CONCAT('${process.env.BASE_URL}images/userProfile/',photoLink)  AS photo,Postphotos.createdAt
                  FROM Postphotos
                 LEFT JOIN Users ON Users.id = Postphotos.userId
                  WHERE Users.id=${req.query.id}  ORDER BY ${orderby} ${order}  LIMIT ${limit} OFFSET ${offset}`, {
                    replacements: [req.query.id],
                    nest: true,
                    type: QueryTypes.SELECT
                })
            friend.photo = photo ? Object.values(photo) : null;
            if (friend.length) {
                if (!friend[0].id) {
                    return responseObject(
                        req,
                        res,
                        "",
                        responseCode.NOT_FOUND,
                        false,
                        res.__("DATA_NOT_FOUND")

                    )
                }
                return responseObject(
                    req,
                    res, {
                        friend: friend[0],
                        photo: photo
                    },
                    responseCode.OK,
                    true,
                    res.__("GET_USER_SUCCESSFULLY")
                );
            } else {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.NOT_FOUND,
                    false,
                    res.__("DATA_NOT_FOUND")

                )
            }
        } catch (err) {
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")

            );
        }
    },
    async unblockFriend(req, res) {
        try {
            await Friends.findOne({
                where: {
                    userId: req.user.userId,
                    friendId: req.body.friendId,
                    status: "block"
                }
            }).then(async block => {
                if (block) {
                    const unblock = await block.update({
                        status: "follow"
                    })
                    return responseObject(
                        req,
                        res,
                        "",
                        responseCode.BAD_REQUEST,
                        res.__("UNBLOCK_SUCCESSFULLY")
                    )

                } else {
                    return responseObject(
                        req,
                        res,
                        "",
                        responseCode.OK,
                        res.__("USER_ALREADY_UNBLOCK")
                    )
                }
            }).catch(err => {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.NOT_FOUND,
                    res.__("USER_NOT_FOUND")
                )
            })
        } catch (err) {
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")
            );
        }
    },
    async getFriendFavouriteDetails(req, res) {
        try {
            var resp = await FILTER()
            const limit = parseInt(resp.limit)
            const offset = ((resp.page - 1) * parseInt(resp.limit))
            const orderby = resp.orderBy
            const order = resp.order
            const friend = await sequelize.query(
                `SELECT Users.id,Users.email,Users.name,Users.username,Users.description,Users.lat,Users.long,CONCAT('${process.env.BASE_URL}images/userProfile/',profile_img) AS profile ,COUNT(Friends.status) AS Followers
           
              FROM Users
              LEFT JOIN Friends ON Friends.friendId = Users.id  
              WHERE Users.id = ${req.query.id}`, {
                    replacements: [req.query.id],
                    nest: true,
                    type: QueryTypes.SELECT
                })
            const Following = await sequelize.query(
                `SELECT COUNT(Friends.status) As Following
                FROM Friends
                 WHERE ${req.query.id} = Friends.userId AND Friends.status = "follow"`, {
                    replacements: [],
                    nest: true,
                    type: QueryTypes.SELECT
                })
            const favourite = await sequelize.query(
                `SELECT DISTINCT  Restaurants.id,Restaurants.name,Restaurants.createdAt,
                      COUNT(Visits.restaurantId) As Favourite
                    FROM Visits
                    LEFT JOIN Restaurants ON Restaurants.id = Visits.restaurantId
                     WHERE ${req.query.id} = Visits.userId  GROUP BY restaurantId ORDER BY ${orderby} ${order} LIMIT ${limit} OFFSET ${offset}`, {
                    replacements: [],
                    nest: true,
                    type: QueryTypes.SELECT
                })
            friend.Following = Object.values(Following, favourite);
            if (friend.length) {
                return responseObject(
                    req,
                    res, {
                        friend: friend[0],
                        Following: Following[0],
                        favourite: favourite
                    },
                    responseCode.OK,
                    true,
                    res.__("GET_USER_SUCCESSFULLY")
                );
            } else {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.NOT_FOUND,
                    false,
                    res.__("DATA_NOT_FOUND")

                )
            }
        } catch (err) {
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")

            );
        }
    },
    async getFriendFollowers(req, res) {
        try {
            const resp = await FILTER()
            const limit = parseInt(resp.limit)
            const offset = ((resp.page - 1) * parseInt(resp.limit))
            const orderby = resp.orderBy
            const order = resp.order
            const search = (resp.searchString)
            const field = (resp.searchColumn)
            if (req.query.type === "following") {
                const friendFollower = await sequelize.query(`SELECT DISTINCT  Friends.id , Friends.userId,Friends.friendId,Users.name, CONCAT('${process.env.BASE_URL}images/userProfile/',profile_img) AS profile_img ,Friends.createdAt,
            IF(Friends.friendId IN (SELECT friendId FROM Friends where Friends.userId = ${req.user.userId} AND Friends.status = "follow"), "Following" , "Follow") AS status
            FROM Friends
            LEFT JOIN Users ON Users.id = Friends.friendId
            WHERE ${req.query.id} = Friends.userId AND Friends.status = "follow" ORDER BY ${orderby} ${order}  LIMIT ${limit} OFFSET ${offset}`, {
                    replacements: [req.query.type],
                    nest: true,
                    type: QueryTypes.SELECT,
                })
                if (friendFollower.length) {
                    return responseObject(
                        req,
                        res,
                        friendFollower,
                        responseCode.OK,
                        true,
                        res.__("GET_USER_SUCCESSFULLY")
                    );
                } else {
                    return responseObject(
                        req,
                        res,
                        "",
                        responseCode.NOT_FOUND,
                        false,
                        res.__("DATA_NOT_FOUND")
                    )
                }
            } else if (req.query.type === "followers") {
                const friendFollower = await sequelize.query(`SELECT DISTINCT  Friends.id , Friends.userId,Friends.friendId,Users.name,CONCAT('${process.env.BASE_URL}images/userProfile/',profile_img) AS profile_img , Friends.createdAt,
            IF(Users.id IN (SELECT Users.id FROM Friends where Friends.userId = ${req.user.userId} AND Users.id = Friends.friendId AND Friends.status = "follow"), "Following" , "Follow") AS status
            FROM Friends

            LEFT JOIN Users ON Users.id = Friends.userId
            WHERE  Friends.friendId  = ${req.query.id} AND Friends.status = "follow" ORDER BY ${orderby} ${order}  LIMIT ${limit} OFFSET ${offset}`, {
                    replacements: [req.query.type, req.query.id],
                    nest: true,
                    type: QueryTypes.SELECT,
                })
                if (friendFollower.length) {
                    return responseObject(
                        req,
                        res,
                        friendFollower,
                        responseCode.OK,
                        true,
                        res.__("GET_USER_SUCCESSFULLY")
                    );
                } else {
                    return responseObject(
                        req,
                        res,
                        "",
                        responseCode.NOT_FOUND,
                        false,
                        res.__("DATA_NOT_FOUND")
                    )
                }
            }
        } catch (err) {
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")

            );
        }
    },
    async getFriendDetails(req, res) {
        try {
            var resp = await FILTER()
            const limit = parseInt(resp.limit)
            const offset = ((resp.page - 1) * parseInt(resp.limit))
            const orderby = resp.orderBy
            const order = resp.order
            const friend = await sequelize.query(
                `SELECT Users.id,Users.email,Users.name,Users.username,Users.description,Users.lat,Users.long,CONCAT('${process.env.BASE_URL}images/userProfile/',profile_img) AS profile ,  COUNT(Friends.status) AS Followers ,
            IF(Friends.friendId IN (SELECT friendId FROM Friends where Friends.userId = ${req.user.userId} AND Friends.status = "follow"), "Following" , "Follow") AS status
              FROM Users
              LEFT JOIN Friends ON Friends.friendId = Users.id  
            
              WHERE Users.id =${req.query.id}`, {
                    replacements: [req.query.id],
                    nest: true,
                    type: QueryTypes.SELECT
                })

            const following = await sequelize.query(
                `SELECT 
                  COUNT(Friends.status) As Following
                FROM Friends
                 WHERE ${req.query.id} = Friends.userId AND Friends.status = "follow"`, {
                    replacements: [req.query.id],
                    nest: true,
                    type: QueryTypes.SELECT
                })
            const favourite = await sequelize.query(
                `SELECT DISTINCT  Restaurants.id,Restaurants.name,Restaurants.createdAt,
                  COUNT(Visits.restaurantId) As Favourite
                FROM Visits
                LEFT JOIN Restaurants ON Restaurants.id = Visits.restaurantId
                 WHERE ${req.query.id} = Visits.userId  GROUP BY restaurantId ORDER BY ${orderby} ${order} LIMIT ${limit} OFFSET ${offset}`, {
                    replacements: [req.query.id],
                    nest: true,
                    type: QueryTypes.SELECT
                })
            friend.favourite = favourite ? Object.values(favourite) : null;
            friend.following = Object.values(following, favourite);
            if (friend.length) {
                if (!friend[0].id) {
                    return responseObject(
                        req,
                        res,
                        "",
                        responseCode.NOT_FOUND,
                        false,
                        res.__("DATA_NOT_FOUND")

                    )
                }
                return responseObject(
                    req,
                    res, {
                        friend: friend[0],
                        following: following[0],
                        favourite: favourite
                    },
                    responseCode.OK,
                    true,
                    res.__("GET_USER_SUCCESSFULLY")
                );
            } else {
                return responseObject(
                    req,
                    res,
                    "",
                    responseCode.NOT_FOUND,
                    false,
                    res.__("DATA_NOT_FOUND")

                )
            }
        } catch (err) {
            return responseObject(
                req,
                res,
                "",
                responseCode.INTERNAL_SERVER_ERROR,
                false,
                res.__("SOMETHING_WENT_WRONG")
            );
        }
    },
}