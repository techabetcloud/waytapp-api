const Joi = require('joi');




export const messageSend = {
    body: Joi.object().keys({
    receiverId:Joi.string().required().messages({"any.required": `receiverdId is a required field`}),
    message:Joi.string().required().messages({"any.required": `message is a required field`}),
  }).unknown(false).error(errors => {
    errors.forEach(err => {0
      switch (err.code) {
        case "object.allowUnknown":
          err.message = err.path[0] + ` parameter  is not allowed!`;
          break;
        default:
          break
      }
    });
    return errors;
  })
  };