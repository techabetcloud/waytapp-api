var express = require('express');
const  { validate } = require ('express-validation')
import * as chatValidator from './chat.validation'
 import chatController from './chat.controller'
 const Chats = require("../../models/chat");

var chatRouter = express.Router();
// need to remove after confirmation
chatRouter.post('/send',validate(chatValidator.messageSend),chatController.messageSend)
// ----------

chatRouter.get('/show/:id',chatController.showChat)
chatRouter.get('/get-allChat',chatController.getAllUserChat)




module.exports = chatRouter;