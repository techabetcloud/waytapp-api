var express = require('express');

const io = require("socket.io");

const Sequelize = require('sequelize');
import {
  Chat,
  User
} from '../../models'
import {
  responseObject,
  errorResponse
} from '../../helpers/responseCode';
import {
  responseCode
} from '../../helpers/StatusCode'
import models from "../../models";
var sequelize = models.sequelize;
const {
  QueryTypes,
  Op
} = require("sequelize");
var moment = require("moment");


export default {
  async messageSend(req, res) {
    try {
      const message = await Chat.build({
        senderId: req.user.userId,
        receiverId: req.body.receiverId,
        message_content: req.body.message,
        is_seen: true
        // datetime:new Date()
      })
      await message.save().then(saveMessage => {
        return responseObject(
          req,
          res,
          saveMessage,
          responseCode.OK,
          true,
          "MESSAGE_SEND_SUCCESSFULLY"
        );
      }).catch(err => {
        return responseObject(
          req,
          res,
          "",
          responseCode.BAD_REQUEST,
          false,
          res.__("SERVER_ERROR")
        );
      })
    } catch (err) {
      return responseObject(
        req,
        res,
        "",
        responseCode.INTERNAL_SERVER_ERROR,
        false,
        res.__("SOMETHING_WENT_WRONG")
      );
    }
  },
  async showChat(req, res) {
    try {
      const chat = await Chat.findAll({
        attributes: ['id', 'message_content', 'senderId', 'receiverId', 'createdAt'],
        where: {
          senderId: {
            [Op.in]: [req.user.userId, req.params.id]
          },
          receiverId: {
            [Op.in]: [req.user.userId, req.params.id],
          }
        },
        order: [
          ['createdAt', 'DESC']
        ],
        raw: true
      })
      if (chat.length) {
        return responseObject(
          req,
          res,
          chat,
          responseCode.OK,
          true,
          res.__("GET_MESSAGE_SUCCESSFULLY")
        );
      } else {
        return responseObject(
          req,
          res,
          "",
          responseCode.NOT_FOUND,
          false,
          res.__("DATA_NOT_FOUND")

        )
      }
    } catch (err) {
      return responseObject(
        req,
        res,
        "",
        responseCode.INTERNAL_SERVER_ERROR,
        false,
        res.__("SOMETHING_WENT_WRONG")

              );
        }
    },
    async getAllUserChat(req,res){
      try {   
        const profile = await sequelize.query(
          `  SELECT chats.id,
          IF(chats.senderId = ${req.user.userId}, chats.senderId ,chats.receiverId) as senderId  ,
          IF(chats.receiverId = ${req.user.userId}, chats.senderId ,chats.receiverId) as receiverId,
          chats.message_content,chats.is_seen,chats.createdAt,
          IF(chats.is_seen = 0 , (SELECT count(Chats.is_seen) FROM Chats 
            where  Chats.is_seen = 0 ), 0) AS seenCount ,
            IF(chats.senderId = ${req.user.userId}, chats.senderId ,chats.receiverId) as senderId  ,
            IF(chats.receiverId = ${req.user.userId}, chats.senderId ,chats.receiverId) as receiverId,
          Users.name,CONCAT('${process.env.BASE_URL}images/userProfile/',profile_img) AS profile_img
          
                FROM Chats AS chats
                LEFT JOIN Users on
                  CASE   
                  WHEN chats.senderId =  ${req.user.userId} AND  Users.id = chats.receiverId then 1
                  WHEN chats.receiverId =  ${req.user.userId} AND Users.id = chats.senderId then 1
                   ELSE 0
                   END
                INNER JOIN
                (
                    SELECT
                        LEAST(senderId, receiverId) AS senderId,
                        GREATEST(senderId, receiverId) AS receiverId,
                        MAX(createdAt) AS max_id
                    FROM Chats
                    GROUP BY
                        LEAST(senderId, receiverId),
                        GREATEST(senderId, receiverId)
                ) AS chat
                    ON LEAST(chats.senderId, chats.receiverId) = chat.senderId AND
                       GREATEST(chats.senderId, chats.receiverId) = chat.receiverId AND
                       chats.createdAt = chat.max_id
                    WHERE chats.senderId = ${req.user.userId} OR chats.receiverId = ${req.user.userId}`, {
          replacements: [req.user.userId],
          nest: true,
          type: QueryTypes.SELECT
        })
      if (profile.length) {
        return responseObject(
          req,
          res,
          profile,
          responseCode.OK,
          true,
          res.__("GET_MESSAGE_SUCCESSFULLY")
        );
      } else {
        return responseObject(
          req,
          res,
          "",
          responseCode.NOT_FOUND,
          false,
          res.__("DATA_NOT_FOUND")
        )
      }
    } catch (err) {
      return responseObject(
        req,
        res,
        "",
        responseCode.INTERNAL_SERVER_ERROR,
        false,
        res.__("SOMETHING_WENT_WRONG")

      );
    }
  },
}