var express = require('express');
const  { validate } = require ('express-validation')
import * as clubbarValidator from './Club-Bar.validation'
import clubBarController from './Club-Bar.controller'

var clubbarRouter = express.Router();

// need to remove after confirmation
clubbarRouter.post('/get-bar-club',validate(clubbarValidator.ClubBar),clubBarController.getClubBar)
// -------------
// node script 
clubbarRouter.post('/post',clubBarController.insert)
// --------------

clubbarRouter.get('/get-bar',validate(clubbarValidator.filterClubBar),clubBarController.filterClubBar)
clubbarRouter.get('/guest-list',validate(clubbarValidator.guestList),clubBarController.avenueGuestList)
clubbarRouter.get('/get-venue',validate(clubbarValidator.venue),clubBarController.getVenue)


module.exports = clubbarRouter;
