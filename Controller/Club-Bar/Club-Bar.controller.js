const Sequelize = require('sequelize');
// const Restaurant = require('../../models').Restaurant
import {
  Restaurant,
  Visit,
  User,
  Restaurant_closing_days,
  RestaurantPhoto,
  WeekDay,
  Restaurant_Schedule,
  User_Login,
  Event
} from '../../models'
import {
  responseObject,
  errorResponse
} from '../../helpers/responseCode';
import {
  responseCode
} from '../../helpers/StatusCode'
import models from "../../models";
import {
  Console
} from 'console';
import {
  FILTER,
  NearByUser
} from '../../helpers/setting'
import {
  date
} from 'joi';
import sendNotification from '../../helpers/email.service'


import data from "../../Restaurants.json"

var sequelize = models.sequelize;
const {
  QueryTypes,
  Op
} = require("sequelize");
var moment = require("moment");
const log = require('log-to-file');



export default {

  async insert(req, res) {
    try {
      //  for(let i = 0 ; i < data.length;i++){

      //         if(data[i].picture1){
      //           await RestaurantPhoto.create({
      //             restaurantId:data[i].id,
      //             picture:data[i].picture1
      //            })
      //         }

      //         if(data[i].picture2){
      //           await RestaurantPhoto.create({
      //             restaurantId:data[i].id,
      //             picture:data[i].picture2
      //            })
      //         }
      //         if(data[i].picture3){
      //           await RestaurantPhoto.create({
      //             restaurantId:data[i].id,
      //             picture:data[i].picture3
      //            })
      //         }
      //         if(data[i].picture4){
      //           await RestaurantPhoto.create({
      //             restaurantId:data[i].id,
      //             picture:data[i].picture4
      //            })
      //         }
      //  }

      const inst = await Restaurant.bulkCreate(data)
      if (inst) {
        return res.json({
          message: "success"
        })
      } else {
        return res.json({
          message: "fali"
        })

      }

    } catch (err) {
      console.log(err)
    }
  },

  async getClubBar(req, res) {
    try {
      var resp = await FILTER()
      var near = await NearByUser(req, res)

      const limit = parseInt(resp.limit)
      const offset = ((resp.page - 1) * parseInt(resp.limit))
      const orderby = resp.orderBy
      const order = resp.order
      const distance = (near.distanceForClub && parseInt(near.distanceForClub)) || resp.distance

      const {
        lat,
        long
      } = req.query;
      const ClubBar = await sequelize.query(`SELECT Restaurants.id,Restaurants.adminId,Restaurants.categoryId,
            Restaurants.name,Restaurants.logo,Restaurants.description,Restaurants.address,Restaurants.city,Restaurants.state,
            Restaurants.country, Restaurants.link,Restaurants.month_close,Restaurants.lat,Restaurants.long,
            Restaurants.status,Restaurants.drinks_allowed,
            IF( Restaurants.status = 1,"Active","Inactive") AS status,
            IF( Restaurants.drinks_allowed = 1,"Allowed","NotAllowed") AS drinks_allowed,
           (3959 * acos(
         cos(radians(${lat}) ) * cos(radians(Restaurants.lat) ) * cos (radians(Restaurants.long) - radians(${long}
         )) + sin(radians(${lat})) * sin(radians(Restaurants.lat ))) )AS distance
      FROM Restaurants
      HAVING  Restaurants.status = 1 AND distance < ${distance} ORDER BY ${orderby} ${order}  LIMIT ${limit} OFFSET ${offset}`, {
        replacements: [lat, long],
        nest: true,
        type: QueryTypes.SELECT,
      })
      if (ClubBar.length) {
        return responseObject(
          req,
          res,
          ClubBar,
          responseCode.OK,
          true,
          res.__("GET_CLUB_BAR_SUCCESSFULLY")
        );
      } else {
        return responseObject(
          req,
          res,
          "",
          responseCode.NOT_FOUND,
          false,
          res.__("DATA_NOT_FOUND")

        )

      }
    } catch (err) {
      return responseObject(
        req,
        res,
        "",
        responseCode.INTERNAL_SERVER_ERROR,
        false,
        res.__("SOMETHING_WENT_WRONG")

      );
    }
  },
  async filterClubBar(req, res) {
    try {
      log(`userName:${req.user.name} , lat:${req.query.lat} , long:${req.query.long}`, 'log/error-logs.log');
      const notification = {
        token: req.user.deviceToken,
        title: "waytapp",
        data: `Welcome to waytapp`
      }
      await sendNotification.firebaseService(notification)
      req.query.lat = req.query.lat ? req.query.lat : '40.733525'
      req.query.long = req.query.long ? req.query.long : '-73.754878'

      if (req.query.lat === "null" || req.query.long === "null") {
        return responseObject(req, res, {}, responseCode.BAD_REQUEST, false, res.__("Lat and Long are required."))
      }
      var resp = await FILTER()
      var near = await NearByUser(req, res)
      const limit = parseInt(resp.limit)
      const offset = ((resp.page - 1) * parseInt(resp.limit))
      const orderby = resp.orderBy
      const order = resp.order
      const distance = (near.distanceForClub && parseInt(near.distanceForClub)) || resp.distance

      const lat = parseFloat(req.query.lat).toFixed(6);
      const long = parseFloat(req.query.long).toFixed(6);
      const date = moment(new Date()).utc().format("YYYY-MM-DD")
      var filter;
      if (req.query.type === "club") {
        filter = ` Restaurants.categoryId = 1`
      }
      if (req.query.type === "bar") {
        filter = `Restaurants.categoryId = 2`
      }
      const clubBar = await sequelize.query(`
                   SELECT Restaurants.id,Restaurants.categoryId,Restaurants.name,
                   IF(picture IS NOT NULL , CONCAT('${process.env.BASE_URL}images/restaurant/',picture), "http://54.183.89.161:5000/waytlogo.png") as restaurantphoto,
                   Restaurants.city,Restaurants.state,Restaurants.country,Restaurants.lat,Restaurants.long,Restaurants.address,Restaurants.description,Restaurants.status, Restaurants.drinks_allowed,Restaurants.link, Restaurants.createdAt,Restaurants.status, IF(Restaurants.categoryId = 1,"Club","Bar") AS category,
                   (SELECT COUNT(Visits.userId)
                   FROM Visits
                   WHERE Visits.restaurantId = Restaurants.id AND Visits.visit_date = '${date}' group by Visits.restaurantId) AS visitUser,
                   (SELECT COUNT(Visits.userId)
                   FROM Visits
                   WHERE Visits.restaurantId = Restaurants.id AND Visits.visit_date = '${date}' 
                   group by Visits.restaurantId)  * 100 / (Select COUNT(Visits.userId) 
                   From Visits
                   WHERE Visits.visit_date = '${date}'
                   )  as totalVisitUserPercentage,

                  (3959 * acos(
                       cos(radians(${lat}) ) * cos(radians(Restaurants.lat) ) * cos (radians(Restaurants.long) - radians(${long}
                        )) + sin(radians(${lat})) * sin(radians(Restaurants.lat ))) )AS distance  
                  FROM Restaurants
                  LEFT JOIN RestaurantPhotos ON  RestaurantPhotos.restaurantId = Restaurants.id
                  
                  WHERE ${filter} AND Restaurants.status = 1  group by  Restaurants.id
                   HAVING distance < ${distance}  ORDER BY ${orderby} ${order}  LIMIT ${limit} OFFSET ${offset}`, {
        replacements: [lat, long],
        nest: true,
        type: QueryTypes.SELECT,
      })
      if (clubBar.length) {
        return responseObject(
          req,
          res,
          clubBar,
          responseCode.OK,
          true,
          res.__("GET_CLUB_BAR_SUCCESSFULLY")
        );
      } else {
        return responseObject(
          req,
          res,
          "",
          responseCode.NOT_FOUND,
          false,
          res.__("DATA_NOT_FOUND")

        )

      }
    } catch (err) {
      console.log(err)
      return responseObject(
        req,
        res,
        "",
        responseCode.INTERNAL_SERVER_ERROR,
        false,
        res.__("SOMETHING_WENT_WRONG")

      );
    }
  },
  async avenueGuestList(req, res) {
    try {

      const restaurant = await Restaurant.findOne({
        where: {
          id: req.query.restaurantId,
          categoryId: req.query.categoryId
        }
      })
      if (!restaurant) {
        return responseObject(
          req,
          res,
          "",
          responseCode.NOT_FOUND,
          false,
          res.__("RESTAURANT_AND_CATEGORY_NOT_FOUND")
        )
      }
      const date = moment(new Date()).utc().format("YYYY-MM-DD")
      const guestList = await Visit.findAll({
        include: [{
          model: User,
          as: "visiting",
          attributes: ['name', [sequelize.col('profile_img'), 'photo']],
          //  distinct: 'userId'
        }],
        where: {
          categoryId: req.query.categoryId,
          restaurantId: req.query.restaurantId,
          visit_date: date
        },
      })
      for (let i = 0; i < guestList.length; i++) {
        guestList[i].visiting.dataValues.photo = process.env.BASE_URL + 'images/userProfile/' + guestList[i].visiting.dataValues.photo;
      }
      if (guestList.length) {
        return responseObject(
          req,
          res,
          guestList,
          responseCode.OK,
          true,
          res.__("GET_GUEST_LIST_SUCCESSFULLY")
        );
      } else {
        return responseObject(
          req,
          res,
          "",
          responseCode.NOT_FOUND,
          false,
          res.__("DATA_NOT_FOUND")
        )
      }
    } catch (err) {
      return responseObject(
        req,
        res,
        "",
        responseCode.INTERNAL_SERVER_ERROR,
        false,
        res.__("SOMETHING_WENT_WRONG")
      );
    }
  },
  async getVenue(req, res) {
    try {
      const restaurant = await Restaurant.findOne({
        where: {
          categoryId: req.query.categoryId,
          id: req.query.restaurantId,
        }
      })
      if (!restaurant) {
        return responseObject(
          req,
          res,
          "",
          responseCode.NOT_FOUND,
          false,
          res.__("RESTAURANT_AND_CATEGORY_NOT_FOUND")
        )
      }
      var resp = await FILTER()
      const venue = await Restaurant.findAll({
        where: {
          categoryId: req.query.categoryId,
          id: req.query.restaurantId,
        },
        include: [{
            model: RestaurantPhoto,
            as: "restaurantPhoto",
            attributes: ['picture'],
            required: false,
            where: {
              restaurantId: req.query.restaurantId,
            },
          },
          {
            model: Restaurant_Schedule,
            as: "restaurantOpen",
            attributes: ['weekDayId', 'from_date', 'to_date'],
            required: false,
            where: {
              restaurantId: req.query.restaurantId,
            },
            include: [{
              model: WeekDay,
              as: "openDays",
              attributes: ['id', 'day'],
              required: false,
            }, ]
          }
        ],
      })
      const arr = [];
      venue.map(el => {
        el.restaurantOpen.map(item => {
          arr.push(item.weekDayId)
        })
      })
      const restaurantclose = await WeekDay.findAll({
        attributes: ['id', 'day'],
        where: {
          id: {
            [Op.notIn]: arr
          }
        },
        raw: true
      })

      if (venue.length) {
        return responseObject(
          req,
          res, {
            venue,
            restaurantclose
          },
          responseCode.OK,
          true,
          res.__("GET_VEUNE_LIST_SUCCESSFULLY")
        );
      } else {
        return responseObject(
          req,
          res,
          "",
          responseCode.NOT_FOUND,
          false,
          res.__("DATA_NOT_FOUND")
        )
      }
    } catch (err) {
      return responseObject(
        req,
        res,
        "",
        responseCode.INTERNAL_SERVER_ERROR,
        false,
        res.__("SOMETHING_WENT_WRONG")
      );
    }
  },
}