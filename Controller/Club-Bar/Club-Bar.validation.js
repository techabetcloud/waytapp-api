const Joi = require('joi');




export const ClubBar = {
    query: Joi.object().keys({
    lat:Joi.string().required().messages({"any.required": `lat is a required field`}),
    long:Joi.string().required().messages({"any.required": `long is a required field`}),
  }).unknown(false).error(errors => {
    errors.forEach(err => {0
      switch (err.code) {
        case "object.allowUnknown":
          err.message = err.path[0] + ` parameter  is not allowed!`;
          break;
        default:
          break
      }
    });
    return errors;
  })
  };
  export const filterClubBar = {
    query: Joi.object().keys({
    lat:Joi.string().required()
    .messages({"any.required": `lat is a required field`,
    "string.empty": `lat is not allowed to be empty`}),
    
    long:Joi.string().required()
    .messages({"any.required": `long is a required field`,
    "string.empty": `long is not allowed to be empty`}),
  
    type:Joi.string().valid('bar','club').required().messages({"any.required": `type is a required field`}),
  }).unknown(false).error(errors => {
    errors.forEach(err => {
      switch (err.code) {
        case "object.allowUnknown":
          err.message = err.path[0] + ` parameter  is not allowed!`;
          break;
        default:
          break
      }
    });
    return errors;
  })
  };
  export const guestList = {
    query: Joi.object().keys({
      categoryId:Joi.string().required()
      .messages({"any.required": `categoryId is a required field`,
      "string.empty": `categoryId is not allowed to be empty`}),
      restaurantId:Joi.string().required()
      .messages({"any.required": `restaurantId is a required field`,
      "string.empty": `restaurantId is not allowed to be empty`}),
    
  }).unknown(false).error(errors => {
    errors.forEach(err => {
      switch (err.code) {
        case "object.allowUnknown":
          err.message = err.path[0] + ` parameter  is not allowed!`;
          break;
          default:
          break
      }
    });
    return errors;
  })
  };
  export const venue = {
    query: Joi.object().keys({
      categoryId:Joi.string().required()
      .messages({"any.required": `categoryId is a required field`,
      "string.empty": `categoryId is not allowed to be empty`}),
      restaurantId:Joi.string().required()
      .messages({"any.required": `restaurantId is a required field`,
      "string.empty": `restaurantId is not allowed to be empty`}),
    }).unknown(false).error(errors => {
    errors.forEach(err => {
      switch (err.code) {
        case "object.allowUnknown":
          err.message = err.path[0] + ` parameter  is not allowed!`;
          break;
          default:
          break
      }
    });
    return errors;
  })
  };