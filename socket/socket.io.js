var express = require('express');
const Sequelize = require('sequelize');
import {
  Chat,
  User,
  User_Login
} from '../models'
import {
  responseObject,
  errorResponse
} from '../helpers/responseCode';
import {
  responseCode
} from '../helpers/StatusCode'
import models from "../models";
import {
  restaurantStorage
} from '../generic';
import sendNotification from '../helpers/email.service'

var sequelize = models.sequelize;
const {
  QueryTypes,
  Op
} = require("sequelize");





const rootSocket = async (io, res) => {
  //  socket connection 
  io.on("connection", (socket) => {
    console.log("User connected", socket.id);

    // attach incoming listener for new user
    socket.on("user_connected", function (senderId) {
      // save in array
      addUser(senderId, socket.id)
      // notify all connected clients
      io.emit("user_connected", senderId);
    });

    // listen from client inside IO "connection" event
    socket.on("send_message", async (data) => {
      // send event to receiver
      let sendData = JSON.parse(data)
      const user = getUser(sendData.receiverId)
      sendData.serverSide = true;
      const receiver = socketUsers.find(user => user.senderId === sendData.receiverId);
      if (receiver?.socketId) io.to(receiver.socketId).emit("send_message", sendData.message);
      //  io.to(socket.id).emit("get_message", JSON.stringify(sendData));

      const token = await User_Login.findOne({
        where: {
            userId:sendData.receiverId
        }
    })
    const users = await User.findOne({
      where: {
          id:sendData.senderId
      }
  })
    const notification = {
        token: token.deviceToken,
        title: users.name,
        data: sendData.message
      }
    await sendNotification.firebaseService(notification)

      // // save in database
      await Chat.create({
        senderId: sendData.senderId,
        receiverId: sendData.receiverId,
        message_content: sendData.message,
        is_received: true
      }).then(chat => {
        return res.send({
          message: "success",
          chat
        })
      })
    });

    //  socket disconnect 
    socket.on("disconnect", () => {
      console.log("a user disconnected!");
      removeUser(socket.id)
      io.emit("getUsers", socketUsers);
    })
  });
}




var socketUsers = [];

const addUser = async (senderId, socketId) => {
  const userActive = socketUsers.filter(user => user.senderId === senderId);
  if (!userActive) socketUsers.push({
    senderId,
    socketId
  });
}

const getUser = (senderId) => {
  return socketUsers.find((user) => user.senderId === senderId);
};

const removeUser = (socketId) => {
  socketUsers = socketUsers.filter(user => user.socketId !== socketId);
  console.log("removing user ", socketUsers);
}



module.exports = rootSocket;