'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  const Restaurant_closing_days = sequelize.define(
    "Restaurant_closing_days",
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      restaurantId: {
        type: DataTypes.INTEGER,
        references: {
          model: 'Restaurants',
          key: 'id'
        },
        allowNull: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      weekDayId: {
        type: DataTypes.INTEGER,
        references: {
          model: 'WeekDays',
          key: 'id'
        },
        allowNull: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      }
      
    },
   
  );
  Restaurant_closing_days.associate = function (models) {
    Restaurant_closing_days.belongsTo(models.WeekDay,{
      foreignKey: "weekDayId",
      targetKey:"id",
      as:"closedDays"
    });

  };

return Restaurant_closing_days;
};