'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  const Albumb_Like = sequelize.define(
      "Albumb_Like",
      {
        id: {
          autoIncrement: true,
          type: DataTypes.INTEGER,
          allowNull: false,
          primaryKey: true
        },
        userId: {
          type: DataTypes.INTEGER,
          references: {
            model: 'Users',
            key: 'id'
          },
          allowNull: false,
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE',
        },
        restaurantId: {
          type: DataTypes.INTEGER,
          references: {
            model: 'Restaurants',
            key: 'id'
          },
          allowNull: false,
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE',
        },
        image: {
          allowNull: false,
          type: DataTypes.STRING,
          // references: {
          //   model: 'Postphotos',
          //   key: 'id'
          // },
        },
        image_Link: {
          allowNull: true,
          type: DataTypes.STRING
        },
        likes:{
          allowNull: false,
          type: DataTypes.BOOLEAN,
          comment: "1 = Like , 0 = unLike",
        }, 
      },
     
    );
    Albumb_Like.associate = function (models) {
  
    };
  return Albumb_Like;
};