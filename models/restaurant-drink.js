'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  const Restaurant_Drink = sequelize.define(
      "Restaurant_Drink",
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER
        },
        restaurantId: {
          type: DataTypes.INTEGER,
          references: {
            model: 'Restaurants',
            key: 'id'
          },
          allowNull: true,
        
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE'
        },
        drinkId: {
          type: DataTypes.INTEGER,
          references: {
            model: 'Drinks',
            key: 'id'
          },
        allowNull: true,
         
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE'
        },
      
        
      },
     
    );
    Restaurant_Drink.associate = function (models) {
      Restaurant_Drink.belongsTo(models.Restaurant, {
        foreignKey: 'restaurantId',
        targetKey:'id',
        as:'restaurant'
      });
      Restaurant_Drink.belongsTo(models.Drink, {
        foreignKey: 'drinkId',
        targetKey:'id',
        as:'drink'
      });
  
    };

  return Restaurant_Drink;
};