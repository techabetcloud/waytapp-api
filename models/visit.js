'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  const Visit = sequelize.define(
    "Visit",
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    userId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id'
      },
      allowNull: false,
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    },
    categoryId: {
      type: DataTypes.INTEGER,
        references: {
        model: 'Restaurant_Categories',
        key: 'id'
      },
      allowNull: false,
       onUpdate: 'CASCADE',
      onDelete: 'CASCADE'
    },
    restaurantId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Restaurants',
        key: 'id'
      },
      allowNull: false,
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    },
    visit_date:{
      allowNull: true,
      type: DataTypes.DATEONLY,
    },
  });
  Visit.associate = function (models) {
    Visit.belongsTo(models.User, {
      foreignKey: "userId",
      targetKey: "id",
      as: "visiting",
    });
    Visit.belongsTo(models.Restaurant, {
      foreignKey: "restaurantId",
      targetKey: "id",
      as: "Restaurant",
    });
  };
  return Visit;
};
