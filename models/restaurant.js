'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  const Restaurant = sequelize.define(
      "Restaurant",
      {
        id: {
          autoIncrement: true,
          type: DataTypes.INTEGER,
          allowNull: false,
          primaryKey: true
        },
        adminId: {
          type: DataTypes.INTEGER,
          allowNull: true,
          // references: {
          //   model: 'admins',
          //   key: 'id'
          // },
          // onUpdate: 'CASCADE',
          // onDelete: 'CASCADE'
         },
        categoryId: {
          type: DataTypes.INTEGER,
            references: {
            model: 'Restaurant_Categories',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE'
        },
       
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        logo: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        description: {
          type: DataTypes.TEXT,
          allowNull:true
          ,
        },
        address:{
          type: DataTypes.STRING,
          allowNull: false,
        },
        city: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        state: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        country: {
          type: DataTypes.STRING,
          allowNull: true,
        },
      
        link: {
          allowNull: true,
          type: DataTypes.TEXT
        },
        month_close: {
          allowNull: true,
          type: DataTypes.STRING
        },
        lat: {
          allowNull: true,
          type: DataTypes.STRING
        },
        long: {
          allowNull: true,
          type: DataTypes.STRING
        },
        status: {
        defaultValue: true,
          type: DataTypes.BOOLEAN,
          comment: "1:Active , 0:Inactive",
         
        },
        drinks_allowed: {
          type: DataTypes.BOOLEAN,
          allowNull: true,
         // defaultValue: false
         comment: "1:Allowed , 0:notAllowed",
        },
      })
    Restaurant.associate = function (models) {
      Restaurant.belongsTo(models.Restaurant_Categorie, {
        foreignKey: "categoryId",
        targetKey:"id",
        as:"category"
      });
      Restaurant.hasMany(models.Restaurant_Schedule, {
        foreignKey: "restaurantId",
        targetKey:"id",
        as:"restaurantOpen"
      }); 
      Restaurant.hasMany(models.RestaurantPhoto, {
        foreignKey: "restaurantId",
        targetKey:"id",
        as:"restaurantPhoto"
      });
      Restaurant.hasMany(models.Visit, {
        foreignKey: "restaurantId",
        targetKey:"id",
        as:"visituser"
      });
    };
    return Restaurant;
  };
  
