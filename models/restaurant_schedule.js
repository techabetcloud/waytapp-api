'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  const Restaurant_Schedule = sequelize.define(
    "Restaurant_Schedule",
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    restaurantId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Restaurants',
        key: 'id'
      },
      allowNull: false,
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    },
    weekDayId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'WeekDays',
        key: 'id'
      },
      allowNull: false,
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    },
    from_date: {
      allowNull: true,
      type: DataTypes.TIME,
    },
    to_date: {
      allowNull: true,
      type: DataTypes.TIME,
    },
  });
  Restaurant_Schedule.associate = function (models) {
    Restaurant_Schedule.belongsTo(models.WeekDay,{
      foreignKey: "weekDayId",
      targetKey:"id",
      as:"openDays"
    });
    // Restaurant_Schedule.belongsTo(models.WeekDay,{
    //   foreignKey: "weekDayId",
    //   targetKey:"id",
    //   as:"closedDays"
    // });
  };
  return Restaurant_Schedule;
};