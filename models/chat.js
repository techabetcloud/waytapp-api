'use strict';
const {
  Model
} = require('sequelize');
var moment = require("moment");

module.exports = (sequelize, DataTypes) => {
  const Chat = sequelize.define(
    "Chat",
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    senderId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id'
      },
      allowNull: false,
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    },
    receiverId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id'
      },
      allowNull: false,
    },
    message_content: {
      type: DataTypes.TEXT,
      get() {
        const rawValue = this.getDataValue('image');
        return rawValue ? process.env.BASE_URL+'images/userProfile/'+rawValue : null;
      },
      allowNull:false,
    },
    is_received: {
      type: DataTypes.BOOLEAN,
      defaultValue:false,
     comment: "1:Chat Received , 0:Chat Not Received",
    },
    is_seen: {
      type: DataTypes.BOOLEAN,
      defaultValue:false,
     comment: "1:Chat seen, 0:chat Not seen",
    },
    is_deleted_by_sender: {
      type: DataTypes.BOOLEAN,
      defaultValue:false
    },
    is_deleted_by_receiver: {
      type: DataTypes.BOOLEAN,
      defaultValue:false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      get() {
        return moment(this.getDataValue('createdAt')).format('h:mm A');
    }
    },
  });
  Chat.associate = function (models) {
    Chat.belongsTo(models.User, {
      foreignKey: "receiverId",
      targetKey: "id",
      as: "users",
    });
  
  };
  return Chat;
};