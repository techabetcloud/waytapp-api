'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  const RestaurantPhoto = sequelize.define(
      "RestaurantPhoto",
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER
        },
        restaurantId: {
          type: DataTypes.INTEGER,
          onDelete: 'CASCADE',
        },
        picture:{
           type:DataTypes.STRING,
           get() {
            const rawValue = this.getDataValue('picture');
            return rawValue ? process.env.BASE_URL+'images/restaurant/'+rawValue : null;
          },
           allowNull:false
        },
        
      },
     
    );
    RestaurantPhoto.associate = function (models) {
  
  
    };

  return RestaurantPhoto;
};