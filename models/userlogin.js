'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  const User_Login = sequelize.define(
      "User_Login",
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER
        },
        
        userId: {
          type: DataTypes.INTEGER,
          references: {
            model: 'Users',
            key: 'id'
          },
          allowNull: true,
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE',
        },
        loginToken: {
          type: DataTypes.STRING,
          allowNull: true
        },
        deviceType: {
          type: DataTypes.ENUM,
          values: ['WEB', 'ANDROID', 'IOS'],
          allowNull: true
        },
        deviceToken: {
          type: DataTypes.STRING,
          allowNull: true
        },
        
      },
     
    );
    User_Login.associate = function (models) {
      // associations can be defined here
  
    };
    return User_Login;
  };
  

    