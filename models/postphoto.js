'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  const Postphoto = sequelize.define(
      "Postphoto",
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER
        },
        userId: {
          type: DataTypes.INTEGER,
          references: {
            model: 'Users',
            key: 'id'
          },
          allowNull: false,
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE',
        },
        photoLink: {
          type: DataTypes.STRING,
          get() {
            const rawValue = this.getDataValue('photoLink');
            return rawValue ? process.env.BASE_URL+'images/userProfile/'+rawValue : null;
          },
         
          allowNull:false
        },
        createdBy: {
          type: DataTypes.INTEGER,
          references: {
            model: 'Users',
            key: 'id'
          },
          allowNull: false,
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE'
        },
  
        isDeleted: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
         comment: "1:postDelete , 0:notDelete",
  
        },
        
      },
     
    );
    Postphoto.associate = function (models) {
      Postphoto.belongsTo(models.User, {
        as: 'User',
        foreignKey: 'UserId',
        targetKey: 'id'
    });
    Postphoto.belongsTo(models.User, {
      as: 'created',
      foreignKey: 'createdBy',
      targetKey: 'id'
  });
  
    };
  return Postphoto;
};