'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  const Restaurant_Categorie = sequelize.define(
    "Restaurant_Categorie",
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    slug: {
      unique: true,
      allowNull: false,
      type: DataTypes.STRING
    },
    name: {
      unique: true,
      allowNull: false,
      type: DataTypes.STRING
    },
    image: {
      allowNull: true,
      type: DataTypes.STRING
    },
    description: {
      allowNull: true,
      type: DataTypes.TEXT
    },
    image: {
      allowNull: true,
      type: DataTypes.STRING
    },
  });
  Restaurant_Categorie.associate = function (models) {
  
  
  };

  return Restaurant_Categorie;
};