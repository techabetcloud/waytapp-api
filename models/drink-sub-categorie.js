
'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  const Drink_Sub_Categorie = sequelize.define(
      "Drink_Sub_Categorie",
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER
        },
        typeId: {
          type: DataTypes.INTEGER,
          references: {
            model: 'Drink_Categories',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE'
        },
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        description: {
          type: DataTypes.TEXT,
          allowNull:true
        },
        image:{
          type: DataTypes.STRING,
          get() {
            const rawValue = this.getDataValue('image');
            return rawValue ? process.env.BASE_URL+'images/drinks/'+rawValue : null;
          },
          allowNull:true
        },
      },
     
    );
    Drink_Sub_Categorie.associate = function (models) {
      Drink_Sub_Categorie.belongsTo(models.Drink_Categorie, {
        foreignKey: 'typeId',
        targetKey: 'id',
        as: 'categories'
    });
  
    };

  return Drink_Sub_Categorie;
};