'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  const Restaurant_Open = sequelize.define(
    "Restaurant_Open",
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    restaurantId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Restaurants',
        key: 'id'
      },
      allowNull: false,
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    },
    from_date: {
      allowNull: true,
      type: DataTypes.DATE,
    },
    to_date: {
      allowNull: true,
      type: DataTypes.DATE,
    },
  });
  Restaurant_Open.associate = function (models) {
  
  
  };
  return Restaurant_Open;
}