'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  const Drink_Categorie = sequelize.define(
      "Drink_Categorie",
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER
        },
        name: {
          type: DataTypes.STRING,
          allowNull: false,
          unique:true
        },
        slug: {
          type: DataTypes.STRING,
          allowNull: true,
          unique:true
        },
        description: {
          type: DataTypes.TEXT,
          allowNull:true
        },
        image:{
        type: DataTypes.STRING,
        get() {
          const rawValue = this.getDataValue('image');
          return rawValue ? process.env.BASE_URL+'images/drinks/'+rawValue : null;
        },
        allowNull:true
        },
        order: {
          type: DataTypes.INTEGER,
          allowNull:true
        },
      
        
      },
     
    );
    Drink_Categorie.associate = function (models) {
      Drink_Categorie.hasMany(models.Drink_Sub_Categorie, {
        foreignKey: "typeId",
        targetKey:"id",
        as:"subCategory"
      });
  
  
    };

  return Drink_Categorie;
};