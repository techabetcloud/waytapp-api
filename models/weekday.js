'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  const WeekDay = sequelize.define(
    "WeekDay",
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    day: {
      type: DataTypes.STRING,
      allowNull:false
    },
  });
  WeekDay.associate = function (models) {
  
  
  };
  return WeekDay;
};