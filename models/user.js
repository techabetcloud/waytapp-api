'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
      "User",
      {
        id: {
          autoIncrement: true,
          type: DataTypes.INTEGER,
          allowNull: false,
          primaryKey: true
        },
      
        name: {
          type: DataTypes.STRING,
          allowNull: false
        },
        email: {
          type: DataTypes.STRING,
          allowNull: false
        },
        gender: {
          allowNull: true,
          type: DataTypes.ENUM,
          values: ["male", "female"],
        },
        date_of_birth:{
          allowNull: true,
          type: DataTypes.DATEONLY,
        },
        username: {
          type: DataTypes.STRING,
          allowNull: true
        },
        password: {
          type: DataTypes.STRING,
          allowNull: true
        },
        description	: {
          type: DataTypes.STRING,
          allowNull: true
        },
        city: {
          type: DataTypes.STRING,
          allowNull: true
        },
        profile_img	: {
          type: DataTypes.STRING,
          get() {
            const rawValue = this.getDataValue('profile_img');
            return rawValue ? process.env.BASE_URL+'images/userProfile/'+rawValue : null;
          },
          allowNull: true
        },
        facebookId: {
          type: DataTypes.STRING,
          allowNull: true
        },
        verifyToken: {
          type: DataTypes.STRING,
          defaultValue: null,
          allowNull: true,
        },
        isVerified: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
        isDeleted: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
        lat: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        long: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        role:{
          type: DataTypes.ENUM,
          values:['User','Admin'],
          defaultValue: 'User'
        },
        createdAt: {
          allowNull: false,
          type: DataTypes.DATE
        },
        updatedAt: {
          allowNull: false,
          type: DataTypes.DATE
        }
      },
      {
       
        scopes: {
          withSecretColumns: {
            attributes: {
              include: ["password", "verifyToken", "isVerified", "isDeleted"],
            },
          },
        },
      }
    );
    User.associate = function (models) {

      User.hasOne(models.User_Login, {
        foreignKey: "userId",
        targetKey: "id",
        as: "Userlogin",
      });
      User.hasMany(models.Visit, {
        foreignKey: "userId",
        targetKey: "userId",
        as: "visit",
      });
      User.hasOne(models.Friends, {
        foreignKey: 'friendId',
        targetKey:'friendId',
        as:'friend'
      });
      User.hasOne(models.Postphoto, {
        foreignKey: 'userId',
        targetKey:'userId',
        as:'postphoto'
      });
    };
    return User;
  };
  
