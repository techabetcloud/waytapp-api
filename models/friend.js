'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  const Friends = sequelize.define(
      "Friends",
      {
        id: {
          autoIncrement: true,
          type: DataTypes.INTEGER,
          allowNull: false,
          primaryKey: true
        },
        userId: {
          type: DataTypes.INTEGER,
          references: {
            model: 'Users',
            key: 'id'
          },
          allowNull: false,
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE',
        },
        friendId:{
          type: DataTypes.INTEGER,
          references: {
            model: 'Users',
            key: 'id'
          },
          allowNull: false,
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE',
        },
        status:{
          allowNull: true,
          type: DataTypes.ENUM,
          values:['follow','unfollow','block'],
        },
      
      },
     
    );
    Friends.associate = function (models) {
      Friends.belongsTo(models.User, {
        foreignKey: 'friendId',
        targetKey:'id',
        as:'friend'
      });
    };
    return Friends;
  };
  

