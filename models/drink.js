'use strict';
const {
  Model
} = require('sequelize');
  module.exports = (sequelize, DataTypes) => {
    const Drink = sequelize.define(
        "Drink",
        {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER
          },
          sub_categorieId: {
            type: DataTypes.INTEGER,
            references: {
              model: 'Drink_Sub_Categories',
              key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
          },
          name: {
            type: DataTypes.STRING,
            allowNull: false,
          },
          price: {
            type: DataTypes.DECIMAL(10,2),
            allowNull: false,
          },
          detail: {
            type: DataTypes.TEXT,
            allowNull:true
          },
          image: {
            type: DataTypes.STRING,
            get() {
              const rawValue = this.getDataValue('image');
              return rawValue ? process.env.BASE_URL+'images/drinks/'+rawValue : null;
            },
            allowNull: true,
          },
          status: {
            type: DataTypes.BOOLEAN,
             defaultValue: true,
           comment: "1:Drink Active , 0:Drink Inactive",
          },
     

        })
        Drink.associate = function (models) {
          Drink.belongsTo(models.Drink_Sub_Categorie, {
            foreignKey: "sub_categorieId",
            targetKey:"id",
            as:"subCategory"
          });
         
        };
  return Drink;
};