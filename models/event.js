'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  const Event = sequelize.define(
      "Event",
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER
        },
        restaurantId: {
          type: DataTypes.INTEGER,
          references: {
            model: 'Restaurants',
            key: 'id'
          },
          allowNull: true,
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE',
        },
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        description: {
          allowNull: true,
          type: DataTypes.TEXT
        },
        address: {
          allowNull: true,
          type: DataTypes.STRING
        },
        city: {
          allowNull: true,
          type: DataTypes.STRING
        },
        state: {
          allowNull: true,
          type: DataTypes.STRING
        },
        country: {
          allowNull: true,
          type: DataTypes.STRING
        },
        picture: {
          type: DataTypes.STRING,
          get() {
            const rawValue = this.getDataValue('picture');
            return rawValue ? process.env.BASE_URL+'images/event/'+rawValue : null;
          },
          allowNull: true,

        },
        lat: {
          allowNull: true,
          type: DataTypes.STRING
        },
        long: {
          allowNull: true,
          type: DataTypes.STRING
        },
        from_date: {
          allowNull: true,
          type: DataTypes.DATE,
        },
        to_date: {
          allowNull: true,
          type: DataTypes.DATE,
        },
      },
     
    );
    Event.associate = function (models) {
      Event.belongsTo(models.Restaurant, {
        foreignKey: 'restaurantId',
        targetKey:'id',
        as:'restaurant'
      });
  
    };
  return Event;
};