require("@babel/register")
var createError = require('http-errors');
var express = require('express');
var path = require('path');
const cors = require('cors')
var cookieParser = require('cookie-parser');
var logger = require('morgan');
let ejs=require('ejs');
var { flash } = require('express-flash-message');
const session = require('express-session');
const bodyParser = require('body-parser');
const Validate = require("express-validation"); 
const { joiErrorFormatConvert } = require('./helpers/responseCode')
const Sequelize = require('sequelize')
var indexRouter = require('./routes/index');
var apiRouter = require('./routes/apiRoutes');
const authRouter = require('./routes/authRoutes')
const adminRouter=require('./routes/adminRoutes')
const apiDeviceMiddleware  = require('./helpers/Middleware/apiDevice');
const { Fancybox }= require("@fancyapps/ui");
var i18n = require('i18n');

const app = express();




//  session config
var sess = {
  secret: 'waytwayt',
  saveUninitialized: true,
  resave:true,
  cookie:{maxAge:3600000000}
}


// CORS HANDLER
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', process.env.BASE_URL);
  res.header('Access-Control-Allow-Headers', 'Origin, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token,Authorization');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT ,DELETE');
  res.header('Access-Control-Allow-Credentials', true);
  next();
})
//CORS HANDLING ENDS




// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.use(express.static(path.join(__dirname + '/public')));
app.use(express.static(__dirname + "/publicChat"));


// app.set('view engine', 'jade');
app.set('view engine', 'ejs');


//  i18n config
i18n.configure({
  locales:['en', 'nb'],
  directory: __dirname + '/locales',
  defaultLocale: 'en',
  cookie: 'locale',
  directoryPermissions: '755',
});


app.use(i18n.init);
app.use(cors())
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// app.use(session(sess))
app.use(session({ secret: 'waytwayt',saveUninitialized: true,resave: true, cookie: { maxAge: 3600000000 }}))
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json({ extended: true }));
app.use(flash({ sessionKeyName: 'wayt@wayt' })); 


// api middleware 
 app.use('/', indexRouter);
 app.use('/api/auth', authRouter);
 app.use('/admin', adminRouter);
 app.use('/api',apiDeviceMiddleware, apiRouter );

// ===============


//  joi error handle
app.use((err, req, res, next) => {
  if (err instanceof Validate.ValidationError){
    if(err.details.body){
    const errors = joiErrorFormatConvert(
      err.statusCode,
      err.details.body[0].message,
      err.details?.body[0]?.context,
      false,
      {}
    );
    return res.status(err.statusCode).json(errors);
    } else if (err.details.query){
      const errors = joiErrorFormatConvert(
        err.statusCode,
        err.details.query[0].message,
        err.details?.query[0]?.context,
        false,
        {}
      );
      return res.status(err.statusCode).json(errors);
    }
  }else{
    return res.status(500).json(err);
  }
 next()
})


//error page 404
app.use(function (req, res) {
  res.status(404).json({data:'404 page not found'});
});


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};


  // render the error page
  res.status(err.status || 500);
  res.render('error');
      next(err);

});


module.exports = app;
