const express = require('express');
 const  { validate } = require ('express-validation')

import userController from '../Controller/user/user.controller'
import adminController from '../Controller/admin/auth/auth.controller';
import emailservice from '../helpers/email.service'
import * as userValidator from '../Controller/user/user.validation'

const { imageUpload } = require('../generic/image.upload')
const { upload } = require('../generic/index')


var authRouter = express.Router();


authRouter.post('/signin',validate(userValidator.register),userController.register)
authRouter.post('/login',validate(userValidator.login),userController.login)

// need to remove after confirmation
authRouter.post('/sendemail',emailservice.sendHTmlemail)
authRouter.get('/verify',validate(userValidator.verify),userController.verify)
// -------------

authRouter.post('/forgotPassword',validate(userValidator.forgotPassword),userController.forgotPassword)
authRouter.post('/verify-otp',validate(userValidator.verifyOTP),userController.verifyOTP)
authRouter.post('/reset-password',validate(userValidator.resetPassword),userController.resetPassword)
authRouter.post('/logout',userController.logout)


authRouter.post('/login-with-facebook',userController.loginWithFacebook)

//  multer image uploader
authRouter.post('/image/upload', upload, imageUpload)




module.exports = authRouter;
