var express = require('express');
import authController from '../Controller/admin/auth/auth.controller';
import { userRouter } from '../Controller/admin/userlist/userListRoutes';
import { emailRoutes } from '../Controller/admin/sendEmail/sendEmailRoute';
import { sendNotificationRoutes } from '../Controller/admin/sendNotification/sendNotificationRoute';
import { restaurantsRoutes } from '../Controller/admin/restaurant/restaurantRoute';
import { eventRoutes } from '../Controller/admin/event/eventRoute';
import { drinksRoutes } from '../Controller/admin/drinks/drinksRoute';
import { drinkCategoreiRoutes } from '../Controller/admin/drinkCategorie/drinkCategorieRoute';
import { drinkSubCategoreiRoutes } from '../Controller/admin/drinkSubCategorie/drinkSubCategorieRoute';
import { drinkRestaurantRoutes } from '../Controller/admin/drinkRestaurant/drinkRestaurantRoute';
import { settingRoutes } from '../Controller/admin/setting/settingRoute';

var adminRouter = express.Router();
// <---admin auth routes-->
adminRouter.get("/", authController.isLoggedIn, (req, res) => { res.redirect('/admin/dashbord')})
adminRouter.post('/login',authController.adminlogin)
adminRouter.get('/login',authController.create)
adminRouter.get('/logout',authController.isLoggedIn,authController.logout)
// <---admin auth routes-->

// <---view of Dashbord page after login--->
adminRouter.get('/dashbord',authController.isLoggedIn,(req,res)=> { res.render('admin/index')} );
    // <---view of Dashbord page--->

    // <--Routes for userlist-->
adminRouter.use('/userlist',authController.isLoggedIn,userRouter);
// <--Routes for userlist-->

// <--Routes for emailtoken-->
 adminRouter.get('/emailtoken',authController.isLoggedIn,(req,res)=> { 
    res.render('admin/emailToken/listing')}); 

// adminRouter.get('/emailtoken',authController.emailTokens);

adminRouter.get('/emailtoken/edit',authController.isLoggedIn,(req,res)=> { 
        res.render('admin/emailToken/edit')});
// <--Routes for emailtoken-->

// <--Routes for email-->
    adminRouter.use('/sendemail',authController.isLoggedIn,emailRoutes);    
// <--Routes for email-->

// <--Routes for sendNotification-->
adminRouter.use('/sendnotification',authController.isLoggedIn,sendNotificationRoutes);    
// <--Routes for sendNotification-->

// <--Routes for restaurants-->
adminRouter.use('/restaurants',authController.isLoggedIn,restaurantsRoutes)
// <--Routes for restaurants-->

// <--Routes for Event-->
adminRouter.use('/event',authController.isLoggedIn,eventRoutes)
// <--Routes for Event-->

// <--Routes for Drinks-->
adminRouter.use('/drinks',authController.isLoggedIn,drinksRoutes)
// <--Routes for Drinks-->

// <--Routes forTypesof Drinks-->
adminRouter.use('/drinkCategorie',authController.isLoggedIn,drinkCategoreiRoutes)
// <--Routes for Types Of Drinks-->

// <--Routes for sub categorie of Drinks-->
adminRouter.use('/drinkSubCategorie',authController.isLoggedIn,drinkSubCategoreiRoutes)
// <--Routes for sub categorie of Drinks-->

// <--Routes for restaurantDrink/drinkRestaurant-->
adminRouter.use('/restaurantDrink',authController.isLoggedIn,drinkRestaurantRoutes)
// <--Routes for restaurantDrink/drinkRestaurant-->

// <--Routes for setting-->
adminRouter.use('/setting',authController.isLoggedIn,settingRoutes)
// <--Routes for setting-->

module.exports = adminRouter;