var express = require('express');

const clubbarRouter = require('../Controller/Club-Bar/Club-Bar.routes')
const userRouter = require('../Controller/user/user.routes')
const friendRouter = require ('../Controller/friend/friend.routes')
const eventRouter = require ('../Controller/event/event.routes')
const chatRouter = require ('../Controller/chat/chat.routes')
const drinkRouter = require ('../Controller/drink/drink.routes')
const likesRouter = require ('../Controller/likes/likes.routes')
const settingRouter = require ('../Controller/setting/setting.routes')

const { bulkUploads } = require('../generic/image.upload')
const { bulkUpload } = require('../generic/index')
const apiRouter = express.Router();


apiRouter.use('/user', userRouter)
apiRouter.use('/club-bar', clubbarRouter)
apiRouter.use('/friend', friendRouter)
apiRouter.use('/event', eventRouter)
apiRouter.use('/chat', chatRouter)
apiRouter.use('/drink', drinkRouter)
apiRouter.use('/likes', likesRouter)
apiRouter.use('/setting', settingRouter)


//  multer image uploader
apiRouter.post('/bulk-image/upload', bulkUpload,bulkUploads)

module.exports = apiRouter;
