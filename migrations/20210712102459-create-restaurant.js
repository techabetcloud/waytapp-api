'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Restaurants', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      adminId: {
        type: Sequelize.INTEGER,
        allowNull: true,
        // references: {
        //   model: 'admins',
        //   key: 'id'
        // },
        // onUpdate: 'CASCADE',
        // onDelete: 'CASCADE'
       },
      categoryId: {
        type: Sequelize.INTEGER,
          references: {
          model: 'Restaurant_Categories',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
     
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      logo: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      description: {
        type: Sequelize.TEXT,
        allowNull:true
        ,
      },
      address:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      city: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      state: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      country: {
        type: Sequelize.STRING,
        allowNull: true,
      },
    
      link: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      month_close: {
        allowNull: true,
        type: Sequelize.STRING
      },
      lat: {
        allowNull: true,
        type: Sequelize.STRING
      },
      long: {
        allowNull: true,
        type: Sequelize.STRING
      },
      status: {
      defaultValue: true,
        type: Sequelize.BOOLEAN,
        comment: "1:Active , 0:Inactive",
       
      },
      drinks_allowed: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
       // defaultValue: false
       comment: "1:Allowed , 0:notAllowed",
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
     
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
      },
     
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Restaurants');
  }
};