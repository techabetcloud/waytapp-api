'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Chats', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      senderId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Users',
          key: 'id'
        },
        allowNull: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      receiverId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Users',
          key: 'id'
        },
        allowNull: false,
      },
      message_content: {
        type: Sequelize.TEXT,
        allowNull:false,
      },
      is_received: {
        type: Sequelize.BOOLEAN,
        defaultValue:false,
       comment: "1:Chat Received , 0:Chat Not Received",
      },
      is_seen: {
        type: Sequelize.BOOLEAN,
        defaultValue:false,
       comment: "1:Chat seen, 0:chat Not seen",
      },
      is_deleted_by_sender: {
        type: Sequelize.BOOLEAN,
        defaultValue:false
      },
      is_deleted_by_receiver: {
        type: Sequelize.BOOLEAN,
        defaultValue:false
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Chats');
  }
};