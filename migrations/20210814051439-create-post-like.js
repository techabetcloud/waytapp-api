'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Post_Likes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      likedBY: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Users',
          key: 'id'
        },
        allowNull: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
       postId:{
        type: Sequelize.INTEGER,
        references: {
          model: 'Postphotos',
          key: 'id'
        },
        allowNull: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      // image: {
      //   allowNull: false,
      //   type: Sequelize.STRING,
      //   // references: {
      //   //   model: 'Postphotos',
      //   //   key: 'id'
      //   // },
      // },
      // image_Link: {
      //   allowNull: true,
      //   type: Sequelize.STRING
      // },
      // likes:{
      //   allowNull: false,
      //   type: Sequelize.BOOLEAN,
      //   comment: "1 = Like , 0 = unLike",
      // },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Post_Likes');
  }
};