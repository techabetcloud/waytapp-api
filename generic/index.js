import express from 'express'
import multer  from 'multer'



export const bulkUpload = multer({
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, "public/images/userProfile");
        },
        filename: function (req, file, cb) {

            cb(null, Date.now() + "-" + file.originalname);
        },
    }),
}).array("image", 6);

export const upload = multer({
    storage: multer.diskStorage({
        destination:function (req,file,cb) {
        cb(null, "public/images/userProfile");
        },
        filename: function (req, file, cb) {

            cb(null, Date.now() + "-" + file.originalname);
        },
    })
}).single('image')
export const restaurantStorage =  multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, "public/images/restaurant");
        },
        filename: function (req, file, cb) {
            cb(null,file.originalname);
        },

    });



    export const eventStorage =  multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, "public/images/event");
        },
        filename: function (req, file, cb) {

            cb(null,file.originalname);
        },
    });



    export const drinksStorage =  multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, "public/images/drinks");
        },
        filename: function (req, file, cb) {

            cb(null,file.originalname);
        },
      
    });

    export const drinksCategoryStorage =  multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, "public/images/drinks");
        },
        filename: function (req, file, cb) {

            cb(null,file.originalname);
        },
      
    });

    export const drinkSubCategoryStorage =  multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, "public/images/drinks");
        },
        filename: function (req, file, cb) {

            cb(null,file.originalname);
        },
      
    });