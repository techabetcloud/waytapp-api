var express = require('express')


export const bulkUploads = async (req, res) => {
    try {
    const fileinfo = req.files;
    const data = []
    fileinfo.map(el => {
      data.push(el.filename)
    })

    if(fileinfo){
        return res.send({ message: "upload picture successfully", data:{image: data}})
    }else{
        return res.send({message:"Only .png, .jpg and .jpeg format allowed!"})
    }
    } catch (error) {
       return res.send({message:"Something Went wrong"}) 
    }
}

export const imageUpload = async (req,res)=>{
    try {
        const fileinfo = req.file;
        if(fileinfo == null){
            return res.send({message:"Only .png, .jpg and .jpeg format allowed!"})
        }else{
            return res.send({ message: "upload picture successfully",data:{profile_img: fileinfo.filename} })
        }
    } catch (error) {
       return res.send({message:"Something Went wrong"}) 
    
    }
}
// export const restaurantImages = async (req, res) => {
//     try {
//     const fileinfo = req.files;
//     if(fileinfo){
//         return res.send({ message: "upload picture successfully", fileinfo })

//     }else{
//         return res.send({message:"Only .png, .jpg and .jpeg format allowed!"})
//     }
//     } catch (error) {
//        return res.send({message:"Something Went wrong"}) 
//     }
    
// }


